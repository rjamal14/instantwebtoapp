import React from 'react'
import Swal from 'sweetalert2'
import { getProfile } from "../../actions/profile.actions";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from 'next/router'
var localStorage = require('localStorage')

const Auth = () => {
    const dispatch = useDispatch();
    const router = useRouter()
    const token = localStorage.getItem('token') ?? null;
    
    React.useEffect(() => {
        if(token !== null){
          router.replace('/app-list')
        }
    }, [token])

    React.useEffect(() => {
        if(token === null){
            const token = window.location.pathname.replace("/auth/","");
            dispatch(getProfile(token))
                .then((res) => {
                    localStorage.setItem('token', token);
                    localStorage.setItem("email", res.email);
                    localStorage.setItem("show_unpaid", 1);
                    router.reload()
                    Swal.fire({
                        icon: "success",
                        title: "Success",
                        text: "Login Successfully!",
                        confirmButtonColor: "#2e9afe",
                    });
                })
                .catch((err) => {
                    router.replace('/')
                    Swal.fire({
                        icon: "error",
                        title: "Oops...",
                        text: "Your link cannot be used!",
                        confirmButtonColor: "#2e9afe",
                    });
                });
        }
    }, [token])

    return (
        <>
            <div className="preloader-area">
                <div className="d-table">
                    <div className="d-table-cell">
                        <img src="/images/appsinstant-web-to-android-creator.png" style={{height:50}} alt="logo" />
                    </div>
                </div>
            </div>

            <style jsx>{`
                .preloader-area {
                    position: fixed;
                    background: #fff;
                    width: 100%;
                    top: 0;
                    height: 100%;
                    z-index: 1010;
                    left: 0;
                    text-align: center;
                    opacity: .96;
                }
                .preloader-area img {
                    margin-bottom: 5px;
                }
                .preloader-area p {
                    font-size: 17px;
                }
            `}</style>
        </>
    )
}

export default Auth;
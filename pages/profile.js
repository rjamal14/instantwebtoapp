import React from "react";
import NavbarStyleConvert from "@/components/_App/NavbarStyleConvert";
import Menu from "@/components/_App/Menu";
import FooterStyleThree from "@/components/_App/FooterStyleThree";
import "@material/data-table/dist/mdc.data-table.css";
import "@rmwc/data-table/data-table.css";
import "@rmwc/icon/icon.css";
import { useDispatch, useSelector } from "react-redux";
import { getProfile, UpdateProfile, loginClient } from "../actions/profile.actions";
var localStorage = require("localStorage");
import { useRouter } from "next/router";
import Swal from "sweetalert2";
import { useForm } from "react-hook-form";
import env from "../config/env";
import Loader from '@/components/_App/Loader'

const AppList = () => {
  const dispatch = useDispatch();
  const profile = useSelector((state) => state.profile.profile);
  const status_code = useSelector((state) => state.profile.status_code);
  const token = localStorage.getItem("token") ?? null;
  const [editable, setEditable] = React.useState(false);
  const [photo, setPhoto] = React.useState(null);
  const [count, setCount] = React.useState(0);
  const router = useRouter();
  const [loading, SetLoading] = React.useState(false);
  const token_client = localStorage.getItem('token_client');
  const expires_in = localStorage.getItem('expires_in');

  const {
    register,
    handleSubmit,
    getValues,
    setValue,
    formState: { errors },
  } = useForm();

  React.useEffect(() => {
    if (token_client === null || new Date(expires_in) < new Date())
      dispatch(loginClient()).then((res) => {
        var expires_in = new Date(res.exp * 1000);
        expires_in.setHours(expires_in.getHours() - 6);
        localStorage.setItem("token_client", res.token);
        localStorage.setItem("expires_in", expires_in);
        window.location.reload()
      });
  }, []);

  React.useEffect(() => {
    if (token !== null || status_code === 401) {
      dispatch(getProfile(token))
        .then((res) => { })
        .catch((err) => {
          console.log("err", err);
          localStorage.clear();
          router.replace("/login");
          Swal.fire({
            icon: "warning",
            title: "Oops...",
            text: "login session has ended, please login again!",
            confirmButtonColor: "#2e9afe",
          });
        });
    }
  }, [token, status_code]);

  React.useEffect(() => {
    if (profile?.first_name) {
      setValue("first_name", profile.first_name);
    }
    if (profile?.last_name) {
      setValue("last_name", profile.last_name);
    }
    if (profile?.sex) {
      setValue("sex", profile.sex);
    }
    if (profile?.phone) {
      setValue("phone", profile.phone);
    }
    if (profile?.photo) {
      setPhoto(
        profile.photo !== null ? env.baseUrl + profile.photo : profile.photo
      );
    }
    if (profile?.nik) {
      setValue("nik", profile.nik);
    }
  }, [profile]);

  React.useEffect(() => {
    setTimeout(() => {
      if (count > 0) {
        var photo = getValues("photo_before");
        var file = photo[0];
        var reader = new FileReader();
        reader.onloadend = function () {
          setPhoto(reader.result);
        };
        reader.readAsDataURL(file);
      }
    }, 1000);
  }, [count]);

  function getBase64(file, cb) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      cb(reader.result);
    };
    reader.onerror = function (error) {
      console.log("Error: ", error);
    };
  }

  const onSubmit = (data) => {
    SetLoading(true)
    let payload = data;
    if (data.photo_before.length > 0) {
      getBase64(data.photo_before[0], (result) => {
        payload["photo"] = result;
      });
    }
    setTimeout(() => {
      dispatch(UpdateProfile(payload, profile.id))
        .then((res) => {
          Swal.fire({
            icon: "success",
            title: "Success",
            text: "Your profile has been updated.",
            confirmButtonColor: "#2e9afe",
          });
          setEditable(false);
          SetLoading(false)
        })
        .catch((err) => {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Something went wrong!",
          });
          SetLoading(false)
        });
    }, 1000);
  };

  return (
    <>
      {loading && <Loader />}
      <NavbarStyleConvert active="Profile" />
      <div id="content-area" className="ptb-83">
        <div className="how-it-works-area margin-auto">
          <div className="how-it-works-content">
            <div className="row m-0">
              <Menu active="Profile" />
              <div className="col-lg-9">
                <div className="row">
                  <div className="content bg-white radius-6 mt-50 p-5" style={{
                    overflowX: 'scroll',
                    borderWidth: 1,
                    borderColor: 'black'
                  }}>
                    <div className="row">
                      <div className="col-md-3 col-xs-12">
                        <div className="d-flex flex-column align-items-center text-center">
                          {profile?.photo !== null ? (
                            <img
                              className="rounded-circle mt-5"
                              width="150px"
                              src={photo}
                            />
                          ) : (
                            <div
                              className="download-app"
                              style={{
                                background: "#2e9afe",
                                width: 150,
                                height: 150,
                                borderRadius: 150,
                                color: "white",
                                flexDirection: "column",
                                display: "flex",
                                justifyContent: "center",
                                marginRight: 10,
                                alignContent: "center",
                                textAlign: "center",
                              }}
                            >
                              <text style={{ fontSize: 40 }}>
                                {profile?.email
                                  ? profile.email
                                    .substring(0, 1)
                                    .toUpperCase()
                                  : ""}
                              </text>
                            </div>
                          )}

                          {editable && (
                            <div>
                              <label for="file-upload" class="custom-file-upload">
                                <i class="fa fa-cloud-upload"></i> Choose File
                              </label>
                              <input
                                id="file-upload"
                                type="file"
                                style={{
                                  width: 100,
                                }}
                                {...register("photo_before", {
                                  //for set validator
                                })}
                                onChange={(e) => {
                                  setCount(count + 1);
                                }}
                                accept="image/*"
                                className="form-control mt-4 freak"
                                placeholder="Enter Last Name"
                              />
                            </div>
                          )}
                          <span className="font-weight-bold mt-3">{`${profile.first_name ?? ""
                            } ${profile.last_name ?? "-"}`}</span>
                          <span className="text-black-50">
                            {`${profile?.email ?? "-"}`}
                          </span>
                          <span> </span>
                        </div>
                      </div>
                      <div className="col-md-9 col-xs-12">
                        <form onSubmit={handleSubmit(onSubmit)}>
                          <div className="form-group">
                            <div className="row">
                              <div className="col-md-6 mt-3">
                                <label className="labels mb-2">
                                  First Name
                                </label>
                                <input
                                  type="text"
                                  className="form-control"
                                  {...register("first_name", {
                                    //for set validator
                                  })}
                                  placeholder="Enter First Name"
                                  disabled={!editable}
                                />
                                {errors.first_name && (
                                  <span className="text-validator">
                                    {errors?.first_name?.message}
                                  </span>
                                )}
                              </div>
                              <div className="col-md-6 mt-3">
                                <label className="labels mb-2">
                                  Last Name
                                </label>
                                <input
                                  type="text"
                                  className="form-control"
                                  {...register("last_name", {
                                    //for set validator
                                  })}
                                  disabled={!editable}
                                  placeholder="Enter Last Name"
                                />
                                {errors.last_name && (
                                  <span className="text-validator">
                                    {errors?.last_name?.message}
                                  </span>
                                )}
                              </div>
                            </div>
                            <div className="row mt-3">
                              <div className="col-md-12 mt-2 mb-2">
                                <label className="labels mb-2">
                                  Phone Number
                                </label>
                                <input
                                  type="text"
                                  className="form-control"
                                  {...register("phone", {
                                    //for set validator
                                  })}
                                  placeholder="Enter Phone Number"
                                  disabled={!editable}
                                />
                                {errors.phone && (
                                  <span className="text-validator">
                                    {errors?.phone?.message}
                                  </span>
                                )}
                              </div>
                              <div className="col-md-12 mt-2 mb-2">
                                <label className="labels mb-2">
                                  Gender
                                </label>
                                <select
                                  disabled={!editable}
                                  className="form-control"
                                  {...register("sex", {
                                    //for set validator
                                  })}
                                  name="sex"
                                  id="sex"
                                >
                                  <option
                                    selected={profile?.sex === null}
                                    value={null}
                                  >
                                    Select Gender
                                  </option>
                                  <option
                                    selected={profile?.sex === "male"}
                                    value="male"
                                  >
                                    Male
                                  </option>
                                  <option
                                    selected={profile?.sex === "female"}
                                    value="female"
                                  >
                                    Female
                                  </option>
                                </select>
                                {errors.sex && (
                                  <span className="text-validator">
                                    {errors?.sex?.message}
                                  </span>
                                )}
                              </div>
                            </div>
                            <div className="mt-5" style={{
                              display: 'flex',
                              justifyContent: 'flex-end',
                              marginRight: 23
                            }}>
                              {editable ? (
                                <div>
                                  <div
                                    style={{
                                      marginRight: 20
                                    }}
                                    onClick={() => {
                                      setEditable(false);
                                    }}
                                    className="btn btn-primary profile-button-disable"
                                  >
                                    Cancel
                                  </div>
                                  <button
                                    className="btn btn-primary profile-button"
                                    type="submit"
                                  >
                                    Save Profile
                                  </button>
                                </div>
                              ) : (
                                <div
                                  onClick={() => {
                                    setEditable(true);
                                  }}
                                  className="btn btn-primary profile-button"
                                >
                                  Edit Profile
                                </div>
                              )}
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <FooterStyleThree />
    </>
  );
};

export default AppList;

import React from "react";
import SliderCaptcha from "../components/Common/slider-captcha/react";
import { useForm } from "react-hook-form";
import { login } from "../actions";
import { useDispatch } from "react-redux";
import Swal from "sweetalert2";
var localStorage = require("localStorage");
import { useRouter } from "next/router";

const Login = () => {
    const router = useRouter();
    const dispatch = useDispatch();
    const {
        register,
        handleSubmit,
        reset,
        getValues,
        formState: { errors },
    } = useForm();
    const [time, setTime] = React.useState(null);
    const [capcha, setCapcha] = React.useState(false);

    React.useEffect(() => {
        if (localStorage.getItem("token")) {
            router.replace("/app-list");
        }
    }, []);

    React.useEffect(() => {
        var data_time = new Date().getTime();
        setTime(data_time)
    }, []);


    const onSubmit = (data) => {
        if(capcha){
            dispatch(login(data))
                .then((res) => {
                    Swal.fire({
                        title: 'Success',
                        text: "Please check your email address and login to download or make any change on your apps!",
                        icon: 'success',
                        showCancelButton: true,
                        allowOutsideClick: false,
                        cancelButtonColor: '#2e9afe',
                        confirmButtonColor: '#2e9afe',
                        confirmButtonText: 'Check email',
                        cancelButtonText: 'Go to home',
                      }).then((result) => {
                        if (result.isConfirmed) {
                            let domain = getValues('email')
                            router.replace('https://'+domain.split("@")[1])
                        }else{
                            window.location.replace('/')
                        }
                    })
                })
                .catch((err) => {
                    Swal.fire({
                        title: 'Oops...',
                        text: "Something went wrong!",
                        icon: 'error',
                        showCancelButton: false,
                        confirmButtonColor: '#2e9afe',
                        confirmButtonText: 'Yes'
                      }).then((result) => {
                        if (result.isConfirmed) {
                            window.location.reload()
                        }
                    })
                });
        }else{
            Swal.fire({
                icon: "warning",
                title: "Oops...",
                text: "Please do the Captcha process!",
                confirmButtonColor: "#2e9afe",
            });
        }
    };

    return (
        <>
            <div className="profile-authentication-area">
                <div className="d-table">
                    <div className="d-table-cell">
                        <div className="container">
                            <div className="signin-form">
                                <center>
                                    <a href="/" className="d-inline-block">
                                        <img
                                            src="/images/appsinstant-web-to-android-creator.png"
                                            style={{ height: 60 }}
                                            alt="logo"
                                        />
                                    </a>
                                </center>
                                <h2 className="mt-50">Login or Register</h2>
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    <div className="form-group mt-50">
                                        <input
                                            type="email"
                                            className="form-control"
                                            placeholder="Enter Email"
                                            {...register("email", {
                                                required: "Email is required",
                                                pattern: {
                                                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                                    message:
                                                        "invalid email address",
                                                },
                                            })}
                                        />
                                        {errors.email && (
                                            <span
                                                style={{
                                                    fontSize: 14,
                                                    marginLeft: 8,
                                                    marginTop: 4,
                                                    color: "red",
                                                }}
                                            >
                                                {errors?.email?.message}
                                            </span>
                                        )}
                                    </div>
                                    <div style={{
                                        width:'30%',
                                        marginLeft:-10,
                                        marginBottom:-25
                                    }}>
                                        <SliderCaptcha
                                            text={{ anchor: "I am human", challenge: 'Slide to finish the puzzle' }}
                                            verify={"https://server.instantwebtoapp.com/captcha/verify/"+time}
                                            create={"https://server.instantwebtoapp.com/captcha/create/"+time}
                                            callback={(val) => {
                                                setCapcha(true)
                                            }}
                                        />
                                    </div>
                                    <button className="mt-50 signin-form-form-button" type="submit">
                                        Submit
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Login;

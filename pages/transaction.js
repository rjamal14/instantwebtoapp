import React from "react";
import NavbarStyleConvert from "@/components/_App/NavbarStyleConvert";
import Menu from "@/components/_App/Menu";
import FooterStyleThree from "@/components/_App/FooterStyleThree";
import "@material/data-table/dist/mdc.data-table.css";
import "@rmwc/data-table/data-table.css";
import "@rmwc/icon/icon.css";
import Table from "../components/Common/table";
import { useDispatch, useSelector } from "react-redux";
import { getTransactionList, CancelTransaction, NewLinkTransaction, getProfile, loginClient } from "../actions";
var localStorage = require("localStorage");
import { useRouter } from "next/router";
import Swal from "sweetalert2";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import Loader from '@/components/_App/Loader'

const AppList = () => {
    const dispatch = useDispatch();
    const [status, SetStatus] = React.useState('inquiry');
    const [page, SetPage] = React.useState(1);
    const [perPage, SetPerPage] = React.useState(10);
    const transaction_list = useSelector(
        (state) => state.transaction.transaction_list
    );
    const status_code = useSelector((state) => state.transaction.status_code);
    const token = localStorage.getItem("token") ?? null;
    const router = useRouter();
    const [loading, SetLoading] = React.useState(false);
    const token_client = localStorage.getItem('token_client');
    const expires_in = localStorage.getItem('expires_in');

    React.useEffect(() => {
        if (token !== null || status_code === 401) {
            dispatch(getProfile(token))
                .then((res) => { })
                .catch((err) => {
                    console.log("err", err);
                    localStorage.clear();
                    router.replace("/login");
                    Swal.fire({
                        icon: "warning",
                        title: "Oops...",
                        text: "login session has ended, please login again!",
                        confirmButtonColor: "#2e9afe",
                    });
                });
        }
    }, [token, status_code]);

    React.useEffect(() => {
        if (token_client === null || new Date(expires_in) < new Date())
            dispatch(loginClient()).then((res) => {
                var expires_in = new Date(res.exp * 1000);
                expires_in.setHours(expires_in.getHours() - 6);
                localStorage.setItem("token_client", res.token);
                localStorage.setItem("expires_in", expires_in);
                window.location.reload()
            });
    }, []);

    const list = transaction_list?.data?.map((i, index) => {
        return {
            col1: <div style={{ minWidth: 80 }}>{((page - 1) * perPage + (index + 1)).toString()}</div>,
            col2: <div style={{ minWidth: 165 }}>{i?.id}</div>,
            col3: <div style={{ minWidth: 150 }}>{generateItemName(i?.transaction_items)}</div>,
            col6: <div>{`$ ${i?.total}`}</div>,
            col7: <div style={{ minWidth: 173 }}>{dateFormat(i?.createdAt)}</div>,
            col8: i,
        };
    });

    const data = React.useMemo(() => list, [transaction_list?.data]);
    const columns = React.useMemo(
        () => [
            {
                Header: "No",
                accessor: "col1",
                textAlign: "center",
                width: "10%",
            },
            {
                Header: "TransactionID",
                accessor: "col2",
                textAlign: "left",
                width: "15%",
            },
            {
                Header: "Item Name",
                accessor: "col3",
                textAlign: "left",
                width: "10%",
            },
            {
                Header: "Total",
                accessor: "col6",
                textAlign: "left",
                width: "5%",
            },
            {
                Header: "Date",
                accessor: "col7",
                textAlign: "left",
                width: "15%",
            },
            {
                Header: "Action",
                accessor: "col8",
                textAlign: "center",
                width: "25%",
                Cell: ({ cell }) => {
                    if (status === 'expired') {
                        return (
                            <div style={{ minWidth: 200 }}>
                                <button
                                    style={{ minWidth: 147 }}
                                    className="btn btn-primary profile-button"
                                    onClick={() => {
                                        SetLoading(true)
                                        OnNewLink(cell.value.id)
                                    }}
                                >
                                    Make new payment
                                </button>
                            </div>
                        )
                    } else {
                        return (
                            <div>
                                <button
                                    className="btn btn-primary profile-button"
                                    style={{
                                        minWidth: 227
                                    }}
                                    onClick={() => {
                                        window.open(`${cell.value?.invoices[0]?.payments[0]?.res?.links[1]?.href}`);
                                    }}
                                >
                                    Make a payment
                                </button>
                                <button
                                    className="btn btn-primary profile-button-disable"
                                    style={{
                                        marginLeft: 5,
                                        minWidth: 227
                                    }}
                                    onClick={() => {
                                        SetLoading(true)
                                        OnCancel(cell.value.id)
                                    }}
                                >
                                    Cancel Payment
                                </button>
                            </div>
                        )
                    }
                }
            },
        ],
        [transaction_list?.data]
    );

    function dateFormat(params) {
        var date = new Date(params);
        var tahun = date.getFullYear();
        var bulan = date.getMonth();
        var tanggal = date.getDate();
        var hari = date.getDay();
        var jam = date.getHours();
        var menit = date.getMinutes();
        var detik = date.getSeconds();
        switch (hari) {
            case 0: hari = "Sunday"; break;
            case 1: hari = "Monday"; break;
            case 2: hari = "Tuesday"; break;
            case 3: hari = "Wednesday"; break;
            case 4: hari = "Thursday"; break;
            case 5: hari = "Friday"; break;
            case 6: hari = "Saturday"; break;
        }
        switch (bulan) {
            case 0: bulan = "January"; break;
            case 1: bulan = "February"; break;
            case 2: bulan = "March"; break;
            case 3: bulan = "April"; break;
            case 4: bulan = "May"; break;
            case 5: bulan = "June"; break;
            case 6: bulan = "July"; break;
            case 7: bulan = "August"; break;
            case 8: bulan = "September"; break;
            case 9: bulan = "October"; break;
            case 10: bulan = "November"; break;
            case 11: bulan = "December"; break;
        }
        var tampilTanggal = hari + ", " + (tanggal < 10 ? '0' + tanggal : tanggal) + " " + (bulan < 10 ? '0' + bulan : bulan) + " " + tahun;
        var tampilWaktu = (jam < 10 ? '0' + jam : jam) + ":" + (menit < 10 ? '0' + menit : menit)
        return tampilTanggal + ' ' + tampilWaktu
    }

    const list2 = transaction_list?.data?.map((i, index) => {
        return {
            col1: <div style={{ minWidth: 80 }}>{((page - 1) * perPage + (index + 1)).toString()}</div>,
            col2: <div style={{ minWidth: 165 }}>{i?.id}</div>,
            col3: <div style={{ minWidth: 150 }}>{generateItemName(i?.transaction_items)}</div>,
            col6: <div>{`$ ${i?.total}`}</div>,
            col7: <div style={{ minWidth: 173 }}>{dateFormat(i?.createdAt)}</div>,
        };
    });

    const data2 = React.useMemo(() => list2, [transaction_list?.data]);
    const columns2 = React.useMemo(
        () => [
            {
                Header: "No",
                accessor: "col1",
                textAlign: "center",
                width: "10%",
            },
            {
                Header: "TransactionID",
                accessor: "col2",
                width: "15%",
                textAlign: "left",
            },
            {
                Header: "Item Name",
                accessor: "col3",
                width: "15%",
                textAlign: "left",
            },
            {
                Header: "Total",
                accessor: "col6",
                textAlign: "left",
                width: "5%",
            },
            {
                Header: "Date",
                accessor: "col7",
                textAlign: "left",
                width: "15%",
            },
        ],
        [transaction_list?.data]
    );

    function generateItemName(params) {
        var txt = "";
        params.map((value, index) => {
            txt += value.item_name;
            txt += index > 0 && index === params.length - 1 ? ", " : "";
        });

        return txt;
    }


    function OnCancel(id) {
        dispatch(CancelTransaction(id))
            .then((res) => {
                dispatch(getTransactionList('inquiry', page, perPage));
                SetLoading(false)
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: "Payment has been successfully canceled",
                    confirmButtonColor: "#2e9afe",
                });
            })
            .catch((err) => {
                SetLoading(false)
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: err.message,
                    confirmButtonColor: "#2e9afe",
                });
            });
    }

    function OnNewLink(id) {
        dispatch(NewLinkTransaction(id))
            .then((res) => {
                SetLoading(false)
                dispatch(getTransactionList('expired', page, perPage));
                window.open(res)
            })
            .catch((err) => {
                SetLoading(false)
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: err.message,
                    confirmButtonColor: "#2e9afe",
                });
            });
    }

    function getData(type) {
        if (token !== null || status_code === 401) {
            dispatch(getTransactionList(type, page, perPage));
        } else {
            localStorage.clear();
            router.replace("/login");
            Swal.fire({
                icon: "warning",
                title: "Oops...",
                text: "login session has ended, please login again!",
                confirmButtonColor: "#2e9afe",
            });
        }
    }

    React.useEffect(() => {
        getData(status);
    }, [status, page, perPage, token, status_code]);

    React.useEffect(() => {
        if (token === null) {
            localStorage.clear();
            router.replace("/login");
        }
        if (status_code === 401) {
            localStorage.clear();
            router.replace("/login");
            Swal.fire({
                icon: "warning",
                title: "Oops...",
                text: "login session has ended, please login again!",
                confirmButtonColor: "#2e9afe",
            });
        }
    }, [token, status_code]);

    function renderTable(type) {
        return (
            <div
                className="row rfml"
                style={{
                    overflowX: "scroll",
                    margin: 1,
                    borderWidth: 1,
                    borderColor: "black",
                }}
            >
                <Table
                    data={type === "inquiry" || type === "expired" ? data : data2}
                    columns={type === "inquiry" || type === "expired" ? columns : columns2}
                    setPage={(val) => {
                        SetPage(val);
                    }}
                    setPerPage={(val) => {
                        SetPerPage(val);
                    }}
                    currentpage={transaction_list?.currentPage}
                    perPage={perPage}
                    totalPage={transaction_list?.totalPages}
                />
            </div>
        );
    }

    return (
        <>
            {loading && <Loader />}
            <NavbarStyleConvert active="Transaction" />
            <div id="content-area" className="ptb-83">
                <div className="how-it-works-area margin-auto">
                    <div className="how-it-works-content">
                        <div className="row m-0">
                            <Menu active="Transaction" />
                            <div className="col-lg-9">
                                <div className="row">
                                    <div
                                        style={{ padding: 0 }}
                                        className={"col-lg-12"}>
                                        <div className="content bg-white radius-6 mt-50 p-5" style={{
                                            overflowX: 'scroll',
                                            borderWidth: 1,
                                            borderColor: 'black'
                                        }}>
                                            <Tabs>
                                                <TabList>
                                                    <Tab
                                                        onClick={() => {
                                                            SetStatus("inquiry");
                                                        }}
                                                    >
                                                        Unpaid
                                                    </Tab>
                                                    <Tab
                                                        onClick={() => {
                                                            SetStatus("settlement");
                                                        }}
                                                    >
                                                        Paid
                                                    </Tab>
                                                    <Tab
                                                        onClick={() => {
                                                            SetStatus("cancel");
                                                        }}
                                                    >
                                                        Canceled
                                                    </Tab>
                                                    <Tab
                                                        onClick={() => {
                                                            SetStatus("failed");
                                                        }}
                                                    >
                                                        Failed
                                                    </Tab>
                                                    <Tab
                                                        onClick={() => {
                                                            SetStatus("expired");
                                                        }}
                                                    >
                                                        Expired
                                                    </Tab>
                                                </TabList>
                                                <TabPanel>
                                                    {renderTable("inquiry")}
                                                </TabPanel>
                                                <TabPanel>
                                                    {renderTable("settlement")}
                                                </TabPanel>
                                                <TabPanel>
                                                    {renderTable("cancel")}
                                                </TabPanel>
                                                <TabPanel>
                                                    {renderTable("failed")}
                                                </TabPanel>
                                                <TabPanel>
                                                    {renderTable("expired")}
                                                </TabPanel>
                                            </Tabs>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <FooterStyleThree />
        </>
    );
};

export default AppList;

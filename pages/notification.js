import React from "react";
import NavbarStyleConvert from "@/components/_App/NavbarStyleConvert";
import Menu from "@/components/_App/Menu";
import FooterStyleThree from "@/components/_App/FooterStyleThree";
import NotificationForm from "@/components/Form/notification";
import NotificationTable from "@/components/Tables/notification";
import "@material/data-table/dist/mdc.data-table.css";
import "@rmwc/data-table/data-table.css";
import "@rmwc/icon/icon.css";
import { useDispatch, useSelector } from "react-redux";
import { getAppList, getProfile, loginClient } from "../actions";
import { useRouter } from "next/router";
import Swal from "sweetalert2";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
var localStorage = require("localStorage");

const AppList = () => {
    const router = useRouter();
    const dispatch = useDispatch();
    const status_code = useSelector((state) => state.profile.status_code);
    const token = localStorage.getItem("token") ?? null;
    const result = useSelector(state => state.app_list.result)
    const [app_selected, SetApp_selected] = React.useState('-');
    const [app_plan_id, SetApp_plan_id] = React.useState('-');
    const token_client = localStorage.getItem('token_client');
    const expires_in = localStorage.getItem('expires_in');

    React.useEffect(() => {
        if (token_client === null || new Date(expires_in) < new Date())
            dispatch(loginClient()).then((res) => {
                var expires_in = new Date(res.exp * 1000);
                expires_in.setHours(expires_in.getHours() - 6);
                localStorage.setItem("token_client", res.token);
                localStorage.setItem("expires_in", expires_in);
                window.location.reload()
            });
    }, []);

    React.useEffect(() => {
        if (token !== null || status_code === 401) {
            dispatch(getProfile(token))
                .then((res) => { })
                .catch((err) => {
                    console.log("err", err);
                    localStorage.clear();
                    router.replace("/login");
                    Swal.fire({
                        icon: "warning",
                        title: "Oops...",
                        text: "login session has ended, please login again!",
                        confirmButtonColor: "#2e9afe",
                    });
                });
        }
    }, [token, status_code]);

    const profile = {}
    React.useEffect(() => {
        if (token !== null | status_code === 401) {
            dispatch(getAppList(1, 999999999999))
        }
    }, [token, status_code]);

    return (
        <>
            <NavbarStyleConvert active="Push Notification" />
            <div id="content-area" className="ptb-83">
                <div className="how-it-works-area margin-auto">
                    <div className="how-it-works-content">
                        <div className="row m-0">
                            <Menu active="Push Notification" />
                            <div className="col-lg-9">
                                <div className="row">
                                    <div
                                        style={{ padding: 0 }}
                                        className={"col-lg-12"}>
                                        <div className="content bg-white radius-6 mt-50 p-5" style={{
                                            overflowX: 'scroll',
                                            borderWidth: 1,
                                            borderColor: 'black'
                                        }}>
                                            <div className="col-md-12 border-right">
                                                <div className="form-group mt-3">
                                                    <div className="col-md-12 mt-2 mb-2 v2">
                                                        <label className="labels mb-2">
                                                            Choose App
                                                        </label>
                                                        <select
                                                            className="form-control"
                                                            name="sex"
                                                            id="sex"
                                                            selected={app_selected}
                                                            onChange={(e) => {
                                                                SetApp_selected(e.target.value.split("|")[0])
                                                                SetApp_plan_id(e.target.value.split("|")[1])
                                                            }}
                                                        >
                                                            <option value="-|-">Select App</option>
                                                            {
                                                                result?.data.map((data) => {
                                                                    return (
                                                                        <option
                                                                            value={data.id + "|" + data.plan_id}
                                                                        >
                                                                            {data.app_package} - {data.app_name}
                                                                        </option>
                                                                    );
                                                                })
                                                            }
                                                        </select>
                                                    </div>
                                                </div>
                                                <Tabs style={{ marginTop: 40 }}>
                                                    <TabList>
                                                        <Tab>New Message</Tab>
                                                        <Tab>Message History</Tab>
                                                    </TabList>
                                                    <TabPanel>
                                                        <NotificationForm app_selected={app_selected} app_plan_id={app_plan_id} />
                                                    </TabPanel>
                                                    <TabPanel>
                                                        <NotificationTable app_selected={app_selected} app_plan_id={app_plan_id} />
                                                    </TabPanel>
                                                </Tabs>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <FooterStyleThree />
        </>
    );
};

export default AppList;

import React from "react";
import NavbarStyleConvert from "@/components/_App/NavbarStyleConvert";
import Menu from "@/components/_App/Menu";
import FooterStyleThree from "@/components/_App/FooterStyleThree";
import Step1 from "@/components/Form/Step-1";
import Step2 from "@/components/Form/Step-2";
import Step3 from "@/components/Form/Step-3";
import Step4 from "@/components/Form/Step-4";
import Step5 from "@/components/Form/Step-5";
import Navigation from "@/components/Form/Navigation";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import Swal from "sweetalert2";
import { MasterAppCreate } from "actions";
var localStorage = require("localStorage");
import { useRouter } from "next/router";
import { loginClient, getPlan, getProfile } from "actions";
import FadeIn from "@/components/Common/FadeIn";
import Resizer from "react-image-file-resizer";
import Modal from "react-modal";
import Loader from "@/components/_App/Loader";

const Convert = () => {
    const [step, setStep] = React.useState(1);
    const [plan, setPlan] = React.useState(1);
    const [count, setCount] = React.useState(2);
    const [dataForm, setDataForm] = React.useState({});
    const [previewBg_Image, setPreviewBg_Image] = React.useState(null);
    const [previewBg_Image_Ori, setPreviewBg_Image_Ori] = React.useState(null);
    const [previewApp_icon, setPreviewApp_icon] = React.useState(null);
    const [previewApp_icon_Ori, setPreviewApp_icon_Ori] = React.useState(null);
    const [modal, OpenModal] = React.useState(false);
    const [loading, SetLoading] = React.useState(false);
    const [previewBg_Image_OB, setPreviewBg_Image_OB] = React.useState(null);
    const [previewBg_Image_Ori_OB, setPreviewBg_Image_Ori_OB] =
        React.useState(null);
    const [bg_color_ob, setBgColorOB] = React.useState("#333333");
    const [btn_color_ob, setBtnColorOB] = React.useState("#35a0f9");
    const [btn_text_color_ob, setBtnTextColorOB] = React.useState("#FFFFFF");
    const [check_txt_color, SetCheck_txt_color] = React.useState("#FFFFFF");
    const [countDesc, setCountDesc] = React.useState(0);
    const [editor, setEditor] = React.useState(false);

    const {
        register,
        handleSubmit,
        setError,
        reset,
        watch,
        setValue,
        clearErrors,
        formState: { errors },
    } = useForm();
    const dispatch = useDispatch();
    const [color, setColor] = React.useState("#FFFFFF");
    const [success, onSuccess] = React.useState(false);
    const router = useRouter();
    const [orientation_screen, SetOrientation_screen] =
        React.useState("potrait");

    const token_client = localStorage.getItem("token_client") ?? null;
    const expires_in = localStorage.getItem("expires_in") ?? null;
    const token = localStorage.getItem("token") ?? null;

    React.useEffect(() => {
        if (token_client === null || new Date(expires_in) < new Date())
            dispatch(loginClient()).then((res) => {
                var expires_in = new Date(res.exp * 1000);
                expires_in.setHours(expires_in.getHours() - 6);
                localStorage.setItem("token_client", res.token);
                localStorage.setItem("expires_in", expires_in);
                window.location.reload();
            });
    }, []);

    React.useEffect(() => {
        const subscription = watch(async (value, { name, type }) => {

            if (name === "btn_radius") {
                var val = parseInt(value.btn_radius)
                if (val < 0) {
                    setValue('btn_radius', 0)
                } else if (val > 25) {
                    setValue('btn_radius', 25)
                }
            }

            if (name === "bg_image" && value.bg_image?.length > 0) {
                const image = await resizeFile(value.bg_image[0]);
                setEditor(true)
                setPreviewBg_Image_Ori(image);
            }

            if (name === "app_icon" && value.app_icon.length > 0) {
                if (value.app_icon.length > 0) {
                    const image = await resizeFile(value.app_icon[0]);
                    setEditor(true)
                    setPreviewApp_icon_Ori(image);
                }
            }

            if (name === "bg_image_ob" && value.bg_image_ob.length > 0) {
                const image = await resizeFile(value.bg_image_ob[0]);
                setEditor(true)
                setPreviewBg_Image_Ori_OB(image);
            }


            if (name === "screen_position_ob") {
                if (value.screen_position_ob === 'cover') {
                    setTimeout(
                        () => {
                            setValue('snap_image_ob', 'center')
                        },
                        100
                    );
                } else if (value.screen_position_ob === 'auto_height') {
                    setTimeout(
                        () => {
                            setValue('snap_image_ob', 'top')
                        },
                        100
                    );
                } else if (value.screen_position_ob === 'auto_width') {
                    setTimeout(
                        () => {
                            setValue('snap_image_ob', 'left')
                        },
                        100
                    );
                } else {
                    setTimeout(
                        () => {
                            setValue('snap_image_ob', '')
                        },
                        100
                    );
                }
            }

            if (name === "screen_position") {
                if (value.screen_position === 'cover') {
                    setTimeout(
                        () => {
                            setValue('snap_image', 'center')
                        },
                        100
                    );
                } else if (value.screen_position === 'auto_height') {
                    setTimeout(
                        () => {
                            setValue('snap_image', 'top')
                        },
                        100
                    );
                } else if (value.screen_position === 'auto_width') {
                    setTimeout(
                        () => {
                            setValue('snap_image', 'left')
                        },
                        100
                    );
                } else {
                    setTimeout(
                        () => {
                            setValue('snap_image', '')
                        },
                        100
                    );
                }
            }


            var validator = [
                "domain_name",
                "domain_name_base",
                "bg_image",
                "screen_position",
                "snap_image",
                "screen_animation_fade",
                "app_position",
            ];

            const found = validator.find((o) => o == name);
            if (found !== undefined) {
                setCount(value.screen_animation_fade);
            }

            setDataForm(value);
        });
        return () => subscription.unsubscribe();
    }, [watch]);

    React.useEffect(() => {
        setCount(dataForm?.screen_animation_fade ?? 2);
    }, [color, previewBg_Image]);

    React.useEffect(() => {
        setValue("btn_txt", 'Next');
        setValue("btn_radius", 0);
        setValue("onboarding_descriptions", "")
        setValue("screen_animation_fade", 2);
        setValue("domain_name_base", "https://");
        setValue("app_oriantation", "lock_vertical");
        setValue("screen_oriantation", "lock_vertical");

        setValue("screen_oriantation_ob", 'autorotate');
        setValue("screen_position_ob", 'cover');
        setValue("snap_image_ob", 'center');

        setValue("screen_position", "cover");
        setValue("snap_image", "center");
    }, []);

    React.useEffect(() => {
        const interval = setInterval(() => {
            if (count > 0) {
                setCount(count - 1);
            }
            if (count < 1) {
                clearInterval(interval);
            }
        }, 1000);
        return () => clearInterval(interval);
    }, [count]);

    React.useEffect(() => {
        if (token !== null) {
            dispatch(getProfile(token))
                .then((res) => { })
                .catch((err) => {
                    console.log("err", err);
                    localStorage.clear();
                    router.replace("/login");
                    Swal.fire({
                        icon: "warning",
                        title: "Oops...",
                        text: "login session has ended, please login again!",
                        confirmButtonColor: "#2e9afe",
                    });
                });
        }
    }, [token]);


    function getData() {
        dispatch(getPlan())
            .then((res) => {
            })
            .catch((err) => {
                console.log("err", err);
                localStorage.clear();
                router.replace("/login");
                Swal.fire({
                    icon: "warning",
                    title: "Oops...",
                    text: "login session has ended, please login again!",
                    confirmButtonColor: "#2e9afe",
                });
            });
    }

    React.useEffect(() => {
        if (token_client !== null && new Date(expires_in) > new Date()) {
            getData();
        }
    }, [token_client, expires_in]);

    const resizeFile = (file) =>
        new Promise((resolve) => {
            Resizer.imageFileResizer(
                file,
                500,
                500,
                "PNG",
                100,
                0,
                (uri) => {
                    resolve(uri);
                },
                "base64"
            );
        });

    const onSubmit = (data) => {
        let err = Object.keys(errors).map((key) => [Number(key), errors[key]]);
        if (err.length == 0 && step < 5) {
            setStep(step + 1);
            window.location.href = "#Home";
        }

        if (step === 2 && previewBg_Image === null) {
            setStep(2);
            setError("bg_image", {
                type: "manual",
                message: "Background Image is required",
            });
        }

        if (step === 2 && editor) {
            setStep(2);
            setError("bg_image", {
                type: "manual",
                message: "Please crop image",
            });
        }

        if (step === 3 && previewBg_Image_OB === null) {
            setStep(3);
            setError("bg_image_ob", {
                type: "manual",
                message: "Background Image is required",
            });
        }

        if (step === 3 && editor) {
            setStep(3);
            setError("bg_image_ob", {
                type: "manual",
                message: "Please crop image",
            });
        }

        if (step === 4 && previewApp_icon === null) {
            setStep(4);
            setError("app_icon", {
                type: "manual",
                message: "App icon is required",
            });
        }

        if (step === 4 && editor) {
            setStep(4);
            setError("app_icon", {
                type: "manual",
                message: "Please crop image",
            });
        }

        if (step === 5) {
            SetLoading(true);
            let payload = data;
            payload["app_icon"] = previewApp_icon;
            payload["bg_image"] = previewBg_Image;
            payload["bg_color"] = color;
            payload["plan_id"] = plan;
            payload["domain_name"] =
                dataForm.domain_name_base + dataForm.domain_name;

            payload["onboarding"] = {
                screen_oriantation: dataForm.screen_oriantation_ob,
                bg_color: bg_color_ob,
                screen_position: dataForm.screen_position_ob,
                snap_image:
                    dataForm.screen_oriantation_ob === "cover"
                        ? "center"
                        : dataForm.snap_image_ob,
                btn_color: btn_color_ob,
                btn_txt: dataForm.btn_txt,
                btn_txt_color: btn_text_color_ob,
                btn_radius: dataForm.btn_radius,
                onboarding_descriptions: countDesc === 0 ? null : dataForm.onboarding_descriptions,
                check_txt_color: check_txt_color,
                bg_image: previewBg_Image_OB,
            };

            dataForm.domain_name_base;

            if (localStorage.getItem("email")) {
                payload["email"] = localStorage.getItem("email");
            }
            setTimeout(() => {
                dispatch(MasterAppCreate(payload))
                    .then((res) => {
                        SetLoading(false);
                        if (res) {
                            window.open(res);
                            setTimeout(
                                () => {
                                    window.location.replace("/app-list");
                                },
                                1000
                            );
                        } else {
                            onSuccess(true);
                            Swal.fire({
                                icon: "success",
                                title: "Success",
                                text: "We're building your app, check app list menu to see the progress and to download your app.",
                                confirmButtonColor: "#2e9afe",
                            });
                            localStorage.setItem("email", data.email);
                            reset();
                            setStep(1);
                        }
                    })
                    .catch((err) => {
                        SetLoading(false);
                        onSuccess(false);
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: err,
                            confirmButtonColor: "#2e9afe",
                        });
                    });
            }, 1000);
        }
    };

    function renderContentStep3() {
        if (countDesc === 0) {
            return (
                <div style={{
                    backgroundColor: 'transparent',
                    width: '100%',
                    height: '15%',
                    position: 'absolute',
                    bottom: 0,
                    borderTopRightRadius: 35,
                    borderTopLeftRadius: 35,
                    display: 'flex',
                    textAlign: 'center',
                    flexDirection: 'column',
                    flexWrap: 'wrap',
                    alignItems: 'center'
                }}>
                    <div style={{
                        position: 'absolute',
                        bottom: 60
                    }}>
                        <label class="container">
                            <span class="checkmark"></span>
                            <text style={{ marginLeft: 10, fontSize: 12, color: check_txt_color }}>Don't show this message again</text>
                        </label>
                    </div>
                    <button className="btn btn-transparent" style={{
                        backgroundColor: btn_color_ob,
                        color: btn_text_color_ob,
                        paddingTop: 4,
                        paddingBottom: 4,
                        paddingRight: 40,
                        paddingLeft: 40,
                        borderRadius: parseInt(dataForm.btn_radius),
                        fontWeight: 'bold',
                        fontSize: 16,
                        position: 'absolute',
                        bottom: 20
                    }}>
                        {dataForm.btn_txt}
                    </button>
                </div>
            );
        } else {
            return (
                <div style={{
                    backgroundColor: 'white',
                    width: '100%',
                    position: 'absolute',
                    minHeight: '26%',
                    maxHeight: '60%',
                    paddingBottom: '20%',
                    paddingLeft:'6%',
                    paddingRight:'6%',
                    bottom: 0,
                    borderTopRightRadius: 35,
                    borderTopLeftRadius: 35,
                    display: 'flex',
                    flexDirection: 'column',
                    flexWrap: 'wrap',
                }}>
                    <div dangerouslySetInnerHTML={{
                        __html: dataForm?.onboarding_descriptions
                    }} id={orientation_screen !== "potrait" ? "inner-right-landscape" : "inner-right"} style={{ textAlign: 'left', fontSize: 14, wordWrap: 'break-word' }}>
                    </div>
                    <div style={{
                        position: 'absolute',
                        alignSelf: 'center',
                        bottom: 60
                    }}>
                        <label class="container">
                            <span class="checkmark"></span>
                            <text style={{ marginLeft: 10, fontSize: 12, color: check_txt_color }}>Don't show this message again</text>
                        </label>
                    </div>
                    <button className="btn btn-transparent" style={{
                        backgroundColor: btn_color_ob,
                        color: btn_text_color_ob,
                        paddingTop: 4,
                        paddingBottom: 4,
                        paddingRight: 40,
                        alignSelf: 'center',
                        paddingLeft: 40,
                        borderRadius: parseInt(dataForm.btn_radius),
                        fontWeight: 'bold',
                        fontSize: 16,
                        position: 'absolute',
                        bottom: 15
                    }}>
                        {dataForm.btn_txt}
                    </button>
                </div>
            );
        }
    }

    function renderContentSimulator(type) {
        if (step === 1) {
            return (
                <div
                    className={
                        orientation_screen == "potrait"
                            ? "area-marvel-iframe"
                            : "area-marvel-iframe-landscape"
                    }
                >
                    {dataForm.domain_name_base +
                        dataForm.domain_name +
                        "/embed" && (
                            <iframe
                                onErrorCapture={(val) => {
                                    console.log(
                                        "onErrorCapture",
                                        val
                                    );
                                }}
                                onInvalid={(val) => {
                                    console.log(
                                        "onInvalid",
                                        val
                                    );
                                }}
                                onError={(val) => {
                                    console.log(
                                        "ifronError",
                                        val
                                    );
                                }}
                                src={
                                    dataForm.domain_name_base +
                                    dataForm.domain_name
                                }
                                style={{}}
                                className={
                                    orientation_screen ==
                                        "potrait"
                                        ? "marvel-iframe "
                                        : "marvel-iframe-landscape"
                                }
                            />
                        )}
                </div>
            );
        } else if (step === 2) {
            return (
                <FadeIn
                    style={{
                        backgroundColor: color,
                    }}
                    duration={
                        dataForm?.screen_animation_fade ??
                        0
                    }
                >
                    <div
                        className={`marvel-content-dd ${orientation_screen !==
                            "potrait" &&
                            orientation_screen !==
                            "autorotate"
                            ? "landscape"
                            : ""
                            }`}
                        style={{
                            paddingTop:
                                orientation_screen !==
                                    "potrait"
                                    ? 160
                                    : 0,
                            backgroundImage: `url(${previewBg_Image})`,
                            backgroundColor: color,
                            backgroundPosition:
                                dataForm?.snap_image,
                            backgroundRepeat:
                                "no-repeat",
                            backgroundSize:
                                dataForm?.screen_position ==
                                    "auto_height"
                                    ? "100% auto"
                                    : dataForm?.screen_position ==
                                        "auto_width" ? "auto 100%" : "cover",
                            transition:
                                "opacity 3000ms ease",
                        }}
                    ></div>
                </FadeIn>
            );
        } else if (step === 3) {
            return (
                <FadeIn
                    style={{
                        backgroundColor: bg_color_ob,
                    }}
                    duration={
                        dataForm?.screen_animation_fade ??
                        0
                    }
                >
                    <div
                        className={`marvel-content-dd ${orientation_screen !==
                            "potrait" &&
                            orientation_screen !==
                            "autorotate"
                            ? "landscape"
                            : ""
                            }`}
                        style={{
                            paddingTop:
                                orientation_screen !==
                                    "potrait"
                                    ? 160
                                    : 0,
                            backgroundImage: `url(${previewBg_Image_OB})`,
                            backgroundColor: bg_color_ob,
                            backgroundPosition:
                                dataForm?.snap_image_ob,
                            backgroundRepeat:
                                "no-repeat",
                            backgroundSize:
                                dataForm?.screen_position_ob ==
                                    "auto_height"
                                    ? "100% auto"
                                    : dataForm?.screen_position_ob ==
                                        "auto_width" ? "auto 100%" : "cover",
                            transition:
                                "opacity 3000ms ease",
                        }}
                    >
                        {renderContentStep3()}
                    </div>
                </FadeIn>
            );
        } else if (step === 4) {
            return (
                <div
                    className={`marvel-content-dd ${orientation_screen !==
                        "potrait" &&
                        orientation_screen !==
                        "autorotate"
                        ? "landscape"
                        : ""
                        }`}
                    style={{
                        width: 466,
                        height:
                            type === "phone"
                                ? 465
                                : 500,
                        backgroundColor: "grey",
                        flexDirection: "column",
                        display: "flex",
                        backgroundPosition: "center",
                        backgroundSize: "auto auto",
                    }}
                >
                    {previewApp_icon !== null && (
                        <img
                            src={previewApp_icon}
                            style={{
                                marginLeft: 20,
                                marginTop: 20,
                                width: 60,
                                height: 60,
                                objectFit: "contain",
                                aspectRatio: 4 / 4,
                                marginBottom: 1,
                                borderRadius: 10,
                            }}
                        />
                    )}
                    <div
                        style={{
                            marginLeft: 20,
                            textAlign: "center",
                            width: 60,
                            flexWrap: "wrap",
                            flexDirection:
                                "column-reverse",
                            alignContent: "center",
                            lineHeight: 1
                        }}
                    >
                        <text
                            style={{
                                color: "white",
                                fontSize: 12,
                            }}
                        >
                            {dataForm?.app_name}
                        </text>
                    </div>
                </div>
            );
        }
    }

    function renderSimulator(type) {
        return (
            <center>
                <div
                    className={
                        "col area-simulator" +
                        (type === "web" ? "  fixed-preview" : " ")
                    }
                    style={{}}
                >
                    <div className="">
                        <div className="col-9">
                            <div
                                style={{
                                    position: "relative",
                                    marginLeft: "-16%",
                                    marginTop: "0",
                                }}
                                class={`marvel-device note8 ${orientation_screen !== "potrait" &&
                                    orientation_screen !== "autorotate"
                                    ? "landscape"
                                    : ""
                                    }`}
                            >
                                <div class="inner"></div>
                                <div class="overflow">
                                    <div class="shadow"></div>
                                </div>
                                <div class="speaker"></div>
                                <div class="screen">
                                    {renderContentSimulator(type)}
                                </div>
                            </div>
                        </div>
                        <br />
                        <center
                            style={{
                                fontSize: 12,
                                color: type === "web" ? "#3333333" : "#Fefefe",
                            }}
                        >
                            {step === 1 && <div>
                                <text>
                                    Preview can't load some URL due X-Frame-Options,
                                </text>
                                <br />
                                <text>
                                    but URL will be rendered normally in final app
                                </text>
                            </div>}
                            <br />
                        </center>
                        <div className="col-3 hidden-only-mobile">
                            <img
                                onClick={() => {
                                    if (orientation_screen === "potrait") {
                                        SetOrientation_screen("ladscape");
                                    } else {
                                        SetOrientation_screen("potrait");
                                    }
                                }}
                                src="/images/smartphone.png"
                                alt="logo"
                                width="30"
                                height="30"
                                style={{
                                }}
                            />
                        </div>
                    </div>
                </div>
                {type === "phone" && (
                    <div>
                        <img
                            onClick={() => {
                                OpenModal(false);
                            }}
                            src="/images/close.png"
                            alt="logo"
                            width="40"
                            height="40"
                            style={{
                                marginRight: 20,
                                marginTop: 10,
                                marginBottom: 5,
                            }}
                        />
                        <img
                            onClick={() => {
                                if (orientation_screen === "potrait") {
                                    SetOrientation_screen("ladscape");
                                } else {
                                    SetOrientation_screen("potrait");
                                }
                            }}
                            src="/images/refresh.png"
                            alt="logo"
                            width="40"
                            height="40"
                            style={{
                                marginTop: 10,
                                marginBottom: 5,
                            }}
                        />
                    </div>
                )}
            </center>
        );
    }

    return (
        <>
            {loading && <Loader />}
            {modal === false && <NavbarStyleConvert active="App Builder" />}
            <div id="Home" className="ptb-83">
                <div className="how-it-works-area">
                    <div className="how-it-works-content">
                        <div className="row m-0">
                            <Menu active="App Builder" />
                            <div className="col-lg-9">
                                <div className="row">
                                    <div
                                        style={{ padding: 0 }}
                                        className={
                                            step < 5 ? "col-lg-7" : "col-lg-12"
                                        }
                                    >
                                        <div className="content bg-white radius-6 mt-50 p-5">
                                            <div className="row">
                                                <div
                                                    className={
                                                        "number " +
                                                        (step > 0
                                                            ? "active"
                                                            : "")
                                                    }
                                                >
                                                    1
                                                </div>
                                                <div style={{ padding: 0 }} className="col border-line" />
                                                <div
                                                    className={
                                                        "number " +
                                                        (step > 1
                                                            ? "active"
                                                            : "")
                                                    }
                                                >
                                                    2
                                                </div>
                                                <div style={{ padding: 0 }} className="col border-line" />
                                                <div
                                                    className={
                                                        "number " +
                                                        (step > 2
                                                            ? "active"
                                                            : "")
                                                    }
                                                >
                                                    3
                                                </div>
                                                <div style={{ padding: 0 }} className="col border-line" />
                                                <div
                                                    className={
                                                        "number " +
                                                        (step > 3
                                                            ? "active"
                                                            : "")
                                                    }
                                                >
                                                    4
                                                </div>
                                                <div style={{ padding: 0 }} className="col border-line" />
                                                <div
                                                    className={
                                                        "number " +
                                                        (step > 4
                                                            ? "active"
                                                            : "")
                                                    }
                                                >
                                                    5
                                                </div>
                                            </div>
                                            <div className="mt-30m">
                                                <p>
                                                    Follow these some simple
                                                    steps to setup your app
                                                </p>
                                                <form
                                                    onSubmit={handleSubmit(
                                                        onSubmit
                                                    )}
                                                >
                                                    <Step1
                                                        register={register}
                                                        success={success}
                                                        errors={errors}
                                                        step={step}
                                                    />
                                                    <Step2
                                                        editor={editor}
                                                        setEditor={(val) => {
                                                            setEditor(val)
                                                            clearErrors('bg_image');
                                                        }}
                                                        onCancel={() => {
                                                            clearErrors('bg_image');
                                                            setEditor(false)
                                                            setPreviewBg_Image(null)
                                                            setPreviewBg_Image_Ori(null)
                                                            var data = dataForm
                                                            delete data.bg_image
                                                            reset()
                                                            reset(dataForm)
                                                        }}
                                                        dataForm={dataForm}
                                                        previewBg_Image_Ori={
                                                            previewBg_Image_Ori
                                                        }
                                                        previewBg_Image={
                                                            previewBg_Image
                                                        }
                                                        setPreviewBg_Image={(
                                                            val
                                                        ) => {
                                                            setPreviewBg_Image(
                                                                val
                                                            );
                                                        }}
                                                        color={color}
                                                        setColor={(val) => {
                                                            setColor(val);
                                                        }}
                                                        disableSnap={
                                                            dataForm?.screen_position ===
                                                                "cover"
                                                                ? true
                                                                : false
                                                        }
                                                        register={register}
                                                        snap_imageData={
                                                            !dataForm?.screen_position
                                                                ? []
                                                                : dataForm?.screen_position ===
                                                                    "auto_height"
                                                                    ? [
                                                                        {
                                                                            value: "top",
                                                                            label: "Top",
                                                                        },
                                                                        {
                                                                            value: "center",
                                                                            label: "Center",
                                                                        },
                                                                        {
                                                                            value: "bottom",
                                                                            label: "Bottom",
                                                                        },
                                                                    ]
                                                                    : [
                                                                        {
                                                                            value: "left",
                                                                            label: "Left",
                                                                        },
                                                                        {
                                                                            value: "center",
                                                                            label: "Center",
                                                                        },
                                                                        {
                                                                            value: "right",
                                                                            label: "Right",
                                                                        },
                                                                    ]
                                                        }
                                                        errors={errors}
                                                        step={step}
                                                    />
                                                    <Step3
                                                        editor={editor}
                                                        setEditor={(val) => {
                                                            setEditor(val)
                                                            clearErrors('bg_image_ob');
                                                        }}
                                                        onCancel={() => {
                                                            clearErrors('bg_image_ob');
                                                            setEditor(false)
                                                            setPreviewBg_Image_OB(null)
                                                            setPreviewBg_Image_Ori_OB(null)
                                                            var data = dataForm
                                                            delete data.bg_image_ob
                                                            reset()
                                                            reset(dataForm)
                                                        }}
                                                        countDesc={countDesc}
                                                        setCountDesc={(val) => {
                                                            setCountDesc(val)
                                                        }}
                                                        dataForm={dataForm}
                                                        onboarding_descriptions={dataForm.onboarding_descriptions}
                                                        setOnboarding_descriptions={(val) => {
                                                            setValue("onboarding_descriptions", val)
                                                        }}
                                                        previewBg_Image_Ori_OB={
                                                            previewBg_Image_Ori_OB
                                                        }
                                                        previewBg_Image_OB={
                                                            previewBg_Image_OB
                                                        }
                                                        setPreviewBg_Image_OB={(
                                                            val
                                                        ) => {
                                                            setPreviewBg_Image_OB(
                                                                val
                                                            );
                                                        }}
                                                        bg_color_ob={
                                                            bg_color_ob
                                                        }
                                                        setBgColorOB={(val) => {
                                                            setBgColorOB(val);
                                                        }}
                                                        btn_color_ob={
                                                            btn_color_ob
                                                        }
                                                        setBtnColorOB={(
                                                            val
                                                        ) => {
                                                            setBtnColorOB(val);
                                                        }}
                                                        btn_text_color_ob={
                                                            btn_text_color_ob
                                                        }
                                                        setBtnTextColorOB={(
                                                            val
                                                        ) => {
                                                            setBtnTextColorOB(
                                                                val
                                                            );
                                                        }}
                                                        check_txt_color={check_txt_color}
                                                        SetCheck_txt_color={(val) => {
                                                            SetCheck_txt_color(val)
                                                        }}
                                                        disableSnap={
                                                            dataForm?.screen_position_ob ===
                                                                "cover"
                                                                ? true
                                                                : false
                                                        }
                                                        register={register}
                                                        snap_imageData={
                                                            !dataForm?.screen_position_ob
                                                                ? []
                                                                : dataForm?.screen_position_ob ===
                                                                    "auto_height"
                                                                    ? [
                                                                        {
                                                                            value: "top",
                                                                            label: "Top",
                                                                        },
                                                                        {
                                                                            value: "center",
                                                                            label: "Center",
                                                                        },
                                                                        {
                                                                            value: "bottom",
                                                                            label: "Bottom",
                                                                        },
                                                                    ]
                                                                    : dataForm?.screen_position_ob ===
                                                                        "auto_width"
                                                                        ? [
                                                                            {
                                                                                value: "left",
                                                                                label: "Left",
                                                                            },
                                                                            {
                                                                                value: "center",
                                                                                label: "Center",
                                                                            },
                                                                            {
                                                                                value: "right",
                                                                                label: "Right",
                                                                            },
                                                                        ]
                                                                        : [
                                                                            {
                                                                                value: "center",
                                                                                label: "Center",
                                                                            },
                                                                        ]
                                                        }
                                                        errors={errors}
                                                        step={step}
                                                    />
                                                    <Step4
                                                        editor={editor}
                                                        setEditor={(val) => {
                                                            setEditor(val)
                                                            clearErrors('app_icon');
                                                        }}
                                                        onCancel={() => {
                                                            clearErrors('app_icon');
                                                            setEditor(false)
                                                            setPreviewApp_icon(null)
                                                            setPreviewApp_icon_Ori(null)
                                                            var data = dataForm
                                                            delete data.app_icon
                                                            reset()
                                                            reset(dataForm)
                                                        }}
                                                        dataForm={dataForm}
                                                        register={register}
                                                        errors={errors}
                                                        step={step}
                                                        previewApp_icon_Ori={
                                                            previewApp_icon_Ori
                                                        }
                                                        previewApp_icon={
                                                            previewApp_icon
                                                        }
                                                        setPreviewApp_icon={(
                                                            val
                                                        ) => {
                                                            setPreviewApp_icon(
                                                                val
                                                            );
                                                        }}
                                                    />
                                                    <Step5
                                                        register={register}
                                                        errors={errors}
                                                        step={step}
                                                        plan={plan}
                                                        setPlan={(val) => {
                                                            setPlan(val);
                                                        }}
                                                    />
                                                    <Navigation
                                                        register={register}
                                                        errors={errors}
                                                        step={step}
                                                        openPreview={() => {
                                                            OpenModal(true);
                                                        }}
                                                        setStep={(val) => {
                                                            setStep(val);
                                                        }}
                                                    />
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    {step < 5 && (
                                        <div className="col-lg-5 hidden-only-mobile">
                                            <div className="content" style={{ marginTop: 20 }}>
                                                {renderSimulator("web")}
                                            </div>
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal
                    isOpen={modal}
                    onRequestClose={() => {
                        OpenModal(false);
                    }}
                    style={customStyles}
                    contentLabel="Example Modal"
                >
                    <center>
                        <div>
                            {renderSimulator("phone")}
                            {orientation_screen === "potrait" && (
                                <text
                                    style={{
                                        color: "white",
                                        fontSize: 12,
                                        marginTop: 20,
                                    }}
                                >
                                    Please rotate your device
                                </text>
                            )}
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                        </div>
                    </center>
                </Modal>
            </div>
            <FooterStyleThree />
        </>
    );
};

const customStyles = {
    content: {
        top: "50%",
        left: "50%",
        right: "auto",
        bottom: "auto",
        marginRight: "-50%",
        transform: "translate(-50%, -50%)",
        width: "100%",
        height: "100%",
        backgroundColor: "#333333",
    },
    overlay: {
        backgroundColor: "rgba(0,0,0,0.5)",
    },
};

export default Convert;

import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
    render() {
        return (
            <Html lang="zxx">
                <Head>
                    <link rel="icon" type="image/png" href="/images/favicon.ico"></link>
                    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
                    <meta name="keywords" content="provide the following services: app builder, web to app convertor, web to android conversion, website to android conversion, app creator, app converter, android builder, website to app builder, app for website, website to iPhone, app creation, app maker, Shopify app store maker, Shopify to app maker, web to ios builder, apps builder, website to apk convertor, website to ios convertor, apk builder, web to iPhone conversion, ios builder, website to mobile app convertor, website into app convertoer, web to mobile app conversion, wix to app convertor, shopify store to app convertor, apps for shopify store, web & mobile builder" />

                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
                
            </Html>
        )
    }
}

export default MyDocument;
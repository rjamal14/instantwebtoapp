import React from "react";
import NavbarStyleConvert from "@/components/_App/NavbarStyleConvert";
import Menu from "@/components/_App/Menu";
import FooterStyleThree from "@/components/_App/FooterStyleThree";
import '@material/data-table/dist/mdc.data-table.css'
import '@rmwc/data-table/data-table.css'
import '@rmwc/icon/icon.css'
import Table from "../../components/Common/table";
import { useDispatch, useSelector } from "react-redux";
import { getAppList, getProfile, getTransactionList, loginClient } from "../../actions";
var localStorage = require('localStorage')
import { useRouter } from 'next/router'
import Swal from "sweetalert2";

const AppList = () => {
  const dispatch = useDispatch();
  const token_client = localStorage.getItem('token_client');
  const expires_in = localStorage.getItem('expires_in');
  const [page, SetPage] = React.useState(1);
  const [perPage, SetPerPage] = React.useState(10);
  const result = useSelector(state => state.app_list.result)
  const status_code = useSelector(state => state.app_list.status_code)
  const token = localStorage.getItem('token') ?? null;
  const show_unpaid = localStorage.getItem('show_unpaid') ?? null;
  const email = localStorage.getItem('email') ?? null;
  const router = useRouter()
  const transaction_unpaid = useSelector((state) => state.transaction.transaction_unpaid);

  React.useEffect(() => {
    if (token_client === null || new Date(expires_in) < new Date())
      dispatch(loginClient()).then((res) => {
        var expires_in = new Date(res.exp * 1000);
        expires_in.setHours(expires_in.getHours() - 6);
        localStorage.setItem("token_client", res.token);
        localStorage.setItem("expires_in", expires_in);
        window.location.reload()
      });
  }, []);

  function renderStatus(params) {
    if (params === 'active') {
      return 'Active'
    } else if (params === 'draft') {
      return 'Draft'
    } else {
      return '-'
    }
  }

  const list = result?.data?.map((i, index) => {
    return {
      col1: <div>{(((page - 1) * perPage) + (index + 1)).toString()}</div>,
      col1: <div style={{ marginRight: 20, marginLeft: 20 }}>{(index + 1).toString()}</div>,
      col2: <div style={{ minWidth: 165 }}>{i?.app_package}</div>,
      col3: <div style={{ minWidth: 100 }}>{i?.app_name}</div>,
      col4: <div style={{ minWidth: 170 }}>{i?.domain_name}</div>,
      col5: <div style={{ minWidth: 80 }}>{i?.app_version}</div>,
      col6: <div style={{ minWidth: 80 }}>{i?.plan?.name ?? '-'}</div>,
      col7: <div style={{ minWidth: 80 }}>{renderStatus(i?.status)}</div>,
      col8: i?.id,
    };
  });

  const data = React.useMemo(() => list, [result.data]);
  const columns = React.useMemo(
    () => [
      {
        Header: 'No',
        accessor: 'col1',
        textAlign: 'center',
        width: '8%',
      },
      {
        Header: 'Package',
        accessor: 'col2',
        textAlign: 'left',
      },
      {
        Header: 'Name',
        accessor: 'col3',
        textAlign: 'left',
      },
      {
        Header: 'Domain',
        accessor: 'col4',
        textAlign: 'left',
      },
      {
        Header: 'Version',
        accessor: 'col5',
        textAlign: 'left',
      },
      {
        Header: 'Plan',
        accessor: 'col6',
        textAlign: 'left',
      },
      {
        Header: 'Status',
        accessor: 'col7',
        textAlign: 'left',
      },
      {
        Header: 'Detail',
        accessor: 'col8',
        textAlign: 'left',
        Cell: ({ cell }) => (
          <div style={{ minWidth: 100 }}>
            <button className="btn btn-primary profile-button" onClick={() => {
              router.push(`app-list/${cell.value}/manage-app`)
            }}>Manage</button>
          </div>
        ),
      },
    ],
    [result.data]
  );

  React.useEffect(() => {
    if (transaction_unpaid.data.length > 0) {
      if (show_unpaid == 1) {
        Swal.fire({
          title: ``,
          text: `Hi ${email}, \nYou have ${transaction_unpaid?.data.length} unpaid ${transaction_unpaid?.data.length > 1 ? 'transactions' : 'transaction'}`,
          icon: 'warning',
          allowOutsideClick: false,
          showCancelButton: true,
          confirmButtonColor: '#79D156',
          cancelButtonColor: '#FF4466',
          confirmButtonText: 'See',
          cancelButtonText: 'Close'

        }).then((result) => {
          if (result.isConfirmed) {
            router.replace('/transaction')
          }
          localStorage.setItem("show_unpaid", 0);
        })
      }
    }
  }, [show_unpaid, transaction_unpaid])

  React.useEffect(() => {
    if (token !== null || status_code === 401) {
      dispatch(getAppList(page, perPage))
      dispatch(getTransactionList('inquiry', page, perPage))
    }
  }, [page, perPage, token, status_code])

  React.useEffect(() => {
    if (token === null) {
      localStorage.clear();
      router.replace('/login')
    }
    if (status_code === 401) {
      localStorage.clear();
      router.replace('/login')
      Swal.fire({
        icon: "warning",
        title: "Oops...",
        text: "login session has ended, please login again!",
        confirmButtonColor: "#2e9afe",
      });
    }
  }, [token, status_code])

  React.useEffect(() => {
    if (token !== null || status_code === 401) {
      dispatch(getProfile(token))
        .then((res) => { })
        .catch((err) => {
          console.log("err", err);
          localStorage.clear();
          router.replace("/login");
          Swal.fire({
            icon: "warning",
            title: "Oops...",
            text: "login session has ended, please login again!",
            confirmButtonColor: "#2e9afe",
          });
        });
    }
  }, [token, status_code]);

  return (
    <>
      <NavbarStyleConvert active="App List" />
      <div id="content-area" className="ptb-83">
        <div className="how-it-works-area margin-auto">
          <div className="how-it-works-content">
            <div className="row m-0">
              <Menu active="App List" />
              <div className="col-lg-9">
                <div className="row">
                  <div
                    style={{ padding: 0 }}
                    className={"col-lg-12"}>
                    <div className="content bg-white radius-6 mt-50 p-5" style={{
                      overflowX: 'scroll',
                      borderWidth: 1,
                      borderColor: 'black'
                    }}>
                      <Table
                        data={data}
                        columns={columns}
                        setPage={(val) => {
                          SetPage(val)
                        }}
                        setPerPage={(val) => {
                          SetPerPage(val)
                        }}
                        currentpage={result?.currentPage}
                        perPage={perPage}
                        totalPage={result?.totalPages}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <FooterStyleThree />
    </>
  );
};

export default AppList;

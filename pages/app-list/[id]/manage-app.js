import React from "react";
import NavbarStyleConvert from "@/components/_App/NavbarStyleConvert";
import ManageAppPages from "@/components/Pages/manage-app";
import AppCredentialPages from "@/components/Pages/appCredential";
import AppiosCredential from "@/components/Pages/iosCredential";
import Menu from "@/components/_App/Menu";
import FooterStyleThree from "@/components/_App/FooterStyleThree";
import "@material/data-table/dist/mdc.data-table.css";
import "@rmwc/data-table/data-table.css";
import "@rmwc/icon/icon.css";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import { useDispatch } from "react-redux";
import { loginClient } from "actions";
var localStorage = require("localStorage");

const ManageApp = () => {

    const dispatch = useDispatch();
    const token_client = localStorage.getItem('token_client');
    const expires_in = localStorage.getItem('expires_in');

    React.useEffect(() => {
        if (token_client === null || new Date(expires_in) < new Date())
            dispatch(loginClient()).then((res) => {
                var expires_in = new Date(res.exp * 1000);
                expires_in.setHours(expires_in.getHours() - 6);
                localStorage.setItem("token_client", res.token);
                localStorage.setItem("expires_in", expires_in);
                window.location.reload()
            });
    }, []);

    return (
        <>
            <NavbarStyleConvert active="App List" />
            <div id="content-area" className="ptb-83">
                <div className="how-it-works-area margin-auto">
                    <div className="how-it-works-content">
                        <div className="row m-0">
                            <Menu active="App List" />
                            <div className="col-lg-9">
                                <div className="row">
                                    <div
                                        style={{ padding: 0 }}
                                        className={"col-lg-12"}>
                                        <div className="mt-50" style={{
                                            overflowX: 'scroll',
                                            borderWidth: 1,
                                            borderColor: 'black'
                                        }}>

                                            <Tabs>
                                                <TabList>
                                                    <Tab>Manage app</Tab>
                                                    <Tab>Push Notif Config</Tab>
                                                    <Tab>iOs IPA Config</Tab>
                                                </TabList>
                                                <TabPanel>
                                                    <ManageAppPages />
                                                </TabPanel>
                                                <TabPanel>
                                                    <AppCredentialPages />
                                                </TabPanel>
                                                <TabPanel>
                                                    <AppiosCredential />
                                                </TabPanel>
                                            </Tabs>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <FooterStyleThree />
        </>
    );
};

export default ManageApp;

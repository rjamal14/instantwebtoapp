import React from "react";
import Plan from "@/components/Form/Plan";
import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import Swal from "sweetalert2";
import { MasterAppChangePlan } from "actions";
var localStorage = require("localStorage");
import { useRouter } from "next/router";
import { loginClient, getPlan, getProfile, getDetailAppList } from "actions";

const ChangePlan = ({ onRequestClose, app_id, SetLoading }) => {
    const [step, setStep] = React.useState(1);
    const [plan, setPlan] = React.useState(null);
    const [previewApp_icon_Ori, setPreviewApp_icon_Ori] = React.useState(null);

    const {
        register,
        handleSubmit,
        setError,
        clearErrors,
        reset,
        watch,
        setValue,
        formState: { errors },
    } = useForm();
    const dispatch = useDispatch();
    const [color, setColor] = React.useState("#FFFFFF");
    const [success, onSuccess] = React.useState(false);
    const router = useRouter();
    const [orientation_screen, SetOrientation_screen] =
        React.useState("potrait");

    const token_client = localStorage.getItem("token_client") ?? null;
    const expires_in = localStorage.getItem("expires_in") ?? null;
    const token = localStorage.getItem("token") ?? null;
    const status_code = useSelector((state) => state.app_list.status_code);
    const detail = useSelector((state) => state.app_list.detail);

    React.useEffect(() => {
        if (token !== null || status_code === 401) {
            const id = window.location.pathname.split("/")[2] || app_id;
            dispatch(getDetailAppList(id));
        }
    }, [token, status_code]);

    React.useEffect(() => {
        if (token_client === null || new Date(expires_in) < new Date())
            dispatch(loginClient()).then((res) => {
                var expires_in = new Date(res.exp * 1000);
                expires_in.setHours(expires_in.getHours() - 6);
                localStorage.setItem("token_client", res.token);
                localStorage.setItem("expires_in", expires_in);
                window.location.reload();
            });
    }, []);

    React.useEffect(() => {
        if (token !== null) {
            dispatch(getProfile(token))
                .then((res) => { })
                .catch((err) => {
                    console.log("err", err);
                    localStorage.clear();
                    router.replace("/login");
                    Swal.fire({
                        icon: "warning",
                        title: "Oops...",
                        text: "login session has ended, please login again!",
                        confirmButtonColor: "#2e9afe",
                    });
                });
        }
    }, [token]);

    function getData() {
        dispatch(getPlan())
            .then((res) => {
            })
            .catch((err) => {
                console.log("err", err);
                localStorage.clear();
                router.replace("/login");
                Swal.fire({
                    icon: "warning",
                    title: "Oops...",
                    text: "login session has ended, please login again!",
                    confirmButtonColor: "#2e9afe",
                });
            });
    }

    React.useEffect(() => {
        if (token_client !== null && new Date(expires_in) > new Date()) {
            getData();
        }
    }, [token_client, expires_in]);

    const onSubmit = () => {

        if (plan !== null) {
            SetLoading(true)
            dispatch(MasterAppChangePlan({ plan_id: plan }, detail.id))
                .then((res) => {
                    SetLoading(false)
                    if (res) {
                        onRequestClose()
                        window.open(res);
                    }
                })
                .catch((err) => {
                    SetLoading(false)
                    onSuccess(false);
                    Swal.fire({
                        title: 'Oops...',
                        text: err,
                        icon: 'error',
                        showCancelButton: true,
                        allowOutsideClick: false,
                        cancelButtonColor: '#FF4466',
                        confirmButtonColor: '#2e9afe',
                        confirmButtonText: 'See invoice',
                        cancelButtonText: 'Close',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            router.replace('/transaction')
                        }
                    })
                });
        } else {
            setError("plan_id", {
                type: "manual",
                message: "Plan is required",
            });
        }
    };

    return (
        <>
            <div id="content-area">
                <div className="">
                    <div className="">
                        <div className="row m-0">
                            {/* <Menu active="App List" /> */}
                            <div className="col-lg-12">
                                <div className="row">
                                    <div
                                        className="col-lg-12"
                                        style={{
                                            paddingLeft: "3%",
                                        }}
                                    >
                                        <div className="">
                                            <div className="row">
                                                <form
                                                    onSubmit={handleSubmit(
                                                        onSubmit
                                                    )}
                                                >
                                                    <Plan
                                                        plan={plan}
                                                        plan_id={detail.plan_id}
                                                        setPlan={(val) => {
                                                            setPlan(val)
                                                            clearErrors('plan_id')
                                                        }}
                                                        register={register}
                                                        errors={errors}
                                                        step={step}
                                                        previewApp_icon={
                                                            previewApp_icon_Ori
                                                        }
                                                        setPreviewApp_icon={(
                                                            val
                                                        ) => {
                                                            setPreviewApp_icon(
                                                                val
                                                            );
                                                        }}
                                                    />
                                                    <div>
                                                        <span
                                                            style={{ marginBottom: 10, marginRight: 16 }}
                                                            onClick={onRequestClose}
                                                            type="submit"
                                                            className="btn profile-button-disable mt-20 "
                                                        >
                                                            Close
                                                        </span>
                                                        <button
                                                            style={{ marginBottom: 10 }}
                                                            type="submit"
                                                            className="btn profile-button white mt-20"
                                                        >
                                                            Pay using paypal
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* <FooterStyleThree /> */}
        </>
    );
};

export default ChangePlan;

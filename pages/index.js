import React from "react";
import MainBanner from "@/components/Landing/MainBanner";
import FooterStyleThree from "@/components/_App/FooterStyleTwo";
import NavbarStyleOne from "@/components/_App/NavbarStyleOne";
import PricingPlanStyle1 from "@/components/PricingPlan/PricingPlanStyle5";
import Benefit from "@/components/Landing/Benefit";
import Faq from "@/components/Common/faq";
import Features from "@/components/Landing/Features";
import { useDispatch } from "react-redux";
import { loginClient } from "actions";
var localStorage = require("localStorage");

const IndexPage5 = () => {
    
    const dispatch = useDispatch();
    const token_client = localStorage.getItem('token_client');
    const expires_in = localStorage.getItem('expires_in');

    React.useEffect(() => {
        if (token_client === null || new Date(expires_in) < new Date())
            dispatch(loginClient()).then((res) => {
                var expires_in = new Date(res.exp * 1000);
                expires_in.setHours(expires_in.getHours() - 6);
                localStorage.setItem("token_client", res.token);
                localStorage.setItem("expires_in", expires_in);
                window.location.reload()
            });
    }, []);
    
    return (
        <>
            <NavbarStyleOne />
            <MainBanner />
            <Benefit />
            <Features />
            <PricingPlanStyle1 />
            <Faq />
            <FooterStyleThree />
        </>
    );
};

export default IndexPage5;

import '@/public/css/bootstrap.min.css'
import '@/public/css/fontawesome.min.css'
import '@/public/css/remixicon.css'
import '@/public/css/animate.min.css'
import '../node_modules/swiper/swiper.min.css'
import '../node_modules/swiper/components/effect-cube/effect-cube.min.css'
import '../node_modules/swiper/components/effect-coverflow/effect-coverflow.min.css'
import '../node_modules/swiper/components/pagination/pagination.min.css'
import '../node_modules/swiper/components/navigation/navigation.min.css'
import '../node_modules/react-modal-video/css/modal-video.min.css'
import 'react-accessible-accordion/dist/fancy-example.css'
import 'react-image-lightbox/style.css'
import 'react-tabs/style/react-tabs.css'
import "@/public/css/marvel-devices.min.css";
import "@/public/css/device-emulator.min.css";
import 'react-quill/dist/quill.snow.css'
// Global CSS
import '@/public/css/styles.css'
import Layout from '@/components/_App/Layout';
import { Provider } from 'react-redux';
import store from '../store';
import Script from 'next/script'

const MyApp = ({ Component, pageProps }) => {

    return (
        <Layout>
            <Provider store={store}>
                <Component {...pageProps} />
                <Script
                    strategy="afterInteractive"
                    dangerouslySetInnerHTML={{
                        __html: `
                        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
                        (function(){
                        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                        s1.async=true;
                        s1.src='https://embed.tawk.to/622733a81ffac05b1d7d8698/1ftki3io8';
                        s1.charset='UTF-8';
                        s1.setAttribute('crossorigin','*');
                        s0.parentNode.insertBefore(s1,s0);
                        })();
                    `,
                    }}
                />
            </Provider>
        </Layout>
    )
}

export default MyApp;
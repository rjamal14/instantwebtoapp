import React from "react";
import Swal from "sweetalert2";
import { paymentSuccess, loginClient } from "../../../actions";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
var localStorage = require("localStorage");
import Loader from '@/components/_App/Loader'

const Auth = () => {
    const dispatch = useDispatch();
    const router = useRouter();
    const [success, setSuccess] = React.useState(false);

    const token = localStorage.getItem("token") ?? null;
    const token_client = localStorage.getItem("token_client") ?? null;
    const expires_in = localStorage.getItem("expires_in") ?? null;

    function Payment() {
        const params = window.location.search;
        dispatch(paymentSuccess(params))
            .then((res) => {
                setSuccess(true);
            })
            .catch((err) => {
                Swal.fire({
                    icon: "warning",
                    title: "Oops...",
                    text: "Server error occurred, please contact the admin!",
                    confirmButtonColor: "#2e9afe",
                });
            });
    }

    React.useEffect(() => {
        if (token_client !== null && new Date(expires_in) > new Date()) {
            Payment();
        }
    }, [token_client, expires_in]);

    React.useEffect(() => {
        if (token_client === null || new Date(expires_in) < new Date())
            dispatch(loginClient()).then((res) => {
                var expires_in = new Date(res.exp * 1000);
                expires_in.setHours(expires_in.getHours() - 6);
                localStorage.setItem("token_client", res.token);
                localStorage.setItem("expires_in", expires_in);
                window.location.reload();
            });
    }, []);

    return (
        <>
            <div className="preloader-area">
                <div className="d-table">
                    <div className="d-table-cell">
                        {!success ? (
                            <Loader />
                        ) : (
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-6 mx-auto">
                                        <div className="payment">
                                            <div className="payment_header">
                                                <div className="check">
                                                    <h3
                                                        style={{
                                                            color: "#FFFFFF",
                                                        }}
                                                    >
                                                        Payment Success !
                                                    </h3>
                                                </div>
                                            </div>
                                            <div className="content-payment">
                                                <br />
                                                <p>
                                                    Your payment was successful!
                                                </p>
                                                <p>Thank you!</p>
                                                <br />
                                                <button className="btn btn-transparent" onClick={() => {
                                                    if (token === null) {
                                                        window.location.replace('/')
                                                    } else {
                                                        window.location.replace('/app-list')
                                                    }
                                                }}>{token !== null ? "Go to Dashboard" : "Go to Home"}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>

            <style jsx>{`
                .preloader-area {
                    position: fixed;
                    background: #f2f2f2;
                    width: 100%;
                    top: 0;
                    height: 100%;
                    z-index: 1010;
                    left: 0;
                    text-align: center;
                    opacity: 0.96;
                }
                .preloader-area p {
                    font-size: 17px;
                }
            `}</style>
        </>
    );
};

export default Auth;

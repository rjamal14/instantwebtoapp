import React from "react";
var localStorage = require("localStorage");

const Auth = () => {
    const token = localStorage.getItem("token") ?? null;

    React.useEffect(() => {}, []);

    return (
        <>
            <div className="preloader-area">
                <div className="d-table">
                    <div className="d-table-cell">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-6 mx-auto">
                                    <div className="payment">
                                        <div className="payment_header_failed">
                                            <div className="check">
                                                <h3
                                                    style={{
                                                        color: "#FFFFFF",
                                                    }}
                                                >
                                                    Payment Failed !
                                                </h3>
                                            </div>
                                        </div>
                                        <div className="content-payment-failed">
                                            <br/>
                                            <p>
                                                Your payment failed, please try again later.
                                            </p>
                                            <br/>
                                            <button className="btn btn-transparent"
                                                onClick={() => {
                                                    if (token === null) {
                                                        window.location.replace(
                                                            "/"
                                                        );
                                                    } else {
                                                        window.location.replace(
                                                            "/transaction"
                                                        );
                                                    }
                                                }}
                                            >
                                                {token !== null
                                                    ? "Go to Transaction"
                                                    : "Go to Home"}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <style jsx>{`
                .preloader-area {
                    position: fixed;
                    background: #f2f2f2;
                    width: 100%;
                    top: 0;
                    height: 100%;
                    z-index: 1010;
                    left: 0;
                    text-align: center;
                    opacity: 0.96;
                }
                .preloader-area p {
                    font-size: 17px;
                }
            `}</style>
        </>
    );
};

export default Auth;

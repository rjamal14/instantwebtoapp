import React from "react";

const Navigation = ({ step, setStep, errors, max = 5, customTextSubmit = "Build Now", hiddenBack = false, openPreview }) => {
    if (step < max) {
        return (
            <>
                <div className="navigation-form">
                    <div>
                        <a
                            className={`btn ${step !== 1 ? `btn-next` : `btn-next-disable`
                                } mt-20 rounded`}
                            onClick={() => {
                                if (step !== 1) {
                                    setStep(step - 1);
                                    window.location.href = "#content-area";
                                }
                            }}
                        >
                            <i class="ri-arrow-left-line white"></i>
                        </a>
                    </div>
                    <div className="">
                        <span onClick={() => {
                            openPreview()
                        }} className="btn btn-primary preview-mobile">Preview</span>
                    </div>
                    <div className="">
                        <button
                            type="submit"
                            className="btn btn-next mt-20 rounded"
                        >
                            <i class="ri-arrow-right-line white"></i>
                        </button>
                    </div>
                </div>
            </>
        );
    } else {
        return (
            <>
                <div className="navigation-form">
                    <div>
                        {!hiddenBack &&
                            <a
                                className="btn btn-next mt-20 rounded"
                                onClick={() => {
                                    setStep(step - 1);
                                    window.location.href = "#content-area";
                                }}
                            >
                                <i class="ri-arrow-left-line white"></i>
                            </a>
                        }
                    </div>
                    {step < 5 && <div className="">
                        <span onClick={() => {
                            openPreview()
                        }} className="btn btn-primary preview-mobile">Preview</span>
                    </div>}
                    <div>
                        <button
                            type="submit"
                            className="btn btn-next white mt-20 rounded"
                        >
                            {customTextSubmit}
                        </button>
                    </div>
                </div>
            </>
        );
    }
};

export default Navigation;

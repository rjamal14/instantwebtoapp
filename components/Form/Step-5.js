import React from "react";
import { useSelector } from "react-redux";
var localStorage = require("localStorage");

const Step5 = ({ step, register, errors, plan, setPlan }) => {
    const [] = React.useState(1);
    const data_plan = useSelector((state) => state.plan.result.data);
    function compare(a, b) {
        if (a.id < b.id) {
            return -1;
        }
        if (a.id > b.id) {
            return 1;
        }
        return 0;
    }

    if (step === 5) {
        return (
            <>
                <h6 style={{ marginTop: 20 }}>Now we're ready to build your app! </h6>
                {
                    !localStorage.getItem("email") ?
                        <div className="ml-30">
                            <p className="mt-10">
                                Please fill your email address bellow, your login credential will be sent to you.
                                <br />
                                use the link to login and download or make any change on your apps
                            </p>
                            <div className="ml-30">
                                <div className="form-group v3 mt-20">
                                    <label className="mtb-10" for="exampleInputEmail1">
                                        Your email address
                                    </label>
                                    <input
                                        className="form-control"
                                        id="exampleInputEmail1"
                                        aria-describedby="emailHelp"
                                        placeholder="Enter Your email address"
                                        {...register("email", {
                                            required: "Email address is required",
                                            pattern: {
                                                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                                message: "invalid email address",
                                            },
                                        })}
                                    />
                                    {errors.email && (
                                        <span className="text-validator">
                                            {errors?.email?.message}
                                        </span>
                                    )}
                                </div>
                            </div>
                        </div> : null
                }
                <h4 className="mt-20">Select Plan </h4>
                <div
                    className="row"
                    style={{ marginBottom: 50, marginTop: 30 }}
                >
                    <div class="container">
                        <div class="row">
                            {data_plan.sort(compare).map((val) => {
                                return (
                                    <div
                                        className={"col-md-3 col-xs-12 " + (plan === val.id ? `border-green` : `border-normal`)}
                                        style={{
                                            padding: 0, margin: 5
                                        }}
                                        onClick={() => {
                                            setPlan(val.id);
                                        }}
                                    >

                                        <div className="single-pricing-box" style={{ margin: 0, background: (val.id === 1 ? '#d5d5d7' : (val.id === 2 ? '#0099ff' : '#00101a')) }}>
                                            <center>
                                                <h4 style={{ color: val.id === 1 ? '#262A37' : '#f6f6f6' }}>{val.name}</h4>
                                                <h6 style={{ color: val.id === 1 ? '#262A37' : '#f6f6f6', textDecoration: 'line-through' }} >${val.starting_price}</h6>
                                                <h4 style={{ color: val.id === 1 ? '#262A37' : '#f6f6f6' }}>${val.price}</h4>
                                                <h6 style={{ color: val.id === 1 ? '#262A37' : '#f6f6f6' }}>/ app</h6>
                                                <h6 style={{ color: val.id === 1 ? '#262A37' : '#f6f6f6' }}>One Time Payment</h6>
                                                <h8 style={{ color: val.id === 1 ? '#262A37' : '#f6f6f6' }}>{val.description}</h8>
                                            </center>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </div>
            </>
        );
    } else {
        return null;
    }
};

export default Step5;
import React from "react";

const Step1 = ({ step, register, errors, dataOnChange, setDataOnChange,domain_disable=false }) => {
    if (step === 1) {
        return (
            <>
                <h4 className="mt-10 mtb-20">Website Screen </h4>
                <div className="form-group v3 mb-30">
                    <h6 className="mt-10 label-2">Your Website URL </h6>
                    <div className="ml-30">
                        <div className="form-group v3 mt-20">
                            <div className="mt-20">
                                <label
                                    className="mtb-10"
                                    for="exampleInputEmail1"
                                >
                                    Domain name
                                </label>
                                <div className="row mlow">
                                    <select
                                        disabled={domain_disable}
                                        style={{
                                            width:'26%'
                                        }}
                                        maxLength={253}
                                        class={domain_disable ?"form-control-disabled" : "form-control"}
                                        {...register("domain_name_base", {
                                            required:
                                                "Behaviour of screen orientation is required",
                                        })}
                                    >
                                        <option value="https://">
                                            https://
                                        </option>
                                        <option value="http://">
                                            http://
                                        </option>
                                    </select>
                                    <input
                                        disabled={domain_disable}
                                        class={domain_disable ?"form-control-disabled wow" : "form-control wow"}
                                        id="exampleInputEmail1"
                                        aria-describedby="emailHelp"
                                        placeholder="Enter Domain name"
                                        {...register("domain_name", {
                                            required: "Domain name is required",
                                        })}
                                    />
                                </div>
                                {errors.domain_name && (
                                    <span className="text-validator">
                                        {errors?.domain_name?.message}
                                    </span>
                                )}
                            </div>
                            <div className="form-group v3 mt-20">
                                <label
                                    className="mtb-10"
                                    for="exampleInputEmail1"
                                >
                                    Behaviour of screen orientation
                                </label>
                                <select
                                    class="form-control"
                                    {...register("app_oriantation", {
                                        required:
                                            "Behaviour of screen orientation is required",
                                    })}
                                >
                                    <option value="autorotate">
                                        Autorotate
                                    </option>
                                    <option value="lock_vertical">
                                        Lock vertical
                                    </option>
                                    <option value="lock_horizontal">
                                        Lock horizontal
                                    </option>
                                </select>
                                {errors.app_oriantation && (
                                    <span className="text-validator">
                                        {errors?.app_oriantation?.message}
                                    </span>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    } else {
        return null;
    }
};

export default Step1;

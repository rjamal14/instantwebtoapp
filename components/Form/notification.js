import React from "react";
import { useDispatch } from "react-redux";
var localStorage = require("localStorage");
import { useForm } from "react-hook-form";
import { CreateNotification } from "../../actions";
import Swal from "sweetalert2";
import Modal from "react-modal";
import ChangePlan from "../../pages/app-list/[id]/change-plan";
import Resizer from "react-image-file-resizer";
import Loader from '@/components/_App/Loader'
import FileUpload from '@/components/Common/FileUpload'

const Step4 = ({ app_selected, app_plan_id }) => {
    const dispatch = useDispatch();
    const {
        register,
        handleSubmit,
        reset,
        getValues,
        setValue,
        watch,
        formState: { errors },
    } = useForm();
    const [modalIsOpen, setIsOpen] = React.useState(false);
    const [loading, SetLoading] = React.useState(false);
    const [DataForm, setDataForm] = React.useState(false);
    
    React.useEffect(() => {
        const subscription = watch(async (value, { name, type }) => {
            setDataForm(value);
        });
        return () => subscription.unsubscribe();
    }, [watch]);

    const onSubmit = async (data) => {
        SetLoading(true)
        let payload = data;
        payload['app_id'] = parseInt(app_selected);
        if (data.image_before.length > 0) {
            const image = await resizeFile(data.image_before[0])
            payload["image"] = image;
        }
        setTimeout(() => {
            dispatch(CreateNotification(payload))
                .then((res) => {
                    Swal.fire({
                        icon: "success",
                        title: "Success",
                        text: 'Your message has been sent successfully!',
                        confirmButtonColor: "#2e9afe",
                    });
                    reset();
                    SetLoading(false)
                })
                .catch((err) => {
                    Swal.fire({
                        icon: "error",
                        title: "Oops...",
                        text: err,
                        confirmButtonColor: "#2e9afe",
                    });
                    reset();
                    SetLoading(false)
                });
        }, 1000);
    };

    const customStyles = {
        content: {
            top: "50%",
            left: "50%",
            right: "auto",
            bottom: "auto",
            marginRight: "-50%",
            transform: "translate(-50%, -50%)",
            width: "74%",
        },
        overlay: {
            backgroundColor: "rgba(0,0,0,0.5)",
        },
    };

    const resizeFile = (file) =>
        new Promise((resolve) => {
            Resizer.imageFileResizer(
                file,
                500,
                500,
                "PNG",
                100,
                0,
                (uri) => {
                    resolve(uri);
                },
                "base64"
            );
        });

    if (app_selected === '-') {
        return (
            <center
                style={{
                    paddingTop: 90,
                }}
            >
                <img
                    src="/images/select.png"
                    className="img-info-upgrade"
                    alt="logo"
                />
                <br />
                <br />
                <text
                    className="txt-info-upgrade"
                >
                    To use the feature, please select the app first
                </text>
                <br />
            </center>
        );
    } else if (parseInt(app_plan_id) < 2) {
        return (
            <center
                style={{
                    paddingTop: 90,
                }}
            >
                <img
                    src="/images/content.png"
                    className="img-info-upgrade"
                    alt="logo"
                />
                <br />
                <br />
                <text
                    className="txt-info-upgrade"
                >
                    Your current
                    plan can not use
                    push message
                    notification,
                </text>
                <br />
                <text
                    className="txt-info-upgrade"
                >
                    please change
                    plan to use the
                    feature
                </text>
                <br />
                <br />
                <button onClick={() => {
                    setIsOpen(true);
                }} className="btn btn-primary profile-button">
                    Change Plan
                </button>
                <Modal
                    isOpen={modalIsOpen}
                    onRequestClose={() => {
                        setIsOpen(false);
                    }}
                    style={customStyles}
                    contentLabel="Example Modal"
                >
                    <ChangePlan
                        app_id={parseInt(app_selected)}
                        onRequestClose={() => {
                            setIsOpen(false);
                        }}
                    />
                </Modal>
            </center>
        );
    } else {
        return (
            <>
                <div>
                    {loading && <Loader />}
                    <form
                        onSubmit={handleSubmit(
                            onSubmit
                        )}
                    >
                        <div className="form-group">
                            <div>
                                <br />
                                <div className="col-md-12 v2">
                                    <input
                                        maxLength="45"
                                        type="text"
                                        placeholder="Title"
                                        className="form-control"
                                        {...register("title", {
                                            //for set validator
                                        })}
                                    />
                                    {errors.phone && (
                                        <span className="text-validator">
                                            {errors?.phone?.message}
                                        </span>
                                    )}
                                </div>
                                <br />
                                <div className="col-md-12 v2">
                                    <label for="file-upload" style={{ display: 'block' }}>
                                        <FileUpload data={DataForm?.image_before} />
                                    </label>
                                    <input
                                        type="file"
                                        id="file-upload"
                                        style={{ height: 40 }}
                                        accept="image/png, image/gif, image/jpeg"
                                        placeholder="Title"
                                        className="form-control"
                                        {...register("image_before", {
                                            //for set validator
                                        })}
                                    />
                                    {errors.phone && (
                                        <span className="text-validator">
                                            {errors?.phone?.message}
                                        </span>
                                    )}
                                </div>
                                <br />
                                <div className="col-md-12 v2">
                                    <textarea
                                        maxlength="110"
                                        placeholder="Your Mesasge"
                                        className="form-control"
                                        {...register("message", {
                                            //for set validator
                                        })}
                                    ></textarea>
                                    {errors.phone && (
                                        <span className="text-validator">
                                            {errors?.phone?.message}
                                        </span>
                                    )}
                                </div>
                            </div>
                            <div className="v2 mt-5" style={{
                                display: 'flex',
                                justifyContent: 'flex-end'
                            }}>
                                <div>
                                    <div
                                        style={{
                                            marginRight: 20,
                                        }}
                                        onClick={() => {
                                            reset();
                                        }}
                                        className="btn btn-primary profile-button-disable"
                                    >
                                        Clear
                                    </div>
                                    <button
                                        className="btn btn-primary profile-button"
                                        type="submit"
                                    >
                                        Send Push Notification
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </>
        );
    }
};

export default Step4;

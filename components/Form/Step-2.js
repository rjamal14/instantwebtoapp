import React, { useRef } from "react";
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css";
import Swal from "sweetalert2";
import FileUpload from '@/components/Common/FileUpload'

const Step2 = ({
    step,
    register,
    errors,
    color,
    setColor,
    snap_imageData,
    previewBg_Image_Ori,
    previewBg_Image,
    setPreviewBg_Image,
    disableSnap,
    dataForm,
    editor,
    setEditor,
    onCancel
}) => {
    if (step === 2) {
        const cropperRef = useRef();

        const onCrop = () => {
            if (editor === false) {
                setEditor(true);
            } else {
                const imageElement = cropperRef?.current;
                const cropper = imageElement?.cropper;
                setPreviewBg_Image(cropper.getCroppedCanvas().toDataURL());
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: 'Image cropped successfully.',
                    confirmButtonColor: "#2e9afe",
                });
                setEditor(false);
            }
        };

        return (
            <>
                <h4 className="mb-30" style={{ marginTop: 20 }}>Splash screen </h4>
                <div className="ml-30 mt-20">
                    <h6 className="mt-10 label-2">Background color </h6>
                    <div className="ml-30">
                        <div className="form-group v3 mt-20">
                            <label className="mtb-10" for="exampleInputEmail1">
                                Background color
                            </label>
                            <br />
                            <div className="row">
                                <div className="col-md-1">
                                    <input
                                        type="color"
                                        value={color}
                                        onChange={(e) => {
                                            setColor(e.target.value);
                                        }}
                                    />
                                </div>
                                <div className="col ml-30">
                                    <p>{color}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="form-group v3 mtb-50">
                        <h6 className="mt-10 label-2">Background image </h6>
                        <div className="ml-30">
                            <div className="form-group v3 mt-20">
                                <div className="form-group v3 mt-20">
                                    <label className="mtb-10" for="exampleInputEmail1">
                                        Background image
                                    </label>
                                    <label for="file-upload" style={{ display: 'block' }}>
                                        <FileUpload data={dataForm?.bg_image} />
                                    </label>
                                    <input
                                        id="file-upload"
                                        type="file"
                                        style={{ height: 40 }}
                                        accept="image/png, image/gif, image/jpeg"
                                        className="form-control"
                                        placeholder="Enter App icon"
                                        {...register("bg_image", {})}
                                    />
                                    <text style={{ color: "grey" }}>
                                        *Supported format: jpg, jpeg, png
                                    </text>
                                    <br/>
                                    {editor ? (
                                        <div>
                                            <br />
                                            <Cropper
                                                src={previewBg_Image_Ori}
                                                style={{ maxHeight: 400, width: "100%" }}
                                                ref={cropperRef}
                                            />
                                            <br />
                                        </div>
                                    ) : previewBg_Image ? (
                                        <div>
                                            <img
                                                src={previewBg_Image}
                                                style={{
                                                    maxHeight: 150,
                                                    maxWidth: 150,
                                                    objectFit: "contain",
                                                }}
                                            />
                                            <br />
                                        </div>
                                    ) : null}
                                    {errors.bg_image && (
                                        <span className="text-validator">
                                            {errors?.bg_image?.message}
                                        </span>
                                    )}
                                    {previewBg_Image_Ori && (
                                        <div>
                                            <a
                                                className="btn btn-secondary mt-3"
                                                style={{ marginRight: 10 }}
                                                onClick={onCrop}
                                            >
                                                {editor ? "Crop Image" : "Edit"}
                                            </a>
                                            {!previewBg_Image && editor && (
                                                <a
                                                    className="btn btn-transparent mt-3"
                                                    onClick={() => {
                                                        onCancel();
                                                    }}
                                                >
                                                    Cancel
                                                </a>
                                            )}
                                            {previewBg_Image && editor && (
                                                <a
                                                    className="btn btn-transparent mt-3"
                                                    onClick={() => {
                                                        setEditor(false);
                                                    }}
                                                >
                                                    Close
                                                </a>
                                            )}
                                        </div>
                                    )}
                                </div>
                                <div className="form-group v3 mt-20">
                                    <label className="mtb-10" for="exampleInputEmail1">
                                        Position on the screen
                                    </label>
                                    <select
                                        class="form-control"
                                        {...register("screen_position", {})}
                                    >
                                        <option value="">Select one</option>
                                        <option value="auto_height">
                                            100% width - auto height
                                        </option>
                                        <option value="auto_width">100% height - auto width</option>
                                        <option value="cover">Cover</option>
                                    </select>
                                    {errors.screen_position && (
                                        <span className="text-validator">
                                            {errors?.screen_position?.message}
                                        </span>
                                    )}
                                </div>
                                <div className="form-group v3 mt-20">
                                    <label className="mtb-10" for="exampleInputEmail1">
                                        Snap the image
                                    </label>
                                    <select
                                        disabled={snap_imageData.length === 0 || disableSnap}
                                        class={snap_imageData.length === 0 || disableSnap ? "form-control disabled" : "form-control"}
                                        {...register("snap_image", {})}
                                    >
                                        <option value="">Select one</option>
                                        {snap_imageData.map((val) => {
                                            return <option value={val.value}>{val.label}</option>;
                                        })}
                                    </select>
                                    {errors.snap_image && (
                                        <span className="text-validator">
                                            {errors?.snap_image?.message}
                                        </span>
                                    )}
                                </div>
                                <div className="form-group v3 mt-20">
                                    <label className="mtb-10" for="exampleInputEmail1">
                                        Animation fade in Second
                                    </label>
                                    <select
                                        class="form-control"
                                        {...register("screen_animation_fade", {
                                            required: "Animation fade in is required",
                                        })}
                                    >
                                        <option value={1}>1 Second</option>
                                        <option value={2}>2 Seconds</option>
                                        <option value={3}>3 Seconds</option>
                                        <option value={4}>4 Seconds</option>
                                        <option value={5}>5 Seconds</option>
                                    </select>
                                    {errors.screen_animation_fade && (
                                        <span className="text-validator">
                                            {errors?.screen_animation_fade?.message}
                                        </span>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="form-group v3 mtb-50">
                        <h6 className="mt-10 label-2">Screen Orientation </h6>
                        <div className="ml-30">
                            <div className="form-group v3 mt-20">
                                <div className="form-group v3 mt-20">
                                    <label className="mtb-10" for="exampleInputEmail1">
                                        Behaviour of screen orientation
                                    </label>
                                    <select
                                        class="form-control"
                                        {...register("screen_oriantation", {
                                            required: "Animation fade in is required",
                                        })}
                                    >
                                        <option value="autorotate">Autorotate</option>
                                        <option value="lock_vertical">Lock vertical</option>
                                        <option value="lock_horizontal">Lock horizontal</option>
                                    </select>
                                    {errors.screen_oriantation && (
                                        <span className="text-validator">
                                            {errors?.screen_oriantation?.message}
                                        </span>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    } else {
        return null;
    }
};

export default Step2;

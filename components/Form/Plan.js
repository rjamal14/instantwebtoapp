import React from "react";
import { useSelector } from "react-redux";

const Step4 = ({ step, register, errors, plan_id, plan, setPlan }) => {
    const [] = React.useState(1);
    const data_plan = useSelector((state) => state.plan.result.data);
    function compare(a, b) {
        if (a.id < b.id) {
            return -1;
        }
        if (a.id > b.id) {
            return 1;
        }
        return 0;
    }
    return (
        <>
            <h6 className="mt-10">You can upgrade your package to get our best service. </h6>
            <h4 className="mt-20">Select Plan </h4>
            <div
                className="row"
                style={{ marginBottom: 30, marginTop: 30 }}
            >
                {data_plan.sort(compare).map((val) => {
                    if (val.id > plan_id) {
                        return (
                            <div
                                className={"col-md-3 col-xs-12 " + (plan === val.id ? `card-active` : `card`)}
                                onClick={() => {
                                    setPlan(val.id);
                                }}
                            >
                                <div class="">
                                    <h5>
                                        <b>{val.name}</b>
                                    </h5>
                                    <h7 style={{ fontWeight: "normal", marginRight: 10, textDecoration: 'line-through', color: 'red' }}>
                                        ${val.starting_price}
                                    </h7>
                                    <h7 style={{ fontWeight: "normal" }}>
                                        ${val.price}
                                    </h7>
                                    <br />
                                    <h7 style={{ fontWeight: "normal" }}>
                                        {val.description}
                                    </h7>
                                </div>
                            </div>
                        );
                    }
                })}
                {errors.plan_id && (
                    <span className="text-validator">
                        {errors?.plan_id?.message}
                    </span>
                )}
            </div>
        </>
    );
};

export default Step4;
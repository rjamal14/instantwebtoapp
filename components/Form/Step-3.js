import React, { useRef } from "react";
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css";
import Swal from "sweetalert2";
import dynamic from 'next/dynamic'
import FileUpload from '@/components/Common/FileUpload'

const QuillNoSSRWrapper = dynamic(import('react-quill'), {
    ssr: false,
    loading: () => <p>Loading ...</p>,
})

const modules = {
    toolbar: [
        ['bold', 'italic', 'underline', 'strike'],
        [
            { list: 'ordered' },
            { list: 'bullet' },
            { indent: '-1' },
            { indent: '+1' },
        ],
        ['clean'],
    ],
    clipboard: {
        // toggle to add extra line breaks when pasting HTML:
        matchVisual: false,
    },
}
/*
 * Quill editor formats
 * See https://quilljs.com/docs/formats/
 */
const formats = [
    'header',
    'font',
    'size',
    'bold',
    'italic',
    'underline',
    'strike',
    'blockquote',
    'list',
    'bullet',
    'indent',
    'link',
    'image',
    'video',
]

const Step3 = ({
    step,
    register,
    errors,
    bg_color_ob,
    setBgColorOB,
    btn_color_ob,
    setBtnColorOB,
    btn_text_color_ob,
    setBtnTextColorOB,
    snap_imageData,
    previewBg_Image_Ori_OB,
    previewBg_Image_OB,
    setPreviewBg_Image_OB,
    disableSnap,
    check_txt_color,
    SetCheck_txt_color,
    onboarding_descriptions,
    setOnboarding_descriptions,
    dataForm,
    countDesc,
    setCountDesc,
    editor,
    setEditor,
    onCancel
}) => {

    if (step === 3) {
        const cropperRef = useRef();

        const onCrop = () => {
            if (editor === false) {
                setEditor(true);
            } else {
                const imageElement = cropperRef?.current;
                const cropper = imageElement?.cropper;
                setPreviewBg_Image_OB(cropper.getCroppedCanvas().toDataURL());
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: 'Image cropped successfully.',
                    confirmButtonColor: "#2e9afe",
                });
                setEditor(false);
            }
        };

        React.useEffect(() => {

            var html = onboarding_descriptions;
            var div = document.createElement("div");
            div.innerHTML = html;
            setCountDesc(div.innerText.length)

        }, [onboarding_descriptions])

        return (
            <>
                <h4 style={{ marginTop: 20 }}>Onboarding Screen </h4>
                <div className="ml-30 mt-20">
                    <h6 className="mt-10 label-2">Background color </h6>
                    <div className="ml-30">
                        <div className="form-group v3 mt-20">
                            <label className="mtb-10" for="exampleInputEmail1">
                                Background color
                            </label>
                            <br />
                            <div className="row">
                                <div className="col-md-1">
                                    <input
                                        type="color"
                                        value={bg_color_ob}
                                        onChange={(e) => {
                                            setBgColorOB(e.target.value);
                                        }}
                                    />
                                </div>
                                <div className="col ml-30">
                                    <p>{bg_color_ob}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="form-group v3 mtb-50">
                        <h6 className="mt-10 label-2">Background image </h6>
                        <div className="ml-30">
                            <div className="form-group v3 mt-20">
                                <div className="form-group v3 mt-20">
                                    <label
                                        className="mtb-10"
                                        for="exampleInputEmail1"
                                    >
                                        Background image
                                    </label>
                                    <label for="file-upload" style={{ display: 'block' }}>
                                        <FileUpload data={dataForm?.bg_image_ob} />
                                    </label>
                                    <input
                                        id="file-upload"
                                        type="file"
                                        style={{ height: 40 }}
                                        accept="image/png, image/gif, image/jpeg"
                                        className="form-control"
                                        placeholder="Enter App icon"
                                        {...register("bg_image_ob", {})}
                                    />
                                    <text style={{ color: "grey" }}>
                                        *Supported format: jpg, jpeg, png
                                    </text>
                                    <br/>
                                    {editor ? (
                                        <div>
                                            <br />
                                            <Cropper
                                                src={previewBg_Image_Ori_OB}
                                                style={{ maxHeight: 400, width: "100%" }}
                                                ref={cropperRef}
                                            />
                                            <br />
                                        </div>
                                    ) : previewBg_Image_OB ? (
                                        <div>
                                            <img
                                                src={previewBg_Image_OB}
                                                style={{
                                                    maxHeight: 150,
                                                    maxWidth: 150,
                                                    objectFit: "contain",
                                                }}
                                            />
                                            <br />
                                        </div>
                                    ) : null}
                                    {errors.bg_image_ob && (
                                        <span className="text-validator">
                                            {errors?.bg_image_ob?.message}
                                        </span>
                                    )}
                                    {previewBg_Image_Ori_OB && (
                                        <div>
                                            <a
                                                className="btn btn-secondary mt-3"
                                                style={{ marginRight: 10 }}
                                                onClick={onCrop}
                                            >
                                                {editor ? "Crop Image" : "Edit"}
                                            </a>
                                            {!previewBg_Image_OB && editor && (
                                                <a
                                                    className="btn btn-transparent mt-3"
                                                    onClick={() => {
                                                        onCancel();
                                                    }}
                                                >
                                                    Cancel
                                                </a>
                                            )}
                                            {previewBg_Image_OB && editor && (
                                                <a
                                                    className="btn btn-transparent mt-3"
                                                    onClick={() => {
                                                        setEditor(false);
                                                    }}
                                                >
                                                    Close
                                                </a>
                                            )}
                                        </div>
                                    )}
                                </div>
                                <div className="form-group v3 mt-20">
                                    <label
                                        className="mtb-10"
                                        for="exampleInputEmail1"
                                    >
                                        Position on the screen
                                    </label>
                                    <select
                                        required={true}
                                        class="form-control"
                                        {...register("screen_position_ob", {})}
                                    >
                                        <option value="">Select one</option>
                                        <option value="auto_height">
                                            100% width - auto height
                                        </option>
                                        <option value="auto_width">
                                            100% height - auto width
                                        </option>
                                        <option value="cover">Cover</option>
                                    </select>
                                    {errors.screen_position_ob && (
                                        <span className="text-validator">
                                            {
                                                errors?.screen_position_ob
                                                    ?.message
                                            }
                                        </span>
                                    )}
                                </div>
                                <div className="form-group v3 mt-20">
                                    <label
                                        className="mtb-10"
                                        for="exampleInputEmail1"
                                    >
                                        Snap the image
                                    </label>
                                    <select
                                        required={true}
                                        disabled={snap_imageData.length === 0 || disableSnap}
                                        class={snap_imageData.length === 0 || disableSnap ? "form-control disabled" : "form-control"}
                                        {...register("snap_image_ob", {})}
                                    >
                                        <option value="">Select one</option>
                                        {snap_imageData.map((val) => {
                                            return (
                                                <option value={val.value}>
                                                    {val.label}
                                                </option>
                                            );
                                        })}
                                    </select>
                                    {errors.snap_image_ob && (
                                        <span className="text-validator">
                                            {errors?.snap_image_ob?.message}
                                        </span>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="form-group v3 mtb-50">
                        <h6 className="mt-10 label-2">Screen Orientation </h6>
                        <div className="ml-30">
                            <div className="form-group v3 mt-20">
                                <div className="form-group v3 mt-20">
                                    <label
                                        className="mtb-10"
                                        for="exampleInputEmail1"
                                    >
                                        Behaviour of screen orientation
                                    </label>
                                    <select
                                        class="form-control"
                                        {...register("screen_oriantation_ob", {
                                            required:
                                                "Animation fade in is required",
                                        })}
                                    >
                                        <option value="autorotate">
                                            Autorotate
                                        </option>
                                        <option value="lock_vertical">
                                            Lock vertical
                                        </option>
                                        <option value="lock_horizontal">
                                            Lock horizontal
                                        </option>
                                    </select>
                                    {errors.screen_oriantation_ob && (
                                        <span className="text-validator">
                                            {
                                                errors?.screen_oriantation_ob
                                                    ?.message
                                            }
                                        </span>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="form-group v3 mtb-50">
                        <h6 className="mt-10 label-2">Onboarding Description </h6>
                        <div className="ml-30">
                            <div className="form-group v3 mt-20">
                                <div className="form-group v3 mt-20">
                                    <label
                                        className="mtb-10"
                                        for="exampleInputEmail1"
                                    >
                                        {countDesc}/1000, erase all text if you want to remove the card
                                    </label>
                                    <QuillNoSSRWrapper
                                        onChange={(val) => {
                                            if (val.length <= 1000) {
                                                setOnboarding_descriptions(val)
                                            } else {
                                                setOnboarding_descriptions(onboarding_descriptions)
                                            }
                                        }}
                                        modules={modules} formats={formats}
                                        value={onboarding_descriptions}
                                        placeholder="Onboarding Description"
                                    ></QuillNoSSRWrapper>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="form-group v3 mtb-50">
                        <h6 className="mt-10 label-2">Button </h6>
                        <div className="ml-30">
                            <div className="form-group v3 mt-20">
                                <label className="mtb-10" for="exampleInputEmail1">
                                    Checklist Text Color
                                </label>
                                <br />
                                <div className="row">
                                    <div className="col-md-1">
                                        <input
                                            type="color"
                                            value={check_txt_color}
                                            onChange={(e) => {
                                                SetCheck_txt_color(e.target.value);
                                            }}
                                        />
                                    </div>
                                    <div className="col ml-30">
                                        <p>{check_txt_color}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group v3 mt-20">
                                <label
                                    className="mtb-10"
                                    for="exampleInputEmail1"
                                >
                                    Button Text
                                </label>
                                <input
                                    className="form-control"
                                    maxLength={12}
                                    id="exampleInputEmail1"
                                    aria-describedby="emailHelp"
                                    placeholder="Enter Button Text"
                                    {...register("btn_txt", {
                                        required: "App Button Text is required",
                                        maxLength: 12,
                                    })}
                                />
                                {errors.btn_txt && (
                                    <span className="text-validator">
                                        {errors?.btn_txt?.message}
                                    </span>
                                )}
                            </div>
                            <div className="form-group v3 mt-20">
                                <div className="row">
                                    <div className="col">
                                        <label
                                            className="mtb-10"
                                            for="exampleInputEmail1"
                                        >
                                            Button color
                                        </label>
                                        <br />
                                        <div className="row mt-2">
                                            <div className="col-md-1">
                                                <input
                                                    type="color"
                                                    value={btn_color_ob}
                                                    onChange={(e) => {
                                                        setBtnColorOB(
                                                            e.target.value
                                                        );
                                                    }}
                                                />
                                            </div>
                                            <div className="col ml-30">
                                                <p>{btn_color_ob}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <label
                                            className="mtb-10"
                                            for="exampleInputEmail1"
                                        >
                                            Button Text Color
                                        </label>
                                        <br />
                                        <div className="row mt-2">
                                            <div className="col-md-1">
                                                <input
                                                    type="color"
                                                    value={btn_text_color_ob}
                                                    onChange={(e) => {
                                                        setBtnTextColorOB(
                                                            e.target.value
                                                        );
                                                    }}
                                                />
                                            </div>
                                            <div className="col ml-30">
                                                <p>{btn_text_color_ob}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <label
                                            className="mtb-10"
                                            for="exampleInputEmail1"
                                        >
                                            Border Radius
                                        </label>
                                        <input
                                            className="form-control"
                                            maxLength={50}
                                            type="number"
                                            id="exampleInputEmail1"
                                            aria-describedby="emailHelp"
                                            placeholder="Enter Border Radius"
                                            {...register("btn_radius", {
                                                required:
                                                    "App Border Radius is required",
                                            })}
                                        />
                                        {errors.btn_radius && (
                                            <span className="text-validator">
                                                {errors?.btn_radius?.message}
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    } else {
        return null;
    }
};

export default Step3;

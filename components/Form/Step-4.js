import React, { useRef } from "react";
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css";
import Swal from "sweetalert2";
import FileUpload from "@/components/Common/FileUpload";

const Step4 = ({
    step,
    register,
    errors,
    previewApp_icon,
    previewApp_icon_Ori,
    setPreviewApp_icon,
    info = "",
    dataForm,
    editor,
    setEditor,
    onCancel
}) => {
    if (step === 4) {
        const cropperRef = useRef();
        const onCrop = () => {
            if (editor === false) {
                setEditor(true);
            } else {
                const imageElement = cropperRef?.current;
                const cropper = imageElement?.cropper;
                setPreviewApp_icon(cropper.getCroppedCanvas().toDataURL());
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: "Image cropped successfully.",
                    confirmButtonColor: "#2e9afe",
                });
                setEditor(false);
            }
        };

        return (
            <>
                <h4 style={{ marginTop: 20 }}>Application information </h4>
                <div className="ml-30">
                    <div className="form-group v3 mtb-50">
                        <h6 className="mt-10 label-2">
                            Information on user device and Playstore
                        </h6>
                        <div className="ml-30">
                            <div className="form-group v3 mt-20">
                                <label className="mtb-10" for="exampleInputEmail1">
                                    App icon
                                </label>
                                <label for="file-upload" style={{ display: "block" }}>
                                    <FileUpload data={dataForm?.app_icon} />
                                </label>
                                <input
                                    type="file"
                                    id="file-upload"
                                    style={{ height: 40 }}
                                    accept="image/png, image/gif, image/jpeg"
                                    className="form-control"
                                    aria-describedby="emailHelp"
                                    placeholder="Enter App icon"
                                    {...register("app_icon", {})}
                                />
                                <text style={{ color: "grey" }}>
                                    *Supported format: jpg, jpeg, png
                                </text>
                                <br/>
                                {editor ? (
                                    <div>
                                        <br />
                                        <Cropper
                                            src={previewApp_icon_Ori}
                                            style={{ maxHeight: 400, width: "100%" }}
                                            ref={cropperRef}
                                        />
                                        <br />
                                    </div>
                                ) : previewApp_icon ? (
                                    <div>
                                        <img
                                            src={previewApp_icon}
                                            style={{
                                                maxHeight: 150,
                                                maxWidth: 150,
                                                objectFit: "contain",
                                            }}
                                        />
                                        <br />
                                    </div>
                                ) : null}
                                {errors.app_icon && (
                                    <span className="text-validator">
                                        {errors?.app_icon?.message}
                                    </span>
                                )}
                                {previewApp_icon_Ori && (
                                    <div>
                                        <a
                                            className="btn btn-secondary mt-3"
                                            style={{ marginRight: 10 }}
                                            onClick={onCrop}
                                        >
                                            {editor ? "Crop Image" : "Edit"}
                                        </a>
                                        {!previewApp_icon && editor && (
                                            <a
                                                className="btn btn-transparent mt-3"
                                                onClick={() => {
                                                    onCancel();
                                                }}
                                            >
                                                Cancel
                                            </a>
                                        )}
                                        {previewApp_icon && editor && (
                                            <a
                                                className="btn btn-transparent mt-3"
                                                onClick={() => {
                                                    setEditor(false);
                                                }}
                                            >
                                                Close
                                            </a>
                                        )}
                                    </div>
                                )}
                            </div>
                            <div className="form-group v3 mt-20">
                                <label className="mtb-10" for="exampleInputEmail1">
                                    App name
                                </label>
                                <input
                                    className="form-control"
                                    maxLength={12}
                                    id="exampleInputEmail1"
                                    aria-describedby="emailHelp"
                                    placeholder="Enter App name"
                                    {...register("app_name", {
                                        required: "App name is required",
                                        pattern: /^[0-9a-zA-Z\s]*$/g,
                                        maxLength: 12,
                                    })}
                                />
                                {errors.app_name && (
                                    <span className="text-validator">
                                        {errors?.app_name?.message}
                                    </span>
                                )}
                                {errors?.app_name?.type === "pattern" && (
                                    <span className="text-validator">
                                        Input does not match the format
                                    </span>
                                )}
                                {errors?.app_name?.type === "maxLength" && (
                                    <span className="text-validator">
                                        App name cannot exceed 12 characters
                                    </span>
                                )}
                            </div>
                            <div className="form-group v3 mt-20">
                                <label className="mtb-10" for="exampleInputEmail1">
                                    App description
                                </label>
                                <input
                                    className="form-control"
                                    id="exampleInputEmail1"
                                    aria-describedby="emailHelp"
                                    placeholder="Enter App description"
                                    {...register("app_descriptions", {
                                        required: "App description is required",
                                    })}
                                />
                                {errors.app_descriptions && (
                                    <span className="text-validator">
                                        {errors?.app_descriptions?.message}
                                    </span>
                                )}
                            </div>
                            <div className="form-group v3 mt-20">
                                <label className="mtb-10" for="exampleInputEmail1">
                                    Playsore package name
                                </label>
                                <input
                                    className="form-control"
                                    maxLength={25}
                                    id="exampleInputEmail1"
                                    aria-describedby="emailHelp"
                                    placeholder="Enter Playsore package name"
                                    {...register("app_package", {
                                        required: "Playsore package name is required",
                                        pattern: /^[a-z]\w*(\.[a-z]\w*)+$/i,
                                        maxLength: 25,
                                    })}
                                />
                                {info !== "" && (
                                    <span className="text-validator" style={{ fontSize: 14 }}>
                                        {info}
                                    </span>
                                )}
                                {errors.app_package && (
                                    <span className="text-validator">
                                        {errors?.app_package?.message}
                                    </span>
                                )}
                                {errors?.app_package?.type === "pattern" && (
                                    <span className="text-validator">
                                        Input does not match the format, eq: com.companyname.appname
                                    </span>
                                )}
                                {errors?.app_package?.type === "maxLength" && (
                                    <span className="text-validator">
                                        Playsore package name cannot exceed 25 characters
                                    </span>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    } else {
        return null;
    }
};

export default Step4;

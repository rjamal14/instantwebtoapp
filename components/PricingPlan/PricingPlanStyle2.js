import React from 'react';
import Link from 'next/link'

const PricingPlanStyle2 = () => {
    return (
        <>
        <div className="pt-100" id="pricing">
            <div className="pt-100 pb-75">
                <div className="container">
                    <div className="" style={{overflow:'auto'}}>
                        <table>
                            <tr>
                                <th style={{fontSize:32,padding:25,borderColor:'#D5D5D5',borderTopRightRadius:8,borderTopLeftRadius:8,color:'#333333',background:'#D5D5D5'}} width="40%">Package</th>
                                <th style={{fontSize:32,padding:25,borderColor:'#79D156',borderTopRightRadius:8,borderTopLeftRadius:8,color:'#FFFFFF',background:'#79D156'}} width="15%">Free</th>
                                <th style={{fontSize:32,padding:25,borderColor:'#FFD247',borderTopRightRadius:8,borderTopLeftRadius:8,color:'#FFFFFF',background:'#FFD247'}} width="15%">Micro</th>
                                <th style={{fontSize:32,padding:25,borderColor:'#2e9afe',borderTopRightRadius:8,borderTopLeftRadius:8,color:'#FFFFFF',background:'#2e9afe'}} width="15%">Small</th>
                                {/* <th style={{fontSize:32,padding:25,borderColor:'#DC5980',borderTopRightRadius:8,borderTopLeftRadius:8,color:'#FFFFFF',background:'#DC5980'}} width="15%">Med</th> */}
                            </tr>
                            <tr>
                                <td style={{fontSize:20,padding:25,borderColor:'#D5D5D5',color:'#333333',background:'#D5D5D5'}} ></td>
                                <td style={{fontSize:20,padding:25,borderColor:'#79D156',color:'#FFFFFF',background:'#79D156'}} >Suitable for test or sample</td>
                                <td style={{fontSize:20,padding:25,borderColor:'#FFD247',color:'#FFFFFF',background:'#FFD247'}} >Basic playstore distribution</td>
                                <td style={{fontSize:20,padding:25,borderColor:'#2e9afe',color:'#FFFFFF',background:'#2e9afe'}} >Suitable for small business</td>
                                {/* <td style={{fontSize:20,padding:25,borderColor:'#DC5980',color:'#FFFFFF',background:'#DC5980'}} >Suitable for medium enterprise</td> */}
                            </tr>
                            <tr>
                                <td style={{borderWidth:1,fontSize:20,padding:25,textAlign:'left',borderColor:'#D5D5D5',color:'#333333',background:'#FFFFFF'}} >
                                    <h4>Overview</h4>
                                    <br/>
                                    <h7>Devices</h7>
                                    <hr/>
                                    <h7>Price </h7>
                                    <hr/>
                                    <br/>
                                    <h4>Auto build</h4>
                                    <br/>
                                    <h7>Max build</h7>
                                    <hr/>
                                    <h7>Averange</h7>
                                    <hr/>
                                    <br/>
                                    <h4>App features</h4>
                                    <br/>
                                    <h7>Splash</h7>
                                    <hr/>
                                    <h7>Locked</h7>
                                    <hr/>
                                    <h7>Pull to refresh page</h7>
                                    <hr/>
                                    <h7>Forward and backward gesture</h7>
                                    <hr/>
                                    <h7>Pinch to zoom gesture</h7>
                                    <hr/>
                                    <h7>No conection page</h7>
                                    <hr/>
                                    <h7>Costumizeable icon and app name</h7>
                                    <hr/>
                                    <h7>Costumizeable package name</h7>
                                    <hr/>
                                    <h7>Auto build playstore key</h7>
                                    <hr/>
                                    <h7>Auto build versioning</h7>
                                    <hr/>
                                    <h7>Build for playstore distribution</h7>
                                    <hr/>
                                    <h7>Google Push Notification</h7>
                                    <hr/>
                                    <h7>One Signal Push Notification</h7>
                                    <hr/>
                                    <h7>One Signal Push Notification integration</h7>
                                    <hr/>
                                    <h7>Google Admob Ads integration</h7>
                                    <hr/>
                                    <h7>Google Analytics integration</h7>
                                    <hr/>
                                    <h7>Widget</h7>
                                    <hr/>
                                </td>
                                <td style={{borderWidth:1,fontSize:20,padding:25,textAlign:'center',borderColor:'#79D156',color:'#333333',background:'#FFFFFF'}} >
                                    <h4 style={{visibility:'hidden'}}>-</h4>
                                    <br/>
                                    <h7>Android</h7>
                                    <hr/>
                                    <h7>Free</h7>
                                    <hr/>
                                    <br/>
                                    <h4 style={{visibility:'hidden'}}>-</h4>
                                    <br/>
                                    <h7>5 times</h7>
                                    <hr/>
                                    <h7>Upto 15 minutes</h7>
                                    <hr/>
                                    <br/>
                                    <h4 style={{visibility:'hidden'}}>-</h4>
                                    <br/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                </td>
                                <td style={{borderWidth:1,fontSize:20,padding:25,textAlign:'center',borderColor:'#FFD247',color:'#333333',background:'#FFFFFF'}} >
                                <h4 style={{visibility:'hidden'}}>-</h4>
                                    <br/>
                                    <h7>Android</h7>
                                    <hr/>
                                    <h7>5 USD</h7>
                                    <hr/>
                                    <br/>
                                    <h4 style={{visibility:'hidden'}}>-</h4>
                                    <br/>
                                    <h7>10 times</h7>
                                    <hr/>
                                    <h7>Upto 15 minutes</h7>
                                    <hr/>
                                    <br/>
                                    <h4 style={{visibility:'hidden'}}>-</h4>
                                    <br/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                </td>
                                <td style={{borderWidth:1,fontSize:20,padding:25,textAlign:'center',borderColor:'#2e9afe',color:'#333333',background:'#FFFFFF'}} >
                                <h4 style={{visibility:'hidden'}}>-</h4>
                                    <br/>
                                    <h7>Android</h7>
                                    <hr/>
                                    <h7>20 USD</h7>
                                    <hr/>
                                    <br/>
                                    <h4 style={{visibility:'hidden'}}>-</h4>
                                    <br/>
                                    <h7>1 per day</h7>
                                    <hr/>
                                    <h7>Upto 10 minutes</h7>
                                    <hr/>
                                    <br/>
                                    <h4 style={{visibility:'hidden'}}>-</h4>
                                    <br/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7 style={{paddingTop:5}}>Text</h7>
                                    <hr/>
                                    <h7 style={{paddingTop:5}}>Text</h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-close-line" style={{color:'red',fontWeight:'bold',fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                </td>
                                {/* <td style={{borderWidth:1,fontSize:20,padding:25,textAlign:'center',borderColor:'#DC5980',color:'#333333',background:'#FFFFFF'}} >
                                <h4 style={{visibility:'hidden'}}>-</h4>
                                    <br/>
                                    <h7>Android</h7>
                                    <hr/>
                                    <h7>40 USD</h7>
                                    <hr/>
                                    <br/>
                                    <h4 style={{visibility:'hidden'}}>-</h4>
                                    <br/>
                                    <h7>2 per day</h7>
                                    <hr/>
                                    <h7>Upto 5 minutes</h7>
                                    <hr/>
                                    <br/>
                                    <h4 style={{visibility:'hidden'}}>-</h4>
                                    <br/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7 style={{paddingTop:5}}>Text, image</h7>
                                    <hr/>
                                    <h7 style={{paddingTop:5}}>Text, image</h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                    <h7><i class="ri-check-line" style={{color:'#79d156',fontWeight:'bold', fontSize:18}}></i></h7>
                                    <hr/>
                                </td> */}
                            </tr>
                        </table>
                    </div>
                </div>

                {/* Shape Images */}
                <div className="shape12">
                    <img src="/images/shape/shape11.png" alt="shape" />
                </div>
                <div className="shape13">
                    <img src="/images/shape/shape15.png" alt="shape" />
                </div>
            </div>
        </div>
        </>
    )
}

export default PricingPlanStyle2;
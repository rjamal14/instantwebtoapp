import React from 'react';
import Link from 'next/link'

const PricingPlanStyle5 = () => {
    return (
        <>
            <div id="Pricing" className="pricing-area" style={{
                paddingTop:120,
            }}>
                <div className="container">
                    <div className="section-title-2">
                        <h2>Pricing</h2>
                        <span className="features-subtext">Choose the Package that suits your business</span>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-lg-4 col-md-6 col-sm-6" style={{marginBottom:50}}>
                            <div className="single-pricing-box" style={{ background: '#d5d5d7' }}>
                                <h2>Starter</h2>
                                <h5 style={{ textDecoration: 'line-through' }}>$10</h5>
                                <h1>$5</h1>
                                <h5>/ app</h5>
                                <h5>One Time Payment</h5>
                                <br />
                                <Link href="/convert">
                                    <a style={{
                                        backgroundColor: '#ffc602',
                                        color: '#333333',
                                        padding: '10px 10%',
                                        borderRadius: 6,
                                        fontWeight: 'bold',
                                        fontSize: 18
                                    }}>Build Now</a>
                                </Link>
                            </div>
                            <div className="single-pricing-box">
                                <ul className="features-list">
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Android APK Builder</li>
                                    <li><img src="/images/remove.png" alt="shape" width={15} style={{marginRight:5}} />Android AAB Builder</li>
                                    <li><img src="/images/remove.png" alt="shape" width={15} style={{marginRight:5}} />iOs IPA Builder</li>
                                    <li><img src="/images/remove.png" alt="shape" width={15} style={{marginRight:5}} />Firebase Push Notification</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Unlimited app revisions</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Build per day: 5</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Build history: 5</li>
                                    <li><img src="/images/remove.png" alt="shape" width={15} style={{marginRight:5}} />Priority build</li>
                                    <li><img src="/images/remove.png" alt="shape" width={15} style={{marginRight:5}} />Private server</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-6" style={{marginBottom:50}}>
                            <div className="single-pricing-box" style={{ background: '#0099ff' }}>
                                <h2 style={{ color: '#f6f6f6' }}>Pro</h2>
                                <h5 style={{ color: '#f6f6f6', textDecoration: 'line-through' }} >$98</h5>
                                <h1 style={{ color: '#f6f6f6' }}>$49</h1>
                                <h5 style={{ color: '#f6f6f6' }}>/ app</h5>
                                <h5 style={{ color: '#f6f6f6' }}>One Time Payment</h5>
                                <br />
                                <Link href="/convert">
                                    <a style={{
                                        backgroundColor: '#ffc602',
                                        color: '#333333',
                                        padding: '10px 10%',
                                        borderRadius: 6,
                                        fontWeight: 'bold',
                                        fontSize: 18
                                    }}>Build Now</a>
                                </Link>
                            </div>
                            <div className="single-pricing-box">
                                <ul className="features-list">
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Android APK Builder</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Android AAB Builder</li>
                                    <li><img src="/images/remove.png" alt="shape" width={15} style={{marginRight:5}} />iOs IPA Builder</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Firebase Push Notification</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Unlimited app revisions</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Build per day: 10</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Build history: 10</li>
                                    <li><img src="/images/remove.png" alt="shape" width={15} style={{marginRight:5}} />Priority build</li>
                                    <li><img src="/images/remove.png" alt="shape" width={15} style={{marginRight:5}} />Private server</li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6 col-sm-6" style={{marginBottom:50}}>
                            <div className="single-pricing-box" style={{ background: '#00101a' }}>
                                <h2 style={{ color: '#f6f6f6' }}>Biz</h2>
                                <h5 style={{ color: '#f6f6f6', textDecoration: 'line-through' }} >$198</h5>
                                <h1 style={{ color: '#f6f6f6' }}>$99</h1>
                                <h5 style={{ color: '#f6f6f6' }}>/ app</h5>
                                <h5 style={{ color: '#f6f6f6' }}>One Time Payment</h5>
                                <br />
                                <Link href="/convert">
                                    <a style={{
                                        backgroundColor: '#ffc602',
                                        color: '#333333',
                                        padding: '10px 10%',
                                        borderRadius: 6,
                                        fontWeight: 'bold',
                                        fontSize: 18
                                    }}>Build Now</a>
                                </Link>
                            </div>
                            <div className="single-pricing-box">
                                <ul className="features-list">
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Android APK Builder</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Android AAB Builder</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />iOs IPA Builder</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Firebase Push Notification</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Unlimited app revisions</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Build per day: 30</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Build history: 30</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Priority build</li>
                                    <li><img src="/images/check.png" alt="shape" width={15} style={{marginRight:5}} />Private server</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                {/* Shape Images */}
                <div className="shape7">
                    <img src="/images/shape/shape6.png" alt="shape" />
                </div>
                <div className="shape8">
                    <img src="/images/shape/shape7.png" alt="shape" />
                </div>
            </div>
        </>
    )
}

export default PricingPlanStyle5;
import React from 'react';
import { useTable, usePagination } from 'react-table'
import { FaAngleDoubleLeft, FaAngleLeft, FaAngleRight, FaAngleDoubleRight } from 'react-icons/fa'

function Table({ setPerPage, setPage, columns, data, currentpage, perPage, totalPage, pagination = true, noData = "We don't see any records from your history." }) {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    // canPreviousPage,
    // canNextPage,
    pageOptions,
    // pageCount,
    // gotoPage,
    // nextPage,
    // previousPage,
    // setPageSize,
    // Get the state from the instance
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      useControlledState: (state) => {
        return React.useMemo(
          () => ({
            ...state,
            pageIndex: currentpage,
          }),
          [state, currentpage]
        );
      },
      initialState: { pageIndex: currentpage }, // Pass our hoisted table state
      manualPagination: true, // Tell the usePagination
      // hook that we'll handle our own data fetching
      // This means we'll also have to provide our own
      // pageCount.
      pageCount: totalPage,
    },
    usePagination
  );

  return (
    <>
      <table {...getTableProps()} className="table table-fixed table-custom1">
        <thead style={{ background: '#666666', color: 'white', height: '5%' }}>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.slice(0, 1).map((column) => (
                <th
                  {...column.getHeaderProps({
                    style: { textAlign: column.textAlign, minWidth: column.minWidth, maxWidth: column.maxWidth, width: column.width },
                  })}
                  className="px-1 bg-red-100 capitalize w-96"
                >
                  {column.render('Header')}
                </th>
              ))}
              {headerGroup.headers.slice(1).map((column) => (
                <th
                  {...column.getHeaderProps({
                    style: { textAlign: column.textAlign, minWidth: column.minWidth, maxWidth: column.maxWidth, width: column.width },
                  })}
                  className="bg-red-100 capitalize w-1/6"
                >
                  {column.render('Header')}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {
            data.length === 0 &&
            <tr>
              <td align="center" colspan={100}><div style={{minWidth:650}}>{noData}</div></td>
            </tr>
          }
          {page.map((row, i) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps({
                      style: {
                        minHeight: 100,
                        minWidth: cell.column.minWidth,
                        maxWidth: cell.column.maxWidth,
                        width: cell.column.width,
                      },
                    })} className={"truncate p-1 border-b-2"} id={(cell.column.textAlign === 'center' ? 'table-cudd-center' : cell.column.textAlign === 'right' ? 'table-cudd-right' : 'table-cudd-left')}>
                      {cell.render('Cell')}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>

      {
        pagination !== false && <div className="flex table-pagination pagination-customs justify-between mb-3">
          <button className='btn btn-primary pagination-button'
            onClick={() => {
              setPage(1);
            }}
            disabled={currentpage === 1}
          >
            <FaAngleDoubleLeft />
          </button>{' '}
          <button className='btn btn-primary pagination-button'
            onClick={() => {
              setPage((s) => (s === 0 ? 0 : s - 1));
            }}
            disabled={currentpage === 1}
          >
            <FaAngleLeft />
          </button>{' '}
          <button className='btn btn-primary pagination-button'
            onClick={() => {
              setPage((s) => s + 1);
            }}
            disabled={currentpage === totalPage}
          >
            <FaAngleRight />
          </button>{' '}
          <button className='btn btn-primary pagination-button'
            onClick={() => {
              setPage(totalPage);
            }}
            disabled={currentpage === totalPage}
          >
            <FaAngleDoubleRight />
          </button>{' '}
          <span style={{marginLeft:20, minWidth:100, textAlign:'center'}}>
            Page{' '}
            <strong>
              {pageIndex} from {pageOptions.length}
            </strong>{' '}
          </span>
          <span className="page" style={{marginLeft:20,marginRight:40}}>
            |
          </span>{' '}
          <select
            class="form-control"
            style={{width:180}}
            value={perPage}
            onChange={(e) => {
              // setPageSize(Number(e.target.value));
              setPerPage(Number(e.target.value));
            }}
          >
            {[5, 10, 20, 50, 100].map((pageSize) => (
              <option key={pageSize} value={pageSize}>
                Show {pageSize} per page
              </option>
            ))}
          </select>
        </div>
      }
    </>
  );
}


export default Table;
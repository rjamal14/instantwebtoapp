import React from 'react'
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemPanel,
    AccordionItemButton
} from 'react-accessible-accordion'

const FAQ = () => {
    return (
        <>
            <div id="FAQ" className="faq-area" style={{
                paddingTop:120,
            }}>
                <div className="container">
                    <div className="section-title-2">
                        <h2>FAQ</h2>
                        <span className="features-subtext">Frequently Asked Questions</span>
                    </div>

                    <div className="faq-accordion">
                        <Accordion allowZeroExpanded preExpanded={['a']}>
                            <AccordionItem uuid="a">
                                <AccordionItemHeading>
                                    <AccordionItemButton>
                                        <span>
                                            What kind of websites can be turned into Android and iOS applications?
                                        </span>
                                    </AccordionItemButton>
                                </AccordionItemHeading>

                                <AccordionItemPanel>
                                    <p style={{ textAlign: 'justify' }}>All types of websites can be converted into Android and iOS app, ranging from static html websites, open-source CMS websites, such as Wordpress, Woocomerce, Joomla, Magento, Drupal, Codeigniter, to closed-source CMS websites, such as Wix, Shopify, Squarspace.</p>
                                </AccordionItemPanel>
                            </AccordionItem>

                            <AccordionItem uuid="b">
                                <AccordionItemHeading>
                                    <AccordionItemButton>
                                        <span>
                                            Do I need to have an Apple device to create iOS apps?
                                        </span>
                                    </AccordionItemButton>
                                </AccordionItemHeading>

                                <AccordionItemPanel>
                                    <p style={{ textAlign: 'justify' }}>No, you don't need to have any Apple device, you can simply use our build system to create an iOs IPA file by following the easy steps under your iOs IPA configuration page</p>
                                </AccordionItemPanel>
                            </AccordionItem>
                        </Accordion>
                    </div>
                </div>
            </div>
        </>
    )
}

export default FAQ;
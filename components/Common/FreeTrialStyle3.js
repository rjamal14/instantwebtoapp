import React from 'react';
import Link from 'next/link';

const FreeTrialStyle3 = ({text, btn_text, btn_link}) => {
    return (
        <>
            <div className="free-trial-area">
                <div className="free-trial-inner">
                    <div className="row">
                        <div className="col-lg-12 col-md-12">
                            <div className="row">
                                <div className="col-md-8">
                                    <h2>{text}</h2>
                                </div>
                                <div className="col-md-4">
                                    <Link href={btn_link}>
                                        <a className="default-btn-login-now">{btn_text}</a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="lines">
                        <div className="line"></div>
                        <div className="line"></div>
                        <div className="line"></div>
                        <div className="line"></div>
                        <div className="line"></div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default FreeTrialStyle3;
const FileUpload = ({ data }) => {

    function generatename() {
        if(data === undefined || data?.length === 0){
            return 'No file selected yet';
        } else if(data?.length > 0 && data?.length < 2){
            return data[0].name;
        } else if(data?.length > 1){
            return data;
        }
    }
    return (
        <>
            <div className="form-control" style={{padding:0}}>
                <div className="wowm btn btn-transparent file-upload-d file-upload-fs"><i class="fa fa-upload" style={{ color: '#2e9afe' }}> <text style={{
                    fontWeight: 'normal',
                    color: '#333333'
                }}> &nbsp; Upload</text> </i></div>
                <div className="woww btn btn-transparent file-upload-fs"><div className="txt-oneline">{generatename()}</div></div>
            </div>
        </>
    )
}

export default FileUpload;
import React from 'react';
import Link from 'next/link';
import ScrollAnimation from 'react-animate-on-scroll';

const AppProgressStyle2 = () => {
    return (
        <>
            <div className="app-progress-area bg-black ptb-100" style={{background:"#F8F8F8"}}>
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-lg-6 col-md-12">
                            <div className="app-progress-animation-image">
                                <div className="app-img">
                                    <ScrollAnimation animateIn='fadeInDown' duration={2}>
                                        <img src="/images/app-progress/app-progress1.png" alt="app-progress" />
                                    </ScrollAnimation>
                                </div>

                                <div className="app-img">
                                    <ScrollAnimation animateIn='fadeInRight' duration={2}>
                                        <img src="/images/app-progress/app-progress2.png" alt="app-progress" />
                                    </ScrollAnimation>
                                </div>

                                <div className="app-img">
                                    <ScrollAnimation animateIn='fadeInLeft' duration={2}>
                                        <img src="/images/app-progress/app-progress3.png" alt="app-progress" />
                                    </ScrollAnimation>
                                </div>

                                <div className="app-img">
                                    <ScrollAnimation animateIn='fadeInUp' duration={2}>
                                        <img src="/images/app-progress/app-progress4.png" alt="app-progress" />
                                    </ScrollAnimation>
                                </div>

                                <div className="app-img main-image">
                                    <img src="/images/app-progress/progress-main.png" alt="app-progress" />
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-6 col-md-12">
                            <div className="app-progress-content text-white">
                                <h2 style={{color:'#2e9afe'}}>We Already Build</h2>
                                <h2 style={{color:'#2e9afe'}}>1,000 apps, and still counting</h2>
                                <text className='couting-subtitle'>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</text>
                                <br/>
                                <br/>
                                <p style={{color:'#333333'}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default AppProgressStyle2;
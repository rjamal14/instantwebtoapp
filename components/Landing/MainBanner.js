import React from 'react'
import ScrollAnimation from 'react-animate-on-scroll'
import Link from 'next/link';

const MainBanner = () => {
    return (
        <>
            <div id="Home" className="gradient-banner-area">
                <div className="container">
                    <div className="row colum-reverse">
                        <div className="col-lg-5 col-md-5">
                            <div className="gradient-banner-content">
                                <h1 className='text-tittle-banner br3-w'>Convert website<br />to Android and iOs<br />within a minutes!</h1>
                                <h5 className='text-sub-tittle-banner br1-w'>add native mobile device functionalities<br />to boost your business growth</h5>
                                <br/>
                                <Link href="/convert">
                                    <text style={{fontSize:20,color:'#010b13'}} className="default-btn cim ibim">Build App Now</text>
                                </Link>
                                <br/>
                            </div>
                        </div>
                        <div className="col-lg-7 col-md-7 mifm">
                            <ScrollAnimation animateIn=' fadeInUp' duration={2} animateOnce={true} initiallyVisible={true}>
                                <img src="/images/android-builder-and-ios-builder.png" alt="banner-img" width={'100%'} />
                            </ScrollAnimation>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default MainBanner;
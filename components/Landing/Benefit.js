import React from 'react';

const Benefit = () => {
    return (
        <>
            <div id="Benefit" style={{
                paddingTop:220,
            }}>
                <div className="features-area">
                    <div className="container">
                        <div className="section-title-2">
                            <h2>Benefit and Use Case</h2>
                            <span className="features-subtext">Why you should use our service</span>
                        </div>

                        <div className="row justify-content-center">
                            <div className="col-xl-4 col-lg-6 col-sm-6 col-md-6">
                                <div className="single-features-item">
                                    <div className="icon bg3">
                                        <i className="ri-settings-line"></i>
                                    </div>
                                    <h3>Easy setup</h3>
                                    <p>Turn website into app and add custom pages in just few easy steps, no coding skill required.</p>
                                </div>
                            </div>

                            <div className="col-xl-4 col-lg-6 col-sm-6 col-md-6">
                                <div className="single-features-item">
                                    <div className="icon bg2">
                                        <i className="ri-timer-flash-line"></i>
                                    </div>
                                    <h3>Instant build</h3>
                                    <p>No need for days or weeks, our app convertor and auto builder system only takes minutes to get your instant app ready for test and distribution.</p>
                                </div>
                            </div>

                            <div className="col-xl-4 col-lg-6 col-sm-6 col-md-6">
                                <div className="single-features-item">
                                    <div className="icon bg4">
                                        <i className="ri-stack-line"></i>
                                    </div>
                                    <h3>Rich features</h3>
                                    <p>Add native mobile app functionality into your converted web app, rebuild easily for any new features added later</p>
                                </div>
                            </div>

                            <div className="col-xl-4 col-lg-6 col-sm-6 col-md-6">
                                <div className="single-features-item">
                                    <div className="icon bg4">
                                        <i className="ri-money-dollar-circle-line"></i>
                                    </div>
                                    <h3>Reduce cost</h3>
                                    <p>Save your money form developing both web and native mobile apps, focus on web first along your growing business.</p>
                                </div>
                            </div>

                            <div className="col-xl-4 col-lg-6 col-sm-6 col-md-6">
                                <div className="single-features-item">
                                    <div className="icon bg2">
                                        <i className="ri-time-line"></i>
                                    </div>
                                    <h3>Time saving</h3>
                                    <p>Not only saving your budget but time for business develpment also optimized.</p>
                                </div>
                            </div>

                            <div className="col-xl-4 col-lg-6 col-sm-6 col-md-6">
                                <div className="single-features-item">
                                    <div className="icon bg3">
                                        <i className="ri-bug-line"></i>
                                    </div>
                                    <h3>Friendly for testing</h3>
                                    <p>Our Starter plan suitable for testing purpose and non Google Play Store distribution.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Benefit;
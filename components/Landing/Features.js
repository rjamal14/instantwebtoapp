import React from 'react'
import Link from 'next/link';
import ScrollAnimation from 'react-animate-on-scroll';

const Features = () => {
    return (
        <>
            <div className="about-area" id="Features" style={{ background: '#FFFFFF', paddingTop: 120, }}>
                <div className="about-area pb-100">
                    <div className="container">
                        <div className="section-title-2">
                            <h2>Features</h2>
                            <span className="features-subtext">We are the best on the market & we can prove with more than just word,<br />check our features below</span>
                        </div>
                        <br />
                        <br />
                        <br />
                        <div className="software-integrations-area">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-6 col-md-12">
                                        <ScrollAnimation animateIn="fadeInUp">
                                            <img src="/images/appsinstant-web-to-android-builder.png" className="img-features" alt="features" />
                                        </ScrollAnimation>
                                    </div>
                                    <div className="col-lg-6 col-md-12">
                                        <div className="software-integrations-content">
                                            <h2>Android and iOs app builder</h2>
                                            <p>Scale up your business, create both Android and iOs mobile app easily to attract your existing and new customers.
                                                Take full control to design, edit and rebuild your app, from basic need of creating native pages into adding native functions, such as:
                                                Splash screen, onboarding screen, permission page, customizeable screen orientation, push notification, hand gestures, and any other features that will be added later</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <div className="software-integrations-area">
                            <div className="container">
                                <div className="row colum-reverse">
                                    <div className="col-lg-6 col-md-12">
                                        <div className="software-integrations-content">
                                            <h2>Customizeable Android package and iOs bundle</h2>
                                            <p>Feel free to customizing your apps:</p>
                                            <p>Icon launcher, app name, Android package name, iOs bundle id</p>
                                        </div>
                                    </div>

                                    <div className="col-lg-6 col-md-12">
                                        <ScrollAnimation animateIn="fadeInUp">
                                            <img src="/images/appsinstant-app-creator.png" className="img-features" alt="features" />
                                        </ScrollAnimation>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <div className="software-integrations-area">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-6 col-md-12">
                                        <ScrollAnimation animateIn="fadeInUp">
                                            <img src="/images/appsinstant-app-convertor.png" className="img-features" alt="features" />
                                        </ScrollAnimation>
                                    </div>
                                    <div className="col-lg-6 col-md-12">
                                        <div className="software-integrations-content">
                                            <h2>Optimized for test and distribution</h2>
                                            <ul>
                                                <li><p>Build easily your app file:</p></li>
                                                <li><p>Android APK for testing via email and messenger.</p></li>
                                                <li><p>Android AAB for Play Store distribution.</p></li>
                                                <li><p>iOs IPA for App Store distribuition.</p></li>
                                                <li><p>Keep the versioning simple with our auto versioning feature</p></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Features;
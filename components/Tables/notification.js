import React from "react";
import { useDispatch, useSelector } from "react-redux";
var localStorage = require("localStorage");
import { useRouter } from "next/router";
import Table from "../Common/table";
import { getNotification } from "../../actions";
import "@material/data-table/dist/mdc.data-table.css";
import "@rmwc/data-table/data-table.css";
import "@rmwc/icon/icon.css";
import Modal from "react-modal";
import ChangePlan from "../../pages/app-list/[id]/change-plan";
import env from "config/env";

const NotificationTable = ({ app_selected, app_plan_id }) => {
    const dispatch = useDispatch();
    const router = useRouter();
    const [page, SetPage] = React.useState(1);
    const [perPage, SetPerPage] = React.useState(10);
    const notification_list = useSelector((state) => state.notification.notification_list);
    const status_code = useSelector((state) => state.notification.status_code);
    const token = localStorage.getItem("token") ?? null;
    const [modalIsOpen, setIsOpen] = React.useState(false);

    React.useEffect(() => {
        getData();
    }, [page, perPage, token, status_code]);

    function getData() {
        if (token !== null || status_code === 401) {
            dispatch(getNotification(parseInt(app_selected), page, perPage));
        } else {
            localStorage.clear();
            router.replace("/login");
            Swal.fire({
                icon: "warning",
                title: "Oops...",
                text: "login session has ended, please login again!",
                confirmButtonColor: "#2e9afe",
            });
        }
    }

    const list = notification_list?.data?.map((i, index) => {
        return {
            col1: <div style={{minWidth:65}}>{((page - 1) * perPage + (index + 1)).toString()}</div>,
            col2: <div style={{minWidth:200}}>{dateFormat(i?.createdAt)}</div>,
            col3: i?.image,
            col4: <div style={{minWidth:200}}>{i?.title}</div>,
            col5: <div style={{minWidth:300}}>{i?.message}</div>,
        };
    });

    const data = React.useMemo(() => list, [notification_list?.data]);
    const columns = React.useMemo(
        () => [
            {
                Header: "No",
                padding: '20',
                accessor: "col1",
                textAlign: "center",
                width: "8%",
            },
            {
                Header: "Date & Time",
                padding: '20',
                accessor: "col2",
                textAlign: "left",
                width: "20%",
            },
            {
                Header: "Image",
                padding: '20',
                accessor: "col3",
                textAlign: "left",
                Cell: ({cell}) => (                 
                    <div style={{minWidth:80}}>
                        {cell.value !== null && <img src={env.baseUrl+cell.value} style={{height:50}}/> }
                    </div>
                ),
                width: "10%",
            },
            {
                Header: "Title",
                padding: '20',
                accessor: "col4",
                textAlign: "left",
                width: "10%",
            },
            {
                Header: "Message",
                padding: '20',
                accessor: "col5",
                textAlign: "left",
            }
        ],
        [notification_list?.data]
    );

    function dateFormat(params) {
        var date = new Date(params);
        var tahun = date.getFullYear();
        var bulan = date.getMonth();
        var tanggal = date.getDate();
        var hari = date.getDay();
        var jam = date.getHours();
        var menit = date.getMinutes();
        var detik = date.getSeconds();
        switch (hari) {
            case 0:
                hari = "Sunday";
                break;
            case 1:
                hari = "Monday";
                break;
            case 2:
                hari = "Tuesday";
                break;
            case 3:
                hari = "Wednesday";
                break;
            case 4:
                hari = "Thursday";
                break;
            case 5:
                hari = "Friday";
                break;
            case 6:
                hari = "Saturday";
                break;
        }
        switch (bulan) {
            case 0:
                bulan = "January";
                break;
            case 1:
                bulan = "February";
                break;
            case 2:
                bulan = "March";
                break;
            case 3:
                bulan = "April";
                break;
            case 4:
                bulan = "May";
                break;
            case 5:
                bulan = "June";
                break;
            case 6:
                bulan = "July";
                break;
            case 7:
                bulan = "August";
                break;
            case 8:
                bulan = "September";
                break;
            case 9:
                bulan = "October";
                break;
            case 10:
                bulan = "November";
                break;
            case 11:
                bulan = "December";
                break;
        }
        var tampilTanggal =
            hari +
            ", " +
            (tanggal < 10 ? "0" + tanggal : tanggal) +
            " " +
            (bulan < 10 ? "0" + bulan : bulan) +
            " " +
            tahun;
        var tampilWaktu =
            (jam < 10 ? "0" + jam : jam) +
            ":" +
            (menit < 10 ? "0" + menit : menit);
        return tampilTanggal + " " + tampilWaktu;
    }

    const customStyles = {
        content: {
            top: "50%",
            left: "50%",
            right: "auto",
            bottom: "auto",
            marginRight: "-50%",
            transform: "translate(-50%, -50%)",
            width: "74%",
        },
        overlay: {
            backgroundColor: "rgba(0,0,0,0.5)",
        }, customStyles
    };


    if (app_selected === '-') {
        return (
            <center
                style={{
                    paddingTop: 90,
                }}
            >
                <img
                    src="/images/select.png"
                    className="img-info-upgrade"
                    alt="logo"
                />
                <br />
                <br />
                <text
                    className="txt-info-upgrade"
                >
                    To use the feature, please select the app first
                </text>
                <br />
            </center>
        );
    } else if (parseInt(app_plan_id) < 2) {
        return (
            <center
                style={{
                    paddingTop: 90,
                }}
            >
                <img
                    src="/images/content.png"
                    className="img-info-upgrade"
                    alt="logo"
                />
                <br />
                <br />
                <text
                    className="txt-info-upgrade"
                >
                    Your current
                    plan can not use
                    push message
                    notification,
                </text>
                <br />
                <text
                    className="txt-info-upgrade"
                >
                    please change
                    plan to use the
                    feature
                </text>
                <br />
                <br />
                <button onClick={() => {
                    setIsOpen(true);
                }} className="btn btn-primary profile-button">
                    Change Plan
                </button>
                <Modal
                    isOpen={modalIsOpen}
                    onRequestClose={() => {
                        setIsOpen(false);
                    }}
                    style={customStyles}
                    contentLabel="Example Modal"
                >
                    <ChangePlan
                        app_id={parseInt(app_selected)}                    
                        onRequestClose={() => {
                            setIsOpen(false);
                        }}
                    />
                </Modal>
            </center>
        );
    } else if (notification_list.data.length === 0) {
        return (
            <center
                style={{
                    paddingTop: 90,
                }}
            >
                <img
                    src="/images/folder.png"
                    className="img-info-upgrade"
                    alt="logo"
                />
                <br />
                <br />
                <text
                    className="txt-info-upgrade"
                >
                    No History yet.
                </text>
            </center>
        );
    } else {

        return (
            <>
                <div
                    className="row rfml"
                    style={{
                        overflowX: "scroll",
                        margin: 1,
                        borderWidth: 1,
                        borderColor: "black",
                    }}
                >
                    <Table
                        data={data}
                        columns={columns}
                        setPage={(val) => {
                            SetPage(val);
                        }}
                        setPerPage={(val) => {
                            SetPerPage(val);
                        }}
                        currentpage={notification_list?.data?.currentPage}
                        perPage={perPage}
                        totalPage={notification_list?.data?.totalPages}
                    />
                </div>
            </>
        )
    }
};

export default NotificationTable;

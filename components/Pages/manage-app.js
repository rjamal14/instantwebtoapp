import React from "react";
import "@material/data-table/dist/mdc.data-table.css";
import "@rmwc/data-table/data-table.css";
import "@rmwc/icon/icon.css";
import Table from "../Common/table";
import ChangePlan from "../../pages/app-list/[id]/change-plan";
import { useDispatch, useSelector } from "react-redux";
import {
    getDetailAppList,
    AppreBuild,
    getAppBuildList,
    getProfile,
} from "../../actions";
var localStorage = require("localStorage");
import { useRouter } from "next/router";
import Swal from "sweetalert2";
import env from "config/env";
import { io } from "socket.io-client";
import Modal from "react-modal";
import Loader from '@/components/_App/Loader'

const AppList = () => {
    const dispatch = useDispatch();
    const [page, SetPage] = React.useState(1);
    const [perPage, SetPerPage] = React.useState(5);
    const [result_queue, Setresult_queue] = React.useState([]);
    const result = useSelector((state) => state.app_build.result);
    const profile = useSelector((state) => state.profile.profile);
    const detail = useSelector((state) => state.app_list.detail);
    const status_code = useSelector((state) => state.app_list.status_code);
    const token = localStorage.getItem("token") ?? null;
    const router = useRouter();
    const [modalIsOpen, setIsOpen] = React.useState(false);
    const [statusBuild, setStatusBuild] = React.useState("");
    const [loading, SetLoading] = React.useState(false);

    function openModal() {
        setIsOpen(true);
    }

    React.useEffect(() => {
        if (token !== null) {
            dispatch(getProfile(token))
                .then((res) => { })
                .catch((err) => {
                    console.log("err", err);
                    localStorage.clear();
                    router.replace("/login");
                    Swal.fire({
                        icon: "warning",
                        title: "Oops...",
                        text: "login session has ended, please login again!",
                        confirmButtonColor: "#2e9afe",
                    });
                });
        }
    }, [token]);

    const socket = io.connect(env.UrlSocket, {
        query: { token },
        transports: ["websocket", "polling", "flashsocket"],
    });

    React.useEffect(() => {
        if (profile.user_id) {
            socket.on("connect", () => {
                console.log("socket connect"); // x8WIv7-mJelg7on_ALbx
            });
            socket.on("disconnect", () => {
                console.log("socket disconnect"); // x8WIv7-mJelg7on_ALbx
            });
            socket.on("buildStatus" + profile.user_id, (data) => {
                const id = parseInt(window.location.pathname.split("/")[2]);
                var listen = data.filter((o) => o.appId === id);
                if (listen.length > 0) {
                    if (
                        listen[0].status === "uploaded" ||
                        listen[0].status === "error" ||
                        listen[0].status === "cancel"
                    ) {
                        Setresult_queue([]);
                    } else {
                        Setresult_queue(listen);
                    }
                    if (listen[0].status !== statusBuild) {
                        dispatch(getAppBuildList(id, page, perPage));
                        setStatusBuild(listen[0].status);
                    }
                }
            });
            socket.emit("statusBuild", { user_id: profile.user_id });
        }
    }, [profile.user_id]);

    function cancleBuild(id) {
        socket.emit("cancelBuild", { id: id });
        dispatch(getAppBuildList(id_app, page, perPage));
        Setresult_queue([]);
        socket.emit("statusBuild", { user_id: profile.user_id });
        const id_app = window.location.pathname.split("/")[2];
    }

    function dateFormat(params) {
        var date = new Date(params);
        var tahun = date.getFullYear();
        var bulan = date.getMonth();
        var tanggal = date.getDate();
        var hari = date.getDay();
        var jam = date.getHours();
        var menit = date.getMinutes();
        var detik = date.getSeconds();
        switch (hari) {
            case 0:
                hari = "Sunday";
                break;
            case 1:
                hari = "Monday";
                break;
            case 2:
                hari = "Tuesday";
                break;
            case 3:
                hari = "Wednesday";
                break;
            case 4:
                hari = "Thursday";
                break;
            case 5:
                hari = "Friday";
                break;
            case 6:
                hari = "Saturday";
                break;
        }
        switch (bulan) {
            case 0:
                bulan = "January";
                break;
            case 1:
                bulan = "February";
                break;
            case 2:
                bulan = "March";
                break;
            case 3:
                bulan = "April";
                break;
            case 4:
                bulan = "May";
                break;
            case 5:
                bulan = "June";
                break;
            case 6:
                bulan = "July";
                break;
            case 7:
                bulan = "August";
                break;
            case 8:
                bulan = "September";
                break;
            case 9:
                bulan = "October";
                break;
            case 10:
                bulan = "November";
                break;
            case 11:
                bulan = "December";
                break;
        }
        var tampilTanggal =
            hari +
            ", " +
            (tanggal < 10 ? "0" + tanggal : tanggal) +
            " " +
            (bulan < 10 ? "0" + bulan : bulan) +
            " " +
            tahun;
        var tampilWaktu =
            (jam < 10 ? "0" + jam : jam) +
            ":" +
            (menit < 10 ? "0" + menit : menit);
        return tampilTanggal + " " + tampilWaktu;
    }

    React.useEffect(() => {
        if (token === null) {
            localStorage.clear();
            router.replace("/login");
        }
        if (status_code === 401) {
            localStorage.clear();
            router.replace("/login");
            Swal.fire({
                icon: "warning",
                title: "Oops...",
                text: "login session has ended, please login again!",
                confirmButtonColor: "#2e9afe",
            });
        }
    }, [token, status_code]);

    function generateStatus(status) {
        const stat = status.charAt(0).toUpperCase() + status.slice(1);
        if (stat === "Queue") {
            return "Queueing";
        } else if (stat === "Process") {
            return "Building app";
        } else if (stat === "Finish") {
            return "Build completed";
        } else if (stat === "Error") {
            return "Error, build cancelled, please rebuild";
        } else if (stat === "Cancel") {
            return "Build cancelled, please rebuild";
        } else if (stat === "Delete") {
            return "Deleted";
        } else {
            return stat;
        }
    }

    React.useEffect(() => {
        if (token !== null || status_code === 401) {
            const id = window.location.pathname.split("/")[2];
            dispatch(getDetailAppList(id));
            dispatch(getAppBuildList(id, page, perPage));
        }
    }, [token, status_code, page, perPage]);

    const list = result.data?.map((i, index) => {
        return {
            col1: <div style={{ marginRight: 20, marginLeft: 20 }}>{((page - 1) * perPage + (index + 1)).toString()}</div>,
            col2: <div>{i?.app_version}</div>,
            col3: <div style={{ minWidth: 220 }}>{generateStatus(i?.status)}</div>,
            col4: i,
            col5: <div style={{ minWidth: 200 }}>{dateFormat(i?.createdAt)}</div>,
        };
    });

    const data = React.useMemo(() => list, [result.data]);


    function generateTextDownload(val) {
        if (val.status === "delete" || val.status === "error" || val.status === "cancel") {
            return 'Link is not available';
        } else {
            return 'Link is not yet available';
        }
    }

    const columns = React.useMemo(
        () => [
            {
                Header: "No",
                accessor: "col1",
                textAlign: "center",
                width: "10%",
            },
            {
                Header: "Version",
                accessor: "col2",
                textAlign: "left",
                width: "10%",
            },
            {
                Header: "Status",
                accessor: "col3",
                textAlign: "left",
            },
            {
                Header: "Download link",
                accessor: "col4",
                textAlign: "left",
                Cell: ({ cell }) => {
                    if (cell.value.link !== null) {
                        return (
                            <div style={{ minWidth: 150 }}>
                                <button
                                    className={"btn btn-primary " + (cell.value.build === 'ipa' ? "ios-button" : cell.value.build === 'apk' ? "apk-button" : "aab-button")}

                                    onClick={() => {
                                        window.open(cell.value.link);
                                    }}
                                    type="submit"
                                >
                                    <i class="fa fa-download"></i> {(cell.value.build === 'ipa' ? "iOs" : "Android")} {cell.value.build.toUpperCase()}
                                </button>
                            </div>
                        );
                    } else {
                        return (
                            <div style={{ minWidth: 150 }}>
                                <text>{generateTextDownload(cell.value)}</text>
                            </div>
                        );
                    }
                },
            },
            {
                Header: "Created at",
                accessor: "col5",
                textAlign: "left",
            },
        ],
        [result.data]
    );

    const columns_queue = React.useMemo(
        () => [
            {
                Header: "Queue number",
                accessor: "col1",
                textAlign: "center",
            },
            {
                Header: "Build type",
                accessor: "col2",
                textAlign: "left",
            },
            {
                Header: "Version",
                accessor: "col3",
                textAlign: "left",
            },
            {
                Header: "Build status",
                accessor: "col4",
                textAlign: "left",
            },
            {
                Header: "Action",
                accessor: "col5",
                textAlign: "left",
                Cell: ({ cell }) => {
                    if (cell.value.status === "queue") {
                        return (
                            <div>
                                <button
                                    className="btn btn-primary profile-button"
                                    onClick={() => {
                                        cancleBuild(cell.value.id);
                                    }}
                                    type="submit"
                                >
                                    Cancel
                                </button>
                            </div>
                        );
                    } else {
                        return <text></text>;
                    }
                },
            },
        ],
        [result_queue]
    );

    const list_queue = result_queue?.map((i, index) => {
        return {
            col1: <div style={{ minWidth: 110 }}>{i?.queueNumber}</div>,
            col2: <div style={{ minWidth: 70 }}>{i?.typeBuild?.toUpperCase()}</div>,
            col3: <div>{i?.versionName}</div>,
            col4: <div style={{ minWidth: 220 }}>{generateStatus(i?.status)}</div>,
            col5: i,
        };
    });
    const data_queue = React.useMemo(() => list_queue, [result_queue]);

    const customStyles = {
        content: {
            top: "50%",
            left: "50%",
            right: "auto",
            bottom: "auto",
            marginRight: "-50%",
            transform: "translate(-50%, -50%)",
            width: "74%",
        },
        overlay: {
            backgroundColor: "rgba(0,0,0,0.5)",
        },
    };

    function generateRebuild(type) {
        dispatch(
            AppreBuild({
                app_id: detail
                    .app_builds[0]
                    .app_id,
                build: type,
            })
        )
            .then(
                (
                    res
                ) => {
                    SetLoading(false)
                    Swal.fire({
                        icon: "success",
                        title: "Success",
                        text: 'System is processing your build request',
                        confirmButtonColor: "#2e9afe",
                    });
                    dispatch(
                        getAppBuildList(
                            detail.id,
                            page,
                            perPage
                        )
                    );
                }
            )
            .catch(
                (
                    err
                ) => {
                    SetLoading(false)
                    if (err.code === 41) {
                        Swal.fire({
                            title: "Oops...",
                            text: err.message,
                            icon: "error",
                            allowOutsideClick: false,
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonColor:
                                "#2e9afe",
                            cancelButtonColor:
                                "#FF4466",
                            confirmButtonText:
                                "Change Plan",
                            cancelButtonText:
                                "Close",
                        }).then((result) => {
                            if (result.isConfirmed) {
                                openModal();
                            }
                        });
                    } else {
                        Swal.fire({
                            icon: "error",
                            title: "Oops...",
                            text: err.message,
                            confirmButtonColor: "#2e9afe",
                        });
                    }
                }
            );
    }

    function rebuildApp() {
        SetLoading(true)
        Swal.fire({
            title: "",
            text: "System will create new version based on latest design and increase the version number, please choose your build type:",
            icon: "question",
            allowOutsideClick: false,
            showCloseButton: true,
            showCancelButton: true,
            showDenyButton: true,
            confirmButtonColor:
                "#2e9afe",
            cancelButtonColor:
                "#2e9afe",
            denyButtonColor:
                "#2e9afe",
            confirmButtonText:
                "Android APK",
            cancelButtonText:
                "iOs IPA",
            denyButtonText: `Android AAB`,

        }).then((result) => {
            if (result.isConfirmed) {
                generateRebuild('apk')
            } else if (result.isDenied) {
                generateRebuild('aab')
            } else if (result.dismiss !== "close") {
                generateRebuild('ipa')
            } else {
                SetLoading(false)
            }
        });
    }

    return (
        <>
            {loading && <Loader />}
            <div className="bg-white">
                <div className="how-it-works-area margin-auto-add">
                    <div className="col-lg-12 col-md-12 p-0">
                        <div className="content radius-6 margin-auto radius-6">
                            <div
                                className="row rfml"
                                style={{
                                    marginBottom: 50,
                                    overflowX: "auto",
                                }}
                            >
                                <tr>
                                    <td width={110}>Package Name</td>
                                    <td width={20}> : </td>
                                    <td style={{maxWidth:45, whiteSpace: 'nowrap'}}>{detail?.app_package}</td>
                                </tr>
                                <tr>
                                    <td width={110}>App Name</td>
                                    <td width={20}> : </td>
                                    <td style={{maxWidth:45, whiteSpace: 'nowrap'}}>{detail?.app_name}</td>
                                </tr>
                                <tr>
                                    <td width={110}>Domain</td>
                                    <td width={20}> : </td>
                                    <td style={{maxWidth:45, whiteSpace: 'nowrap'}}>{detail?.domain_name}</td>
                                </tr>
                                <tr>
                                    <td width={110}>Plan</td>
                                    <td width={20}> : </td>
                                    <td style={{maxWidth:45, whiteSpace: 'nowrap'}}>
                                        {detail?.plan?.name}
                                    </td>
                                </tr>
                                <tr style={{ marginTop: 10 }}>
                                    <center>
                                        <td>
                                            <button
                                                className="btn btn-primary profile-button"
                                                disabled={
                                                    detail.app_builds
                                                        .length === 0
                                                }
                                                onClick={() => {
                                                    router.replace(
                                                        "/app-list/" +
                                                        detail?.id +
                                                        "/edit/"
                                                    );
                                                }}
                                                style={{ marginRight: 10 }}
                                                type="submit"
                                            >
                                                Edit
                                            </button>
                                        </td>
                                        <td>
                                            <button
                                                style={{ marginRight: 10 }}
                                                className="btn btn-primary profile-button"
                                                onClick={() => {
                                                    rebuildApp()
                                                }}
                                                disabled={
                                                    detail.app_builds
                                                        .length === 0
                                                }
                                                type="submit"
                                            >
                                                Rebuild
                                            </button>
                                        </td>
                                        <td>
                                            {detail.plan_id < 3 && (
                                                <button
                                                    style={{
                                                        whiteSpace: 'nowrap'
                                                    }}
                                                    disabled={
                                                        detail.app_builds
                                                            .length === 0
                                                    }
                                                    className="btn btn-primary profile-button"

                                                    onClick={() => {
                                                        openModal();
                                                    }}
                                                    type="submit"
                                                >
                                                    Change Plan
                                                </button>
                                            )}
                                        </td>
                                    </center>
                                </tr>
                                {detail.app_builds.length === 0 && (
                                    <text style={{ color: 'red', marginTop: 8 }}>You have pending payment, please complete it to continue to edit or rebuild this app.</text>
                                )}
                            </div>
                            <div
                                className="row"
                                style={{
                                    overflowX: "scroll",
                                    marginBottom: 50,
                                    margin: 1,
                                    borderWidth: 1,
                                    borderColor: "black",
                                }}
                            >
                                <Table
                                    data={data}
                                    columns={columns}
                                    setPage={(val) => {
                                        SetPage(val);
                                    }}
                                    setPerPage={(val) => {
                                        SetPerPage(val);
                                    }}
                                    currentpage={result?.currentPage}
                                    perPage={perPage}
                                    totalPage={result?.totalPages}
                                />
                            </div>
                            {data_queue.length > 0 && (
                                <div
                                    className="row"
                                    style={{
                                        overflowX: "scroll",
                                        margin: 1,
                                        borderWidth: 1,
                                        borderColor: "black",
                                    }}
                                >
                                    <Table
                                        pagination={false}
                                        noData="All task has been completed!"
                                        data={data_queue}
                                        columns={columns_queue}
                                    />
                                </div>
                            )}
                            <br />
                            <br />
                        </div>
                    </div>
                </div>
            </div>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={() => {
                    setIsOpen(false);
                }}
                style={customStyles}
                contentLabel="Example Modal"
            >
                <ChangePlan
                    SetLoading={(val) => {
                        SetLoading(val)
                    }}
                    onRequestClose={() => {
                        setIsOpen(false);
                    }}
                />
            </Modal>
        </>
    );
};

export default AppList;

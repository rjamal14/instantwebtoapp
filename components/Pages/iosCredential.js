import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getDetailIOSAppCredential, CreateIOSAppCredential } from "../../actions";
var localStorage = require("localStorage");
import Swal from "sweetalert2";
import { useForm } from "react-hook-form";
import Modal from "react-modal";
import ChangePlan from "../../pages/app-list/[id]/change-plan";
import Loader from '@/components/_App/Loader'
import { io } from "socket.io-client";
import env from "config/env";
import FileUpload from '@/components/Common/FileUpload'

const AppList = () => {
    const dispatch = useDispatch();
    const profile = useSelector((state) => state.profile.profile);
    const token = localStorage.getItem("token") ?? null;
    const [editable, setEditable] = React.useState(false);
    const detail = useSelector((state) => state.app_list.detail);
    const [count, setCount] = React.useState(0);
    const [modalIsOpen, setIsOpen] = React.useState(false);
    const [loading, SetLoading] = React.useState(false);
    const [typeCer, SetTypeCer] = React.useState(1);
    const [step, SetStep] = React.useState(0);
    const [generate, setGenerate] = React.useState(false);
    const [fileCer, setFileCer] = React.useState([]);
    const [fileProfile, setFileProfile] = React.useState([]);
    const [editFileCer, setEditFileCer] = React.useState(0);
    const [editFileProfile, setEditFileProfile] = React.useState(0);


    const toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });

    const socket = io.connect(env.UrlSocket, {
        query: { token },
        transports: ["websocket", "polling", "flashsocket"],
    });

    const {
        register,
        handleSubmit,
        reset,
        getValues,
        setValue,
        formState: { errors },
    } = useForm();

    React.useEffect(() => {

        if (detail.id) {
            socket.on("connect", () => {
                console.log("socket connect");
            });
            socket.on("disconnect", () => {
                socket.off("generateiOSCertificateStatus" + detail.id)
            });
            socket.on("generateiOSCertificateStatus" + detail.id, (data) => {
                if (data.status === "success") {
                    setValue("file_csr_key", data.result[0].url)
                    setValue("file_csr", data.result[1].url)
                    SetTypeCer(6)
                    setGenerate(true)
                } else if (data.status === "failed") {
                    SetTypeCer(5)
                }
            });
        }

        getDetail()
    }, [profile, detail]);

    function setDefValue(res) {
        setValue('identifier', res.identifier)
        setValue('permission_page', res.permission_page)
        setFileCer(res.file_cer)
        setFileProfile(res.file_profile)
        setValue('file_csr', res.file_csr)
        setValue('file_csr_key', res.file_csr_key)

        if (res.file_profile === null) {
            setEditFileProfile(0)
        } else {
            setEditFileProfile(1)
        }

        if (res.file_cer === null) {
            setEditFileCer(0)
        } else {
            setEditFileCer(1)
        }

        if (res) {
            if (res.permission_page !== null) {
                SetStep(1)
            }
            if (res.identifier !== null && res.permission_page !== null) {
                SetStep(2)
                SetTypeCer(1)
            }

            if (res.file_cer !== null && res.file_csr !== null && res.file_csr_key !== null) {
                SetStep(3)
                SetTypeCer(6)
            }
        }
    }

    function getDetail() {
        dispatch(getDetailIOSAppCredential(detail.id)).then((res) => {
            setDefValue(res)
        })
            .catch((err) => {
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Something went wrong!",
                    confirmButtonColor: "#2e9afe",
                });
            });
    }

    const onSubmit = async (data) => {
        SetLoading(true)
        var payload = data

        if (Array.isArray(fileProfile) && fileProfile?.length > 0) {
            const file = fileProfile[0];
            payload['file_profile'] = await toBase64(file);
        }

        if (Array.isArray(fileCer) && fileCer?.length > 0) {
            const file = fileCer[0];
            payload['file_cer'] = await toBase64(file);
        }

        dispatch(
            CreateIOSAppCredential(payload)
        )
            .then((res) => {
                SetLoading(false)
                Swal.fire({
                    icon: "success",
                    title: "Success",
                    text: 'Changes saved succesfully',
                    confirmButtonColor: "#2e9afe",
                });
                setEditable(false);
                reset();
                setValue("app_id", detail.id);
                setDefValue(res)
            })
            .catch((err) => {
                SetLoading(false)
                setEditable(false);
                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: err,
                    confirmButtonColor: "#2e9afe",
                });
            });

    };

    useEffect(() => {
        setValue("app_id", detail.id);
    }, [detail]);

    const customStyles = {
        content: {
            top: "50%",
            left: "50%",
            right: "auto",
            bottom: "auto",
            marginRight: "-50%",
            transform: "translate(-50%, -50%)",
            width: "74%",
        },
        overlay: {
            backgroundColor: "rgba(0,0,0,0.5)",
        }, customStyles
    };

    function renderCertificate() {
        if (typeCer === 1) {
            return (
                <div>
                    <br />
                    <text>Certificate has not been created yet</text>
                </div>
            );
        } else if (typeCer === 2) {
            return (
                <div>
                    <br />
                    <div onClick={() => {
                        SetTypeCer(3)
                        socket.emit('generateiOSCertificate', { identifier_id: getValues('identifier'), app_id: getValues('app_id'), email: profile.email })
                    }} className="btn btn-primary profile-button"><i class="fa fa-download"></i> Generate Signin Certificate</div>
                </div>
            );
        } else if (typeCer === 3) {
            return (
                <div className="col-md-6">
                    <br />
                    <div className="row">
                        <div className="col-lg-2 col-xs-12">
                            <div class="loader" />
                        </div>
                        <div className="col">
                            <text style={{ color: '#3498db' }}>Generating Signing Certificate...</text>
                        </div>
                    </div>
                </div>
            );
        } else if (typeCer === 4) {
            return (
                <div className="col-md-6">
                    <br />
                    <div className="row">
                        <div className="col-1">
                            <i style={{ color: '#3498db' }} class="fa fa-check" />
                        </div>
                        <div className="col">
                            <text style={{ color: '#3498db' }}>Generating Signing Certificate Success</text>
                        </div>
                    </div>
                </div>
            );
        } else if (typeCer === 5) {
            return (
                <div className="col-md-6">
                    <br />
                    <div className="row">
                        <div className="col-1">
                            <i style={{ color: 'red' }} class="fa fa-times-circle" />
                        </div>
                        <div className="col">
                            <text style={{ color: 'red' }}>Generating Signing Certificate failed</text>
                        </div>
                    </div>
                </div>
            );
        } else if (typeCer === 6) {
            return (
                <div className="form-group mt-20">
                    <text className="" style={{ fontWeight: 'bold', color: '#333333' }}>Your Signin Certificate</text>
                    <br />
                    <br />
                    <label
                        className="mt-10"
                        for="exampleInputEmail1"
                    >
                        Download and save this .key file for future use.
                    </label>
                    <br />
                    <div>
                        <br />
                        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                            <div style={{
                                paddingTop: 10,
                                paddingBottom: 10,
                                marginRight: 20
                            }}>
                                <i style={{ color: '#3498db' }} class="fa fa-check" /> &nbsp; <text> Signin Certificate .key</text>
                            </div>
                            <div>
                                <div
                                    onClick={() => {
                                        window.open(getValues('file_csr_key'))
                                    }}
                                    className="btn btn-primary profile-button-disable"
                                >
                                    Download
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <label
                        className="mt-10"
                        for="exampleInputEmail1"
                    >
                        Download and use this .csr file to create .cer file in your Apple developer account.
                    </label>
                    <br />
                    <div>
                        <br />
                        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                            <div style={{
                                paddingTop: 10,
                                paddingBottom: 10,
                                marginRight: 20
                            }}>
                                <i style={{ color: '#3498db' }} class="fa fa-check" /> &nbsp; <text> Signin Certificate .csr</text>
                            </div>
                            <div>
                                <div
                                    onClick={() => {
                                        window.open(getValues('file_csr'))
                                    }}
                                    className="btn btn-primary profile-button-disable"
                                >
                                    Download
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <br />
                    <text className="" style={{ fontWeight: 'bold', color: '#333333' }}>iOs Certificate</text><br />
                    <label
                        className="mt-10"
                        for="exampleInputEmail1"
                    >
                        Using .csr above, create iOS Distribution (App Store and Ad Hoc) certificate in your Apple developer account, <br /> then upload the .cer file here
                    </label>
                    {generateFileUploadCertificate()}
                    {errors.file_cer && (
                        <span className="text-validator">
                            {
                                errors?.file_cer
                                    ?.message
                            }
                        </span>
                    )}
                </div>
            );
        }

    }

    const handleFileSelected = (e) => {
        const files = Array.from(e.target.files)
        setFileCer(files)
    }

    const handleFileSelectedProfile = (e) => {
        const files = Array.from(e.target.files)
        setFileProfile(files)
    }

    function renderComponent(type) {
        if (detail.plan_id < 3 || detail.app_builds.length === 0) {
            return (
                <div className="how-it-works-area margin-auto">
                    <div className="how-it-works-content">
                        <div className="col-lg-12 col-md-12 p-0">
                            <div className="margin-auto-add">
                                <div className="radius-6 mb-5">
                                    <div className="row">
                                        <div className="col-md-12 border-right p-5 pt-1 pb-1">
                                            <center
                                                style={{
                                                    paddingTop: 90,
                                                }}
                                            >
                                                <img
                                                    src="/images/content.png"
                                                    className="img-info-upgrade"
                                                    alt="logo"
                                                />
                                                <br />
                                                <br />
                                                <br />
                                                <text className="txt-info-upgrade">
                                                    Your current
                                                    plan can not build
                                                    iOs IPA,
                                                </text>
                                                <br />
                                                <text className="txt-info-upgrade">
                                                    please change
                                                    plan to use the
                                                    feature
                                                </text>
                                                <br />
                                                <br />
                                                <br />
                                                <button onClick={() => {
                                                    setIsOpen(true);
                                                }} className="btn btn-primary profile-button">
                                                    Change Plan
                                                </button>
                                                <br />
                                                <br />
                                                <br />
                                                <br />
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="how-it-works-area margin-auto">
                    <div className="how-it-works-content">
                        <div className="col-lg-12 col-md-12 p-0">
                            <div className="margin-auto-add">
                                <div className="radius-6 mb-5">
                                    <div className="row">
                                        <div className="col-md-12 border-right p-5 pt-1 pb-1">
                                            <form
                                                onSubmit={handleSubmit(
                                                    onSubmit
                                                )}
                                            >
                                                <div>
                                                    <br />
                                                    <text className="mt-10 ios-title">iOs IPA </text><br />
                                                    <text className="mt-10 ios-subtitle">Before build your iOs IPA, please complete these 4 steps</text>
                                                    <a className="mt-10 ios-subtitle" style={{color:'#2e9afe'}} target="_blank" href="https://docs.appsinstant.com/knowledgebase/ios-permission-page-setup/"> Guideline to setup your IPA</a>
                                                    <div className="row ml-30">
                                                        <div style={{ padding: 0 }} className="form-group mt-50 ml-10">
                                                            <div className="row">
                                                                <div className="col step-ios">1/4</div>
                                                                <div className="col">
                                                                    <text className="mt-10 ios-subtitle-bold"> Permission page theme </text>
                                                                </div>
                                                            </div>
                                                            <div className="ml-30">
                                                                <div className="form-group mt-20">
                                                                    <div className="form-group mt-20">
                                                                        <label
                                                                            className="mb-10"
                                                                            for="exampleInputEmail1"
                                                                        >
                                                                            Choose the theme for iOs permission page, eq: Permission for Push notification
                                                                        </label>
                                                                        <select
                                                                            disabled={!editable}
                                                                            class="form-control"
                                                                            {...register("permission_page", {
                                                                                required:
                                                                                    "Theme for iOs permission page is required",
                                                                            })}
                                                                        >
                                                                            <option value="">
                                                                                Select Permission page theme
                                                                            </option>
                                                                            <option value="light">
                                                                                Light
                                                                            </option>
                                                                            <option value="dark">
                                                                                Dark
                                                                            </option>
                                                                        </select>
                                                                        {errors.permission_page && (
                                                                            <span className="text-validator">
                                                                                {
                                                                                    errors?.permission_page
                                                                                        ?.message
                                                                                }
                                                                            </span>
                                                                        )}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {step > 0 &&
                                                            <div style={{ padding: 0 }} className="form-group mt-50 ml-10">
                                                                <div className="row">
                                                                    <div className="col step-ios">2/4</div>
                                                                    <div className="col">
                                                                        <text className="mt-10 ios-subtitle-bold"> Identifier </text>
                                                                    </div>
                                                                </div>
                                                                <div className="ml-30">
                                                                    <div className="form-group mt-20">
                                                                        <div className="form-group mt-20">
                                                                            <label
                                                                                className="mb-10"
                                                                                for="exampleInputEmail1"
                                                                            >
                                                                                In your Apple Developer account, Register a new identifier, choose App IDs, then paste the registered Bundle ID here
                                                                            </label>
                                                                            <input
                                                                                disabled={!editable}
                                                                                type="text"
                                                                                className="form-control"
                                                                                placeholder="Enter Identifier"
                                                                                {...register("identifier", {
                                                                                    required:
                                                                                        "Identifier is required",
                                                                                    pattern: /^[a-z]\w*(\.[a-z]\w*)+$/i,
                                                                                    maxLength: 25
                                                                                })}
                                                                            />
                                                                            {errors.identifier && (
                                                                                <span className="text-validator">
                                                                                    {errors?.identifier?.message}
                                                                                </span>
                                                                            )}
                                                                            {errors?.identifier?.type === "pattern" && (
                                                                                <span className="text-validator">
                                                                                    Input does not match the format, eq: com.companyname.appname
                                                                                </span>
                                                                            )}
                                                                            {errors?.identifier?.type === "maxLength" && (
                                                                                <span className="text-validator">
                                                                                    Playsore package name cannot exceed 25 characters
                                                                                </span>
                                                                            )}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        }
                                                        {step > 1 && <div style={{ padding: 0 }} className="form-group mt-50 ml-10">
                                                            <div className="row">
                                                                <div className="col step-ios">3/4</div>
                                                                <div className="col">
                                                                    <text className="mt-10 ios-subtitle-bold"> Certificate </text>
                                                                </div>
                                                            </div>
                                                            <div className="ml-30">
                                                                {renderCertificate()}
                                                            </div>
                                                        </div>}
                                                        {step > 2 &&
                                                            <div style={{ padding: 0 }} className="form-group mt-50 ml-10">
                                                                <div className="row">
                                                                    <div className="col step-ios">4/4</div>
                                                                    <div className="col">
                                                                        <text className="mt-10 ios-subtitle-bold"> Profile </text>
                                                                    </div>
                                                                </div>
                                                                <div className="ml-30">
                                                                    <div className="form-group mt-20">
                                                                        <div className="form-group mt-20">
                                                                            <label
                                                                                className="mt-10"
                                                                                for="exampleInputEmail1"
                                                                            >
                                                                                In your Apple Developer account, Create iOS App Development, then upload the .mobileprovision file here
                                                                            </label>
                                                                            {generateFileUploadProfile()}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        }
                                                        <div className="form-group ml-10">
                                                            <div className="ml-30">
                                                                <div className="form-group mt-20">
                                                                    <div className="form-group mt-20">
                                                                        <div className="mt-5" style={{
                                                                            display: 'flex', display: 'flex', flexWrap: 'wrap',
                                                                            justifyContent: 'flex-end',
                                                                            marginBottom: 50
                                                                        }}>
                                                                            {editable ? (
                                                                                <div>
                                                                                    <div
                                                                                        style={{
                                                                                            marginRight: 20,
                                                                                        }}
                                                                                        onClick={() => {
                                                                                            setEditable(
                                                                                                false
                                                                                            );

                                                                                            if (generate) {
                                                                                                SetTypeCer(1)
                                                                                                setGenerate(false)
                                                                                                setValue("file_csr_key", null)
                                                                                                setValue("file_csr", null)
                                                                                            }
                                                                                        }}
                                                                                        className="btn btn-primary profile-button-disable"
                                                                                    >
                                                                                        Cancel
                                                                                    </div>
                                                                                    <button
                                                                                        className="btn btn-primary profile-button"
                                                                                        type="submit"
                                                                                    >
                                                                                        Save
                                                                                    </button>
                                                                                </div>
                                                                            ) : (
                                                                                <div
                                                                                    onClick={() => {
                                                                                        setEditable(
                                                                                            true
                                                                                        );
                                                                                        if (typeCer === 1) {
                                                                                            SetTypeCer(2)
                                                                                        }
                                                                                    }}
                                                                                    className="btn btn-primary profile-button"
                                                                                >
                                                                                    Edit
                                                                                </div>
                                                                            )}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }

    function generateFileUploadProfile() {
        if (editFileProfile === 0) {
            return (
                <div className="row mt-12">
                    <div className="col-12">
                        <label for="file-upload" style={{ display: 'block' }}>
                            <FileUpload data={fileProfile} />
                        </label>
                        <input
                            disabled={!editable}
                            id="file-upload"
                            type="file"
                            style={{
                                height: 38
                            }}
                            accept=".mobileprovision"
                            className="form-control"
                            placeholder="Enter App icon"
                            onChange={handleFileSelectedProfile}
                        />
                    </div>
                </div>
            );
        } else if (editFileProfile === 1) {
            return (
                <div>
                    <br />
                    <div style={{ display: 'flex', display: 'flex', flexWrap: 'wrap' }}>
                        <div style={{
                            paddingTop: 10,
                            paddingBottom: 10,
                            marginRight: 20
                        }}>
                            <i style={{ color: '#3498db' }} class="fa fa-check" /> &nbsp;<text> Profile uploaded</text>
                        </div>
                        <div>
                            <div
                                onClick={() => {
                                    window.open(env.baseUrl + fileProfile)
                                }}
                                className="btn btn-primary profile-button-disable"
                            >
                                Download
                            </div>
                            {editable && <div
                                onClick={() => {
                                    setEditFileProfile(2)
                                }}
                                style={{ marginLeft: 10 }}
                                className="btn btn-primary profile-button"
                                type="submit"
                            >
                                Edit
                            </div>}
                        </div>
                    </div>
                </div>
            );
        } else if (editFileProfile === 2) {
            return (
                <div className="row mt-10" style={{ padding: 0 }}>
                    <div className="col-lg-10 col-xs-12">
                        <label for="file-upload-1" style={{ display: 'block' }}>
                            <FileUpload data={fileProfile} />
                        </label>
                        <input
                            disabled={!editable}
                            id="file-upload-1"
                            type="file"
                            style={{
                                height: 38
                            }}
                            accept=".mobileprovision"
                            className="form-control"
                            placeholder="Enter App icon"
                            onChange={handleFileSelectedProfile}
                        />
                    </div>
                    <div className="col-lg-2 col-xs-12">
                        <div
                            style={{
                                marginRight: 20,
                            }}
                            onClick={() => {
                                setEditFileProfile(1)
                            }}
                            className="btn btn-primary profile-button-disable"
                        >
                            Cancel
                        </div>
                    </div>
                </div>
            );
        }
    }

    function generateFileUploadCertificate() {
        if (editFileCer === 0) {
            return (
                <div className="row mt-10">
                    <div className="col-12">
                        <label for="file-upload-2" style={{ display: 'block' }}>
                            <FileUpload data={fileCer} />
                        </label>
                        <input
                            disabled={!editable}
                            id="file-upload-2"
                            type="file"
                            style={{
                                height: 38
                            }}
                            accept=".cer"
                            className="form-control"
                            placeholder="Enter App icon"
                            onChange={handleFileSelected}
                        />
                    </div>
                </div>
            );
        } else if (editFileCer === 1) {
            return (
                <div>
                    <br />
                    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                        <div style={{
                            paddingTop: 10,
                            paddingBottom: 10,
                            marginRight: 20
                        }}>
                            <i style={{ color: '#3498db' }} class="fa fa-check" /> &nbsp; <text> Certificate uploaded</text>
                        </div>
                        <div>
                            <div
                                onClick={() => {
                                    window.open(env.baseUrl + fileCer)
                                }}
                                className="btn btn-primary profile-button-disable"
                            >
                                Download
                            </div>
                        </div>
                        <div className="col">
                            {editable && <div
                                onClick={() => {
                                    setEditFileCer(2)
                                }}
                                style={{ marginLeft: 10 }}
                                className="btn btn-primary profile-button"
                                type="submit"
                            >
                                Edit
                            </div>}

                        </div>
                    </div>
                </div>
            );
        } else if (editFileCer === 2) {
            return (
                <div className="row mt-10" style={{ padding: 0 }}>
                    <div className="col-lg-10 col-xs-12">
                        <label for="file-upload-3" style={{ display: 'block' }}>
                            <FileUpload data={fileCer} />
                        </label>
                        <input
                            disabled={!editable}
                            type="file"
                            id="file-upload-3"
                            style={{
                                height: 38
                            }}
                            accept=".cer"
                            className="form-control"
                            placeholder="Enter App icon"
                            onChange={handleFileSelectedProfile}
                        />
                    </div>
                    <div className="col-lg-2 col-xs-12">
                        <div
                            style={{
                                marginRight: 20,
                            }}
                            onClick={() => {
                                setEditFileCer(1)
                            }}
                            className="btn btn-primary profile-button-disable"
                        >
                            Cancel
                        </div>
                    </div>
                </div>
            );
        }
    }

    return (
        <>
            {loading && <Loader />}
            <div className="bg-white">
                {renderComponent(count)}
                <Modal
                    isOpen={modalIsOpen}
                    onRequestClose={() => {
                        setIsOpen(false);
                    }}
                    style={customStyles}
                    contentLabel="Example Modal"
                >
                    <ChangePlan
                        SetLoading={(val) => {
                            SetLoading(val)
                        }}
                        onRequestClose={() => {
                            setIsOpen(false);
                        }}
                    />
                </Modal>
            </div>
        </>
    );
};

export default AppList;

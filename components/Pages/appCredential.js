import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getDetailAppCredential, CreateAppCredential } from "../../actions";
var localStorage = require("localStorage");
import Swal from "sweetalert2";
import { useForm } from "react-hook-form";
import Modal from "react-modal";
import ChangePlan from "../../pages/app-list/[id]/change-plan";
import Loader from "@/components/_App/Loader";
import FileUpload from "@/components/Common/FileUpload";

const AppList = () => {
  const dispatch = useDispatch();
  const profile = useSelector((state) => state.profile.profile);
  const status_code = useSelector((state) => state.profile.status_code);
  const token = localStorage.getItem("token") ?? null;
  const [editable, setEditable] = React.useState(false);
  const detail = useSelector((state) => state.app_list.detail);
  const [keyAdmin, setKeyAdmin] = React.useState("");
  const [keyClient, setKeyClient] = React.useState("");
  const [keyIOS, setKeyIOS] = React.useState("");
  const [count, setCount] = React.useState(0);
  const [modalIsOpen, setIsOpen] = React.useState(false);
  const [loading, SetLoading] = React.useState(false);
  const [keyAdmin_ORI, setKeyAdmin_ORI] = React.useState(undefined);
  const [keyClient_ORI, setKeyClient_ORI] = React.useState(undefined);
  const [keyIOS_ORI, setKeyIOS_ORI] = React.useState(undefined);

  const {
    register,
    handleSubmit,
    reset,
    getValues,
    setValue,
    formState: { errors },
  } = useForm();

  React.useEffect(() => {
    dispatch(getDetailAppCredential(detail.id, "firebase_admin"))
      .then((res) => {
        setKeyAdmin(res);
      })
      .catch((err) => {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Something went wrong!",
          confirmButtonColor: "#2e9afe",
        });
      });
    dispatch(getDetailAppCredential(detail.id, "firebase"))
      .then((res) => {
        setKeyClient(res);
      })
      .catch((err) => {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Something went wrong!",
          confirmButtonColor: "#2e9afe",
        });
      });
    dispatch(getDetailAppCredential(detail.id, "firebase_ios"))
      .then((res) => {
        setKeyIOS(res);
      })
      .catch((err) => {
        console.log("err", err);
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Something went wrong!",
          confirmButtonColor: "#2e9afe",
        });
      });
  }, [profile, detail]);

  const onSubmit = (data) => {
    SetLoading(true);
    setTimeout(() => {
      // firebase admin
      dispatch(
        CreateAppCredential({
          app_id: data.app_id,
          type: "firebase_admin",
          key_value: keyAdmin,
        })
      )
        .then((res) => {
          SetLoading(false);
          Swal.fire({
            icon: "success",
            title: "Success",
            text: "Successfully created App Credential.",
            confirmButtonColor: "#2e9afe",
          });
          setEditable(false);
          reset();
          setValue("app_id", detail.id);
        })
        .catch((err) => {
          SetLoading(false);
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Something went wrong!",
            confirmButtonColor: "#2e9afe",
          });
        });

      // firebase client

      dispatch(
        CreateAppCredential({
          app_id: data.app_id,
          type: "firebase",
          key_value: keyClient,
        })
      )
        .then((res) => {
          Swal.fire({
            icon: "success",
            title: "Success",
            text: "Firebase setup has been saved sucesfully, please rebuild your app to update and use the Push Notification feature",
            confirmButtonColor: "#2e9afe",
          });
          setEditable(false);
          reset();
          setValue("app_id", detail.id);
        })
        .catch((err) => {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Something went wrong!",
            confirmButtonColor: "#2e9afe",
          });
        });

      // firebase client

      dispatch(
        CreateAppCredential({
          app_id: data.app_id,
          type: "firebase_ios",
          key_value: keyIOS,
        })
      )
        .then((res) => {
          Swal.fire({
            icon: "success",
            title: "Success",
            text: "Firebase setup has been saved sucesfully, please rebuild your app to update and use the Push Notification feature",
            confirmButtonColor: "#2e9afe",
          });
          setEditable(false);
          reset();
          setValue("app_id", detail.id);
        })
        .catch((err) => {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Something went wrong!",
            confirmButtonColor: "#2e9afe",
          });
        });
    }, 1000);
  };

  useEffect(() => {
    setValue("app_id", detail.id);
  }, [detail]);

  const handleChangeAdmin = (e) => {
    if (e.target.files.length > 0) {
      setKeyAdmin_ORI(e.target.files);
      const fileReader = new FileReader();
      fileReader.readAsText(e.target.files[0], "UTF-8");
      fileReader.onload = (e) => {
        var patern = JSON.parse(e.target.result);
        var flag = true;
        var prefix = [
          "type",
          "project_id",
          "private_key_id",
          "private_key",
          "client_email",
          "client_id",
          "auth_uri",
          "token_uri",
          "auth_provider_x509_cert_url",
          "client_x509_cert_url",
        ];
        prefix.map((val) => {
          if (patern[val] === undefined) {
            flag = false;
          }
        });
        if (flag) {
          setKeyAdmin(JSON.parse(e.target.result));
          setCount(Math.random());
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Incorrect JSON format, please check and use Firebase's valid JSON format.",
            confirmButtonColor: "#2e9afe",
          });
        }
      };
    }
  };

  const handleChangeClient = (e) => {
    if (e.target.files.length > 0) {
      setKeyClient_ORI(e.target.files);
      const fileReader = new FileReader();
      fileReader.readAsText(e.target.files[0], "UTF-8");
      fileReader.onload = (e) => {
        var patern = JSON.parse(e.target.result);
        var flag = true;
        var package_name_same = true;
        var prefix = ["project_info", "client", "configuration_version"];
        var prefix1 = ["project_number", "project_id", "storage_bucket"];
        var prefix2 = ["client_info", "oauth_client", "api_key", "services"];
        prefix.map((val, index) => {
          if (patern[val] === undefined) {
            flag = false;
          }
          if (flag) {
            if (index === 0) {
              prefix1.map((val2) => {
                if (patern[val][val2] === undefined) {
                  flag = false;
                }
              });
            } else if (index === 1) {
              if (
                patern["client"][0]["client_info"]["android_client_info"][
                "package_name"
                ] !== detail.app_package
              ) {
                package_name_same = false;
              }
              prefix2.map((val2) => {
                if (patern[val][0][val2] === undefined) {
                  flag = false;
                }
              });
            }
          }
        });
        if (!package_name_same) {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Package name does not match, please check your setup.",
            confirmButtonColor: "#2e9afe",
          });
        } else if (!flag) {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Incorrect JSON format, please check and use Firebase's valid JSON format.",
            confirmButtonColor: "#2e9afe",
          });
        } else {
          setKeyClient(JSON.parse(e.target.result));
          setCount(Math.random());
        }
      };
    }
  };

  const handleChangeIOS = (e) => {
    if (e.target.files.length > 0) {
      setKeyIOS_ORI(e.target.files);
      const fileReader = new FileReader();
      fileReader.readAsText(e.target.files[0], "UTF-8");
      fileReader.onload = (e) => {
        const text = e.target.result;
        var flag = true;
        var prefix = [
          "CLIENT_ID",
          "REVERSED_CLIENT_ID",
          "API_KEY",
          "GCM_SENDER_ID",
          "PLIST_VERSION",
          "BUNDLE_ID",
          "PROJECT_ID",
          "STORAGE_BUCKET",
          "IS_ADS_ENABLED",
          "IS_ANALYTICS_ENABLED",
          "IS_APPINVITE_ENABLED",
          "IS_GCM_ENABLED",
          "IS_SIGNIN_ENABLED",
          "GOOGLE_APP_ID",
        ];
        prefix.map((val) => {
          if (text.search(val) < 0) {
            flag = false;
          }
        });
        if (flag) {
          var convert = require("xml-js");
          var xmlToJson = convert.xml2json(text, { compact: false, spaces: 4 });
          setKeyIOS(JSON.parse(xmlToJson));
          setCount(Math.random());
        } else {
          Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Incorrect .plist format, please check and use Firebase's valid .plist format.",
            confirmButtonColor: "#2e9afe",
          });
        }
      };
    }
  };

  const customStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      width: "74%",
    },
    overlay: {
      backgroundColor: "rgba(0,0,0,0.5)",
    },
    customStyles,
  };

  function renderComponent(type) {
    if (detail.plan_id < 2 || detail.app_builds.length === 0) {
      return (
        <div className="how-it-works-area margin-auto">
          <div className="how-it-works-content">
            <div className="col-lg-12 col-md-12 p-0">
              <div className="margin-auto-add">
                <div className="radius-6 mb-5">
                  <div className="row">
                    <div className="col-md-12 border-right p-5 pt-1 pb-1">
                      <center
                        style={{
                          paddingTop: 90,
                        }}
                      >
                        <img
                          src="/images/content.png"
                          className="img-info-upgrade"
                          alt="logo"
                        />
                        <br />
                        <br />
                        <br />
                        <text className="txt-info-upgrade">
                          Your current plan can not use push message
                          notification,
                        </text>
                        <br />
                        <text className="txt-info-upgrade">
                          please change plan to use the feature
                        </text>
                        <br />
                        <br />
                        <br />
                        <button
                          onClick={() => {
                            setIsOpen(true);
                          }}
                          className="btn btn-primary profile-button"
                        >
                          Change Plan
                        </button>
                        <br />
                        <br />
                        <br />
                        <br />
                      </center>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      var convert = require("xml-js");
      var json2xml = convert.json2xml(keyIOS === "" ? {} : keyIOS, {
        compact: false,
        spaces: 4,
      });
      return (
        <div className="how-it-works-area margin-auto">
          <div className="how-it-works-content">
            <div className="col-lg-12 col-md-12 p-0">
              <div className="margin-auto-add">
                <div className="radius-6 mb-5">
                  <div className="row">
                    <div className="col-md-12 border-right p-5 pt-1 pb-1">
                      <form onSubmit={handleSubmit(onSubmit)}>
                        <div>
                          <br />
                          <text className="mt-10 ios-title">
                            Firebase Push Notification{" "}
                          </text>
                          <br />
                          <text className="mt-10 ios-subtitle">
                            Before use push notification feature, please
                            complete this setup and rebuild your app{" "}
                          </text>
                          <div className="ml-30">
                            <div className="form-group mtb-50 ml-10">
                              <text className="mt-10 ios-subtitle-bold">
                                Firebase Cloud Messaging for Android
                              </text>
                              <br />
                              <a
                                className="mtb-10"
                                href="https://docs.appsinstant.com/knowledgebase/android-firebase-cloud-messaging"
                                style={{ color: "#2e9afe" }}
                                target="_blank"
                              >
                                Guideline to setup your Firebase Cloud Messaging
                                for Android
                              </a>
                              <br />
                              <br />
                              <div className="ml-30 mt-10">
                                <text className="ios-subtitle-bold">
                                  Firebase Client for Android{" "}
                                </text>
                                <div className="form-group">
                                  <label
                                    className="mtb-10"
                                    for="exampleInputEmail1"
                                  >
                                    Connect Firebase and your app. Upload your
                                    Firebase Client JSON file here:
                                  </label>
                                  {editable && (
                                    <div>
                                      <label
                                        for="file-upload-1"
                                        style={{ display: "block" }}
                                      >
                                        <FileUpload data={keyClient_ORI} />
                                      </label>
                                      <input
                                        type="file"
                                        id="file-upload-1"
                                        style={{ height: 40 }}
                                        accept=".json"
                                        onChange={handleChangeClient}
                                        className="form-control"
                                        placeholder="Enter Phone Number"
                                      />
                                      {errors.sex && (
                                        <span className="text-validator">
                                          {errors?.sex?.message}
                                        </span>
                                      )}
                                    </div>
                                  )}
                                  <textarea
                                    style={{
                                      height: 170,
                                    }}
                                    value={
                                      keyClient === ""
                                        ? "No file uploaded yet"
                                        : JSON.stringify(keyClient)
                                    }
                                    disabled
                                    className="form-control mt-20"
                                  ></textarea>
                                </div>
                              </div>
                            </div>
                            <div className="form-group mtb-50 ml-10">
                              <div className="ml-30">
                                <div className="form-group mt-20">
                                  <text className="mt-10 ios-subtitle-bold">
                                    Firebase Admin{" "}
                                  </text>
                                  <br />
                                  <label
                                    className="mtb-10"
                                    for="exampleInputEmail1"
                                  >
                                    Connect Firebase and your app. Upload your
                                    Firebase Admin JSON file here:
                                  </label>
                                  {editable && (
                                    <div>
                                      <label
                                        for="file-upload"
                                        style={{ display: "block" }}
                                      >
                                        <FileUpload data={keyAdmin_ORI} />
                                      </label>
                                      <input
                                        type="file"
                                        id="file-upload"
                                        style={{ height: 40 }}
                                        accept=".json"
                                        onChange={handleChangeAdmin}
                                        className="form-control"
                                        placeholder="Enter Phone Number"
                                      />
                                      {errors.sex && (
                                        <span className="text-validator">
                                          {errors?.sex?.message}
                                        </span>
                                      )}
                                    </div>
                                  )}
                                  <textarea
                                    style={{
                                      height: 170,
                                    }}
                                    value={
                                      keyAdmin === ""
                                        ? "No file uploaded yet"
                                        : JSON.stringify(keyAdmin)
                                    }
                                    disabled
                                    className="form-control mt-20"
                                  ></textarea>
                                </div>
                              </div>
                            </div>
                            <div className="form-group mtb-50 ml-10">
                              <text className="mt-10 ios-subtitle-bold">
                                Firebase Cloud Messaging for iOs
                              </text>
                              <br />
                              <a
                                className="mtb-10"
                                href="https://docs.appsinstant.com/knowledgebase/ios-firebase-cloud-messaging"
                                style={{ color: "#2e9afe" }}
                                target="_blank"
                              >
                                Guideline to setup your Firebase Cloud Messaging
                                for iOs
                              </a>
                              <div className="ml-30">
                                <div className="form-group mt-20">
                                  <text className="mt-10 ios-subtitle-bold">
                                    Firebase Client for IOs{" "}
                                  </text>
                                  <br />
                                  <label
                                    className="mtb-10"
                                    for="exampleInputEmail1"
                                  >
                                    Connect Firebase and your app. Upload your
                                    Firebase .plist file here:
                                  </label>
                                  {editable && (
                                    <div>
                                      <label
                                        for="file-upload-2"
                                        style={{ display: "block" }}
                                      >
                                        <FileUpload data={keyIOS_ORI} />
                                      </label>
                                      <input
                                        id="file-upload-2"
                                        type="file"
                                        style={{ height: 40 }}
                                        accept=".plist"
                                        onChange={handleChangeIOS}
                                        className="form-control"
                                        placeholder="Enter Phone Number"
                                      />
                                      {errors.sex && (
                                        <span className="text-validator">
                                          {errors?.sex?.message}
                                        </span>
                                      )}
                                    </div>
                                  )}
                                  <textarea
                                    style={{
                                      height: 170,
                                    }}
                                    value={
                                      keyIOS === ""
                                        ? "No file uploaded yet"
                                        : json2xml
                                    }
                                    disabled
                                    className="form-control mt-20"
                                  ></textarea>
                                </div>
                              </div>
                            </div>
                            <div className="form-group mtb-10 ml-10">
                              <div className="ml-30">
                                <div className="form-group">
                                  <div
                                    className="mt-5"
                                    style={{
                                      display: "flex",
                                      justifyContent: "flex-end",
                                      marginBottom: 50,
                                    }}
                                  >
                                    {editable ? (
                                      <div>
                                        <div
                                          style={{
                                            marginRight: 20,
                                          }}
                                          onClick={() => {
                                            setEditable(false);
                                          }}
                                          className="btn btn-primary profile-button-disable"
                                        >
                                          Cancel
                                        </div>
                                        <button
                                          className="btn btn-primary profile-button"
                                          type="submit"
                                        >
                                          Save
                                        </button>
                                      </div>
                                    ) : (
                                      <div
                                        onClick={() => {
                                          setEditable(true);
                                        }}
                                        className="btn btn-primary profile-button"
                                      >
                                        Edit
                                      </div>
                                    )}
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }

  return (
    <>
      {loading && <Loader />}
      <div className="bg-white">
        {renderComponent(count)}
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={() => {
            setIsOpen(false);
          }}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <ChangePlan
            SetLoading={(val) => {
              SetLoading(val);
            }}
            onRequestClose={() => {
              setIsOpen(false);
            }}
          />
        </Modal>
      </div>
    </>
  );
};

export default AppList;

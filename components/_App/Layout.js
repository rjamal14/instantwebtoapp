import React from 'react'
import Head from "next/head"
import GoTop from './GoTop'
import Preloader from './Preloader'

const Layout = ({ children }) => {

    // Preloader
    const [loader, setLoader] = React.useState(true);
    React.useEffect(() => {
        setTimeout(() => setLoader(false), 1500);
    }, [])

    return (
        <>
            <Head>
                {/* Required meta tags */}
                <meta charSet="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <meta name="keywords" content="provide the following services: app builder, web to app convertor, web to android conversion, website to android conversion, app creator, app converter, android builder, website to app builder, app for website, website to iPhone, app creation, app maker, Shopify app store maker, Shopify to app maker, web to ios builder, apps builder, website to apk convertor, website to ios convertor, apk builder, web to iPhone conversion, ios builder, website to mobile app convertor, website into app convertoer, web to mobile app conversion, wix to app convertor, shopify store to app convertor, apps for shopify store, web & mobile builder" />

                <title>appsinstant - web to app convertor</title>
            </Head>

            {children}

            {loader ? <Preloader /> : null}

            <GoTop scrollStepInPx="100" delayInMs="10.50" />
        </>
    );
}

export default Layout;
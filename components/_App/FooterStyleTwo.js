// Footer Component Style File Path: public/css/pages-and-components-css/footer.scss

import React from 'react';
import Link from 'next/link';

const FooterStyleTwo = () => {

    const currentYear = new Date().getFullYear();

    return (
        <>
            <div className="footer-area footer-style-two">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4 col-md-12 col-sm-12">
                            <div className="single-footer-widget">
                                <Link href="/">
                                    <a className="logo">
                                        <img src="/images/appsinstant-web-to-android-creator.png" alt="logo" width="230px" />
                                    </a>
                                </Link>
                                <br />
                                <p>Instant Android and iOs apps builder,</p>
                                <p>the best solution for your growing business</p>
                            </div>
                        </div>

                        <div className="col-lg-3 col-md-12 col-sm-12">
                            <text>Accepted payment method</text>
                            <br />
                            <br />
                            <img src='/images/payment-for-website-to-app-creation.png' alt='payment' />
                        </div>

                        <div className="col-lg-5 col-md-12 col-sm-12">
                            <div className='row'>
                                <div className='col footer-rigth'>
                                    <img className="img-footer-right" src='/images/payment-for-website-to-apk.png' alt='payment' />
                                    <img className="img-footer-right-1" src='/images/payment-for-app-maker.png' alt='payment' />
                                    <img className="img-footer-right" src='/images/payment-for-apps-builder.png' alt='payment' />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="copyright-area">
                        <p className="copyright">Copyrights &copy; {currentYear} <strong>appsinstant.com</strong>, an instant mobile app builder. All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </>
    );
}

export default FooterStyleTwo;
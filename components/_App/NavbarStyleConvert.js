// Navbar Component Style File Path: public/css/pages-and-components-css/navbar.scss

import React from "react";
import Link from "@/utils/ActiveLink";
import { useSelector } from "react-redux";
import env from "../../config/env";
const NavbarStyleOne = ({ active }) => {
    const [menu, setMenu] = React.useState(true);
    const profile = useSelector((state) => state.profile.profile);
    const toggleNavbar = () => {
        setMenu(!menu);
    };

    const classOne = menu
        ? "collapse navbar-collapse"
        : "collapse navbar-collapse show";
    const classTwo = menu
        ? "navbar-toggler navbar-toggler-right collapsed"
        : "navbar-toggler navbar-toggler-right";

    return (
        <>
            <div id="navbar" className="navbar-area is-sticky navbar-head">
                <div className="texap-nav">
                    <div className="">
                        <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{background:'#f6f6f6', padding:16,paddingRight:'6%',paddingLeft:'6%'}}>
                            <Link href="/">
                                <a className="navbar-brand">
                                    <img
                                        src="/images/appsinstant-web-to-android-creator.png"
                                        className="logo-apps"
                                        alt="logo"
                                    />
                                </a>
                            </Link>

                            <button
                                onClick={toggleNavbar}
                                className={classTwo}
                                type="button"
                                data-toggle="collapse"
                                data-target="#navbarSupportedContent"
                                aria-controls="navbarSupportedContent"
                                aria-expanded="false"
                                aria-label="Toggle navigation"
                            >
                                <span className="icon-bar top-bar"></span>
                                <span className="icon-bar middle-bar"></span>
                                <span className="icon-bar bottom-bar"></span>
                            </button>

                            <div
                                className={classOne}
                                id="navbarSupportedContent"
                            >
                                <ul className="navbar-nav show-only-mobile">
                                    <li className="nav-item">
                                        <Link href="/convert">
                                            <a
                                                onClick={toggleNavbar}
                                                className={"nav-link " + (active === 'App Builder' ? "actv" : "")}
                                            >
                                                <i
                                                    class="ri-apps-line"
                                                    style={{
                                                        color:
                                                            active ===
                                                            "App Builder"
                                                                ? "white"
                                                                : "rgb(0 0 0 / 90%)",
                                                    }}
                                                ></i>{" "}
                                                &nbsp;
                                                <text
                                                    style={{
                                                        color:
                                                            active ===
                                                            "App Builder"
                                                                ? "white"
                                                                : "rgb(0 0 0 / 90%)",
                                                    }}
                                                >
                                                    App Builder
                                                </text>
                                            </a>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link href="/app-list">
                                            <a
                                                onClick={toggleNavbar}
                                                className={"nav-link " + (active === 'App List' ? "actv" : "")}
                                            >
                                                <i
                                                    class="ri-stack-fill"
                                                    style={{
                                                        color:
                                                            active ===
                                                            "App List"
                                                                ? "white"
                                                                : "rgb(0 0 0 / 90%)",
                                                    }}
                                                ></i>{" "}
                                                &nbsp;
                                                <text
                                                    style={{
                                                        color:
                                                            active ===
                                                            "App List"
                                                                ? "white"
                                                                : "rgb(0 0 0 / 90%)",
                                                    }}
                                                >
                                                    App List
                                                </text>
                                            </a>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link href="/transaction">
                                            <a
                                                onClick={toggleNavbar}
                                                className={"nav-link " + (active === 'Transaction' ? "actv" : "")}
                                            >
                                                <i
                                                    class="ri-money-dollar-box-line"
                                                    style={{
                                                        color:
                                                            active === "Transaction"
                                                                ? "white"
                                                                : "rgb(0 0 0 / 90%)",
                                                    }}
                                                ></i>{" "}
                                                &nbsp;
                                                <text
                                                    style={{
                                                        color:
                                                            active === "Transaction"
                                                                ? "white"
                                                                : "rgb(0 0 0 / 90%)",
                                                    }}
                                                >
                                                    Transaction
                                                </text>
                                            </a>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link href="/notification">
                                            <a
                                                onClick={toggleNavbar}
                                                className={"nav-link " + (active === 'Push Notification' ? "actv" : "")}
                                            >
                                                <i
                                                    class="ri-notification-line"
                                                    style={{
                                                        color:
                                                            active ===
                                                            "Push Notification"
                                                                ? "white"
                                                                : "rgb(0 0 0 / 90%)",
                                                    }}
                                                ></i>{" "}
                                                &nbsp;
                                                <text
                                                    style={{
                                                        color:
                                                            active ===
                                                            "Push Notification"
                                                                ? "white"
                                                                : "rgb(0 0 0 / 90%)",
                                                    }}
                                                >
                                                    Push Notification
                                                </text>
                                            </a>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link href="/profile">
                                            <a
                                                onClick={toggleNavbar}
                                                className={"nav-link " + (active === 'Profile' ? "actv" : "")}
                                            >
                                                <i
                                                    class="ri-profile-line"
                                                    style={{
                                                        color:
                                                            active === "Profile"
                                                                ? "white"
                                                                : "rgb(0 0 0 / 90%)",
                                                    }}
                                                ></i>{" "}
                                                &nbsp;
                                                <text
                                                    style={{
                                                        color:
                                                            active === "Profile"
                                                                ? "white"
                                                                : "rgb(0 0 0 / 90%)",
                                                    }}
                                                >
                                                    Profile
                                                </text>
                                            </a>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link href="/">
                                            <a
                                                onClick={() => {
                                                    localStorage.clear();
                                                    router.replace("/");
                                                }}
                                                className={"nav-link " + (active === 'Logout' ? "actv" : "")}
                                            >
                                                <i
                                                    class="ri-logout-box-line"
                                                    style={{
                                                        color:
                                                            active === "Logout"
                                                                ? "white"
                                                                : "rgb(0 0 0 / 90%)",
                                                    }}
                                                ></i>{" "}
                                                &nbsp;
                                                <text
                                                    style={{
                                                        color:
                                                            active === "Logout"
                                                                ? "white"
                                                                : "rgb(0 0 0 / 90%)",
                                                    }}
                                                >
                                                    Logout
                                                </text>
                                            </a>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                            <div
                                className="others-options hidden-only-mobile"
                                style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    alignItems: "center",
                                }}
                            >
                                {profile?.email &&
                                localStorage.getItem("token") ? (
                                    profile?.photo !== null ? (
                                        <img
                                            className="rounded-circle"
                                            style={{
                                                width: 50,
                                                flexDirection: "column",
                                                display: "flex",
                                                justifyContent: "center",
                                                marginRight: 10,
                                                alignContent: "center",
                                                textAlign: "center",
                                                height: 50,
                                            }}
                                            src={env.baseUrl + profile?.photo}
                                        />
                                    ) : (
                                        <div
                                            className="download-app"
                                            style={{
                                                background: "#2e9afe",
                                                width: 50,
                                                borderRadius: 30,
                                                color: "white",
                                                flexDirection: "column",
                                                display: "flex",
                                                justifyContent: "center",
                                                marginRight: 10,
                                                alignContent: "center",
                                                textAlign: "center",
                                                height: 50,
                                            }}
                                        >
                                            <text>
                                                {profile?.email
                                                    .substring(0, 1)
                                                    .toUpperCase()}
                                            </text>
                                        </div>
                                    )
                                ) : null}
                                {profile?.email &&
                                    localStorage.getItem("token") && (
                                        <text className="download-text">
                                            {profile?.email}
                                        </text>
                                    )}
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </>
    );
};

export default NavbarStyleOne;

import React from "react";
import Link from '@/utils/ActiveLink';

const NavbarStyleOne = () => {
    const [menu, setMenu] = React.useState(true)
    const [menuSelected, setMenuSelected] = React.useState('')

    const toggleNavbar = () => {
        setMenu(!menu)
    }

    React.useEffect(() => {
        if (window.location.hash === '') {
            setMenuSelected('Home')
        } else {
            setMenuSelected(window.location.hash.replace('#', ""))
        }
    }, [])

    React.useEffect(() => {

        let elementId = document.getElementById("navbar");
        document.addEventListener("scroll", () => {
            if (window.scrollY > 170) {
                elementId.classList.add("is-sticky");
            } else {
                elementId.classList.remove("is-sticky");
            }
        });

        let elementId2 = document.getElementById("nav-link-color");
        document.addEventListener("scroll", () => {
            if (window.scrollY > 170) {
                elementId2.classList.add("color-dark");
                elementId2.classList.remove("color-light");
            } else {
                elementId2.classList.add("color-light");
                elementId2.classList.remove("color-dark");
            }
        });

        let elementId3 = document.getElementById("nav-link-color-2");
        document.addEventListener("scroll", () => {
            if (window.scrollY > 170) {
                elementId3.classList.add("color-dark");
                elementId3.classList.remove("color-light");
            } else {
                elementId3.classList.add("color-light");
                elementId3.classList.remove("color-dark");
            }
        }); 

        let elementId4 = document.getElementById("logo-img");
        document.addEventListener("scroll", () => {
            if (window.scrollY > 170) {
                elementId4.src = '/images/logo-appsinstant-app-creator.png';
            } else {
                elementId4.src = '/images/appsinstant-app-builder.png';
            }
        });

        let elementId5 = document.getElementById("spn1");
        document.addEventListener("scroll", () => {
            if (window.scrollY > 170) {
                elementId5.classList.add("dark");
            } else {
                elementId5.classList.remove("dark");
            }
        });

        let elementId6 = document.getElementById("spn2");
        document.addEventListener("scroll", () => {
            if (window.scrollY > 170) {
                elementId6.classList.add("dark");
            } else {
                elementId6.classList.remove("dark");
            }
        });

        let elementId7 = document.getElementById("spn3");
        document.addEventListener("scroll", () => {
            if (window.scrollY > 170) {
                elementId7.classList.add("dark");
            } else {
                elementId7.classList.remove("dark");
            }
        });
    })

    const classOne = menu ? 'collapse navbar-collapse' : 'collapse navbar-collapse show';
    const classTwo = menu ? 'navbar-toggler navbar-toggler-right collapsed' : 'navbar-toggler navbar-toggler-right';

    return (
        <>
            <div id="navbar" className="navbar-area navbar-style-four">
                <div className="texap-nav">
                    <div className="container">
                        <nav className="navbar navbar-expand-md navbar-light bg-light">
                            <Link href="/">
                                <a className="navbar-brand" >
                                    <img src="/images/appsinstant-app-builder.png" id="logo-img" className="img-logo-navbar" alt="logo" />
                                </a>
                            </Link>
                            <button
                                onClick={toggleNavbar}
                                className={classTwo}
                                type="button"
                                data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false"
                                aria-label="Toggle navigation"
                            >
                                <span className="icon-bar top-bar" id="spn1"></span>
                                <span className="icon-bar middle-bar" id="spn2"></span>
                                <span className="icon-bar bottom-bar" id="spn3"></span>
                            </button>

                            <div className={classOne} id="navbarSupportedContent">
                                <ul className="navbar-nav color-light" id="nav-link-color">
                                    <li className="nav-item">
                                        <Link href="#Home">
                                            <a onClick={() => {
                                                setMenuSelected('Home')
                                                setMenu(!menu)
                                            }} className={(menuSelected === 'Home' ? "nav-link active" : "nav-link")}>Home</a>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link href="#Benefit">
                                            <a onClick={() => {
                                                setMenuSelected('Benefit')
                                                setMenu(!menu)
                                            }} className={(menuSelected === 'Benefit' ? "nav-link active" : "nav-link")}>Benefit</a>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link href="#Features">
                                            <a onClick={() => {
                                                setMenuSelected('Features')
                                                setMenu(!menu)
                                            }} className={(menuSelected === 'Features' ? "nav-link active" : "nav-link")}>Features</a>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link href="#Pricing">
                                            <a onClick={() => {
                                                setMenuSelected('Pricing')
                                                setMenu(!menu)
                                            }} className={(menuSelected === 'Pricing' ? "nav-link active" : "nav-link")}>Pricing</a>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link href="#FAQ">
                                            <a onClick={() => {
                                                setMenuSelected('FAQ')
                                                setMenu(!menu)
                                            }} className={(menuSelected === 'FAQ' ? "nav-link active" : "nav-link")}>FAQ</a>
                                        </Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link href="https://docs.appsinstant.com">
                                            <a onClick={() => {
                                                setMenuSelected('Docs')
                                                setMenu(!menu)
                                            }} className={(menuSelected === 'Docs' ? "nav-link active" : "nav-link")}>Docs</a>
                                        </Link>
                                    </li>
                                    <li className="nav-item show-only-mobile">
                                        <Link href="/app-list">
                                            <a className={"nav-link"}><i class="fa fa-user-circle"></i>&nbsp;&nbsp; My Account</a>
                                        </Link>
                                    </li>
                                </ul>
                            </div>

                            <div className="others-options">
                                <ul className="navbar-nav color-light" id="nav-link-color-2">
                                    <li className="nav-item">
                                        <Link href="/app-list">
                                            <a className={"nav-link"}><i class="fa fa-user-circle"></i>&nbsp;&nbsp; My Account</a>
                                        </Link>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </>
    );
}

export default NavbarStyleOne;
import React from 'react'
import { useRouter } from 'next/router'
var localStorage = require('localStorage')

const Menu = ({ active }) => {
    const router = useRouter()

    // Preloader
    const [loader, setLoader] = React.useState(true);
    React.useEffect(() => {
        setTimeout(() => setLoader(false), 1500);
    }, [])

    return(
        <>
            <div className="col-lg-3 col-md-12 p-0 border-nav">
                <div className="mt-100">
                    <nav className={"navbar navbar-light" + (active === 'App Builder' ? " actv" : "")}>
                        <a className="navbar-brand ml-navbar" href="/convert">
                            <i 
                                class="ri-apps-line"
                                style={{
                                    color: active === 'App Builder' ? 'white' : 'rgb(0 0 0 / 90%)',
                                }}
                            >
                            </i> &nbsp; 
                            <text style={{color: active === 'App Builder' ? 'white' : 'rgb(0 0 0 / 90%)',}}>
                                App Builder
                            </text>
                        </a>
                    </nav>
                    <nav className={"navbar navbar-light" + (active === 'App List' ? " actv" : "")}>
                        <a className="navbar-brand ml-navbar" href="/app-list">
                            <i 
                                class="ri-stack-fill"
                                style={{
                                    color: active === 'App List' ? 'white' : 'rgb(0 0 0 / 90%)',
                                }}
                            >
                            </i> &nbsp; 
                            <text style={{color: active === 'App List' ? 'white' : 'rgb(0 0 0 / 90%)',}}>
                                App List
                            </text>
                        </a>
                    </nav>
                    {
                        localStorage.getItem('token') && <div>
                            <nav className={"navbar navbar-light" + (active === 'Transaction' ? " actv" : "")}>
                                <a className="navbar-brand ml-navbar" href="/transaction">
                                    <i 
                                        class="ri-money-dollar-box-line"
                                        style={{
                                            color: active === 'Transaction' ? 'white' : 'rgb(0 0 0 / 90%)',
                                        }}
                                    >
                                    </i> &nbsp; 
                                    <text style={{color: active === 'Transaction' ? 'white' : 'rgb(0 0 0 / 90%)',}}>
                                        Transaction
                                    </text>
                                </a>
                            </nav>
                            <nav className={"navbar navbar-light" + (active === 'Push Notification' ? " actv" : "")}>
                                <a className="navbar-brand ml-navbar" href="/notification">
                                    <i 
                                        class="ri-notification-line"
                                        style={{
                                            color: active === 'Push Notification' ? 'white' : 'rgb(0 0 0 / 90%)',
                                        }}
                                    >
                                    </i> &nbsp; 
                                    <text style={{color: active === 'Push Notification' ? 'white' : 'rgb(0 0 0 / 90%)',}}>
                                        Push Notification
                                    </text>
                                </a>
                            </nav>
                            <nav className={"navbar navbar-light" + (active === 'Profile' ? " actv" : "")}>
                                <a className="navbar-brand ml-navbar" href="/profile">
                                    <i 
                                        class="ri-profile-line"
                                        style={{
                                            color: active === 'Profile' ? 'white' : 'rgb(0 0 0 / 90%)',
                                        }}
                                    >
                                    </i> &nbsp; 
                                    <text style={{color: active === 'Profile' ? 'white' : 'rgb(0 0 0 / 90%)',}}>
                                        Profile
                                    </text>
                                </a>
                            </nav>
                            <nav className={"navbar navbar-light" + (active === 'Logout' ? " actv" : "")}>
                                <a className="navbar-brand ml-navbar" href="#" onClick={()=>{
                                    localStorage.clear();
                                    router.replace('/')
                                }}>
                                    <i 
                                        class="ri-logout-box-line"
                                        style={{
                                            color: active === 'Logout' ? 'white' : 'rgb(0 0 0 / 90%)',
                                        }}
                                    >
                                    </i> &nbsp; 
                                    <text style={{color: active === 'Logout' ? 'white' : 'rgb(0 0 0 / 90%)',}}>
                                        Logout
                                    </text>
                                </a>
                            </nav>
                        </div>
                    }
                </div>
            </div>
        </>
    );
}

export default Menu;
FROM node:lts as dependencies
WORKDIR /instantwebtoapp
COPY package.json ./
RUN yarn install --frozen-lockfile

FROM node:lts as builder
ARG NODE_ENV
WORKDIR /instantwebtoapp
RUN touch /instantwebtoapp/.env
RUN echo "NODE_ENV=${NODE_ENV}" >> /instantwebtoapp/.env
COPY . .
COPY --from=dependencies /instantwebtoapp/node_modules ./node_modules
RUN yarn build

FROM node:lts as runner
WORKDIR /instantwebtoapp
COPY --from=builder /instantwebtoapp/.env ./.env
COPY --from=builder /instantwebtoapp/next.config.js ./next.config.js
COPY --from=builder /instantwebtoapp/jsconfig.json ./jsconfig.json
COPY --from=builder /instantwebtoapp/public ./public
COPY --from=builder /instantwebtoapp/components ./components
COPY --from=builder /instantwebtoapp/.next ./.next
COPY --from=builder /instantwebtoapp/node_modules ./node_modules
COPY --from=builder /instantwebtoapp/package.json ./package.json
EXPOSE 3000
CMD ["yarn", "start"]
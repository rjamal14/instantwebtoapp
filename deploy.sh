cd /home/deploy/instantwebtoapp

git checkout production
git pull origin production

docker-compose down
docker pull instantwebtoapp/instantwebtoapp:production
docker-compose up --build --force-recreate -d
sleep 10
docker system prune -a -f

echo 'Deploy instantwebtoapp finished.'

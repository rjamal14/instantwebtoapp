import { PlanContants } from "./constants";
import axios from "../helpers/axios_noAuth";

export const getPlan = () => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
        dispatch({ type: PlanContants.PLAN_REQUEST });
        const res = await axios.get(`/Plan`);
        if(res.data.status === 'success'){        
            const result = res.data.result
            dispatch({
                type: PlanContants.PLAN_SUCCESS,
                payload: { result }
            });
            resolve("success")
        }else{
            rejected('error')
            if(res.status === 400){
                dispatch({
                    type: PlanContants.PLAN_FAILURE,
                    payload: { error: res.data.error }
                });
            }
        }
    })
}
 
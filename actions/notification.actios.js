import { notificationConstants } from "./constants";
import axios from "../helpers/axios";

export const getNotification = (app_id,page,perPage) => async (dispatch) => {
  return new Promise(async (resolve, rejected) => {
      dispatch({ type: notificationConstants.FETCH_NOTIFICATION_REQUEST });
      try {
        const res = await axios.get(`Notif?filter[app_id]=${app_id}&page=${page}&per_page=${perPage}`)
        if(res.status === 200){
            const notification_list = res.data.result
            dispatch({
                type: notificationConstants.FETCH_NOTIFICATION_SUCCESS,
                payload: { notification_list , status_code : res.status }
            });
            resolve(notification_list)
        }else{
            rejected('error')
        }
      } catch (error) {
        dispatch({
            type: notificationConstants.FETCH_NOTIFICATION_FAILURE,
            payload: { error: 'An error occurred, please try again in a moment' , status_code :error.response.status }
        });
        rejected('error')
      }
  })
}

export const CreateNotification = (dataPayload) => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
      dispatch({ type: notificationConstants.FETCH_UPDATE_NOTIFICATION_REQUEST }) 
      try {
        const res = await axios.post(`/Notif/`, dataPayload)
        if (res.data.status === "success") {
          const notification = res.data.result
          dispatch({
            type: notificationConstants.FETCH_UPDATE_NOTIFICATION_SUCCESS,
            payload: { notification }
          })
          resolve("success")
        }else{
          const notification = res.data
          rejected(notification.message)
        }
      } catch (error) {
          console.log('error',error);
        dispatch({
          type: notificationConstants.FETCH_UPDATE_NOTIFICATION_FAILURE,
          payload: { error: "An error occurred, please try again in a moment" }
        })
        }
    })
}
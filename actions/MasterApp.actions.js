import { MasterAppContants } from "./constants";
import axios from "../helpers/axios_noAuth";

export const MasterAppCreate = (dataPayload) => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
        dispatch({ type: MasterAppContants.MASTER_APP_REQUEST });
        const res = await axios.post(`/MasterApp/create`, dataPayload);
        if(res.data.status === 'success'){        
            if(res.data.result.payment){
                resolve(res.data.result.payment.res.links[1].href)
            }else{
                rejected(res.data.message)
            }
            dispatch({
                type: MasterAppContants.MASTER_APP_SUCCESS,
            });
        }else{
            rejected(res.data.message)
            dispatch({
                type: MasterAppContants.MASTER_APP_FAILURE,
                payload: { error: res.data.error }
            });
        }

        if(res.status === 400){
            rejected(res.data.message)
            dispatch({
                type: MasterAppContants.MASTER_APP_FAILURE,
                payload: { error: res.data.error }
            });
        }
    })
}

export const MasterAppEdit = (payload,id) => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
        dispatch({ type: MasterAppContants.MASTER_APP_REQUEST });
        const res = await axios.patch(`/MasterApp/update/${id}`, payload);
        if(res.data.status === 'success'){        
            resolve("success")
            dispatch({
                type: MasterAppContants.MASTER_APP_SUCCESS,
            });
        }else{
            rejected(res.data.message)
            dispatch({
                type: MasterAppContants.MASTER_APP_FAILURE,
                payload: { error: res.data.error }
            });
        }
        if(res.status === 400){
            rejected(res.data.message)
            dispatch({
                type: MasterAppContants.MASTER_APP_FAILURE,
                payload: { error: res.data.error }
            });
        }
    })
}
 
export const MasterAppChangePlan = (payload,id) => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
        dispatch({ type: MasterAppContants.MASTER_APP_REQUEST });
        const res = await axios.patch(`/MasterApp/update/${id}`, payload);
        if(res.data.status === 'success'){        
            if(res.data.result.payment){
                resolve(res.data.result.payment.res.links[1].href)
            }
            dispatch({
                type: MasterAppContants.MASTER_APP_SUCCESS,
            });
        }else{
            rejected(res.data.message)
            dispatch({
                type: MasterAppContants.MASTER_APP_FAILURE,
                payload: { error: res.data.error }
            });
        }
        if(res.status === 400){
            rejected(res.data.message)
            dispatch({
                type: MasterAppContants.MASTER_APP_FAILURE,
                payload: { error: res.data.error }
            });
        }
    })
}
 
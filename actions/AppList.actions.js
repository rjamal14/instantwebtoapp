import { AppListConstants } from "./constants";
import axios from "../helpers/axios";

export const getAppList = (page,perPage) => {
  return async (dispatch) => {
    dispatch({ type: AppListConstants.FETCH_APP_LIST_REQUEST }) 

    try {
      const res = await axios.get(`/MasterApp?page=${page}&per_page=${perPage}`)
      if (res.data.status === "success") {
        const app_list = res.data.result
        dispatch({
          type: AppListConstants.FETCH_APP_LIST_SUCCESS,
          payload: { app_list }
        })
      }
    } catch (error) {
      dispatch({
        type: AppListConstants.FETCH_APP_LIST_FAILURE,
        payload: { error: "An error occurred, please try again in a moment", status_code :error.response.status }
      })
    }
  }
}

export const getDetailAppList = (id) => {
  return async (dispatch) => {
    dispatch({ type: AppListConstants.FETCH_APP_LIST_DETAIL_REQUEST }) 

    try {
      const res = await axios.get(`/MasterApp/${id}`)
      if (res.data.status === "success") {
        const app_list_detail = res.data.result
        dispatch({
          type: AppListConstants.FETCH_APP_LIST_DETAIL_SUCCESS,
          payload: { app_list_detail }
        })
      }
    } catch (error) {
      dispatch({
        type: AppListConstants.FETCH_APP_LIST_DETAIL_FAILURE,
        payload: { error: "An error occurred, please try again in a moment", status_code :error.response.status }
      })
    }
  }
}
import { profileConstants } from "./constants";
import axios from "../helpers/axios_noAuth";
import axios_useAuth from "../helpers/axios";

export const getProfile = (token) => async (dispatch) => {
  return new Promise(async (resolve, rejected) => {
      dispatch({ type: profileConstants.FETCH_PROFILE_REQUEST });
      try {
        const res = await axios.get(`oauth/auth?token=${token}`)
        if(res.status === 200){
            const profile = res.data.result.data.user.profile
            dispatch({
                type: profileConstants.FETCH_PROFILE_SUCCESS,
                payload: { profile , status_code : res.status }
            });
            resolve(profile)
        }else{
            rejected('error')
        }
      } catch (error) {
        dispatch({
            type: profileConstants.FETCH_PROFILE_FAILURE,
            payload: { error: 'An error occurred, please try again in a moment' , status_code :error.response.status }
        });
        rejected('error')
      }
  })
}

export const UpdateProfile = (dataPayload, id) => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
      dispatch({ type: profileConstants.FETCH_UPDATE_PROFILE_REQUEST }) 
  
      try {
        const res = await axios_useAuth.patch(`/profile/`+id, dataPayload)
        if (res.data.status === "success") {
          const profile = res.data.result
          dispatch({
            type: profileConstants.FETCH_UPDATE_PROFILE_SUCCESS,
            payload: { profile }
          })
          resolve("success")
        }
      } catch (error) {
          console.log('error',error);
        dispatch({
          type: profileConstants.FETCH_UPDATE_PROFILE_FAILURE,
          payload: { error: "An error occurred, please try again in a moment", status_code :error.response.status }
        })
        rejected('error')
        }
    })
}
import { PaymentContants } from "./constants";
import axios from "../helpers/axios_noAuth";

export const paymentSuccess = (params) => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
        dispatch({ type: PaymentContants.PAYMENT_REQUEST });
        const res = await axios.get(`/Payment/Success${params}`);
        if(res.data.status === 'success'){        
            dispatch({
                type: PaymentContants.PAYMENT_SUCCESS,
                payload: 'success'
            });
            resolve("success")
        }else{
            rejected('error')
            if(res.status === 400){
                dispatch({
                    type: PaymentContants.PAYMENT_FAILURE,
                    payload: { error: res.data.error }
                });
            }
        }
    })
}
 
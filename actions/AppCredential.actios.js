import { AppCredentialConstants } from "./constants";
import axios from "../helpers/axios";

export const getDetailAppCredential = (id, type) => async (dispatch) => {
  return new Promise(async (resolve, rejected) => {
    dispatch({ type: AppCredentialConstants.FETCH_APP_CREDENTIAL_REQUEST });
    try {
      const res = await axios.get(`/AppCredential/app/${id}/${type}`)
      if (res.status === 200) {
        if (res.data.result === null) {
          resolve('')
        } else {
          resolve(res.data.result.key_value)
        }
      } else {
        rejected('error')
      }
    } catch (error) {
      console.log('error', error);
      dispatch({
        type: AppCredentialConstants.FETCH_APP_CREDENTIAL_FAILURE,
        payload: { error: 'An error occurred, please try again in a moment' }
      });
      rejected('error')
    }
  })
}

export const CreateAppCredential = (dataPayload) => async (dispatch) => {
  return new Promise(async (resolve, rejected) => {
    dispatch({ type: AppCredentialConstants.FETCH_UPDATE_APP_CREDENTIAL_REQUEST })

    try {
      const res = await axios.post(`/AppCredential/create`, dataPayload)
      if (res.data.status === "success") {
        const AppCredential = res.data.result
        dispatch({
          type: AppCredentialConstants.FETCH_UPDATE_APP_CREDENTIAL_SUCCESS,
          payload: { AppCredential }
        })
        resolve("success")
      }
    } catch (error) {
      console.log('error', error);
      dispatch({
        type: AppCredentialConstants.FETCH_UPDATE_APP_CREDENTIAL_FAILURE,
        payload: { error: "An error occurred, please try again in a moment", status_code: error.response.status }
      })
      rejected('error')
    }
  })
}

export const getDetailIOSAppCredential = (id) => async (dispatch) => {
  return new Promise(async (resolve, rejected) => {
    dispatch({ type: AppCredentialConstants.FETCH_APP_CREDENTIAL_REQUEST });
    try {
      const res = await axios.get(`/IosConfig/app/${id}`)
      if (res.status === 200) {
        if (res.data.result === null) {
          resolve('')
        } else {
          resolve(res.data.result)
        }
      } else {
        rejected('error')
      }
    } catch (error) {
      console.log('error', error);
      dispatch({
        type: AppCredentialConstants.FETCH_APP_CREDENTIAL_FAILURE,
        payload: { error: 'An error occurred, please try again in a moment' }
      });
      rejected('error')
    }
  })
}

export const CreateIOSAppCredential = (dataPayload) => async (dispatch) => {
  return new Promise(async (resolve, rejected) => {
    dispatch({ type: AppCredentialConstants.FETCH_UPDATE_APP_CREDENTIAL_REQUEST })

    try {
      const res = await axios.post(`/IosConfig/upsert`, dataPayload)
      if (res.data.status === "success") {
        const AppCredential = res.data.result
        dispatch({
          type: AppCredentialConstants.FETCH_UPDATE_APP_CREDENTIAL_SUCCESS,
          payload: { AppCredential }
        })
        resolve(AppCredential)
      }else{
        rejected(res.data.message)
      }
    } catch (error) {
      console.log('error', error);
      dispatch({
        type: AppCredentialConstants.FETCH_UPDATE_APP_CREDENTIAL_FAILURE,
        payload: { error: "An error occurred, please try again in a moment", status_code: error.response.status }
      })
      rejected('error')
    }
  })
}
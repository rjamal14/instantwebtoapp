import { AppBuildContants } from "./constants";
import axios from "../helpers/axios_noAuth";

export const getAppBuildList = (id,page,perPage) => {
    return async (dispatch) => {
      dispatch({ type: AppBuildContants.APP_BUILD_REQUEST }) 
  
      try {
        const res = await axios.get(`/AppBuild/?app_id=${id}&page=${page}&per_page=${perPage}`)
        if (res.data.status === "success") {
          const app_build = res.data.result
          dispatch({
            type: AppBuildContants.APP_BUILD_SUCCESS,
            payload: { app_build }
          })
        }
      } catch (error) {
        dispatch({
          type: AppBuildContants.APP_BUILD_FAILURE,
          payload: { error: "An error occurred, please try again in a moment", status_code :error.response.status }
        })
      }
    }
  }


export const AppreBuild = (payload) => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
        dispatch({ type: AppBuildContants.APP_BUILD_REBUILD_REQUEST });
        const res = await axios.post(`/AppBuild/create`, payload);
        if(res.data.status === 'success'){        
          resolve(true)
          dispatch({
              type: AppBuildContants.APP_BUILD_REBUILD_SUCCESS,
          });
        }else{
            rejected(res.data)
            dispatch({
                type: AppBuildContants.APP_BUILD_REBUILD_FAILURE,
                payload: { error: res.data.error }
            });
        }
    })
}
import { authConstants } from "./constants";
import axios from "../helpers/axios_noAuth";
import env from "../config/env"
var localStorage = require('localStorage')

export const login = (payload) => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
        dispatch({ type: authConstants.LOGIN_REQUEST });
        const res = await axios.post(`/oauth/magic-link`, payload);
        if(res.status === 200){
            const status = res.data.status;
            dispatch({
                type: authConstants.LOGIN_SUCCESS,
                payload: {
                    status
                }
            });
            resolve('success')
        }else{
            dispatch({
                type: authConstants.LOGIN_FAILURE,
                payload: { error: res }
            });
            rejected('error')
        }
    })
}

export const loginClient = () => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
        dispatch({ type: authConstants.LOGIN_CLIENT_REQUEST });
        const payload = {
            client_id       : env.client_id,
            client_secret   : env.client_secret
        }
        const res = await axios.post(`/oauth/token/client`, payload);
        if(res.status === 200){
            const status = res.data.status;
            dispatch({
                type: authConstants.LOGIN_CLIENT_SUCCESS,
                payload: {
                    status
                }
            });
            resolve(res.data.result)
        }else{
            dispatch({
                type: authConstants.LOGIN_CLIENT_FAILURE,
                payload: { error: res }
            });
            rejected('error')
        }
    })
}

export const isUserLoggedIn = () => {
    return async dispatch => {
        const token = localStorage.getItem('token');
        if(token){
            const user = JSON.parse(localStorage.getItem('user'));
            dispatch({
                type: authConstants.LOGIN_SUCCESS,
                payload: {
                    token, user
                }
            });
        }else{
            dispatch({
                type: authConstants.LOGIN_FAILURE,
                payload: { error: 'Failed to login' }
            });
        }
    }
}

export const signout = () => {
    return async dispatch => {

        localStorage.clear();
        dispatch({ type: authConstants.LOGOUT_SUCCESS });

        // dispatch({ type: authConstants.LOGOUT_REQUEST });
        // const res = await axios.post(`/admin/signout`);

        // if(res.status === 200){
        //     localStorage.clear();
        //     dispatch({ type: authConstants.LOGOUT_SUCCESS });
        // }else{
        //     dispatch({
        //         type: authConstants.LOGOUT_FAILURE,
        //         payload: { error: res.data.error }
        //     });
        // }        
    }
}
import { TransactionConstants } from "./constants";
import axios from "../helpers/axios";

export const getTransactionList = (type,page,perPage) => {
  return async (dispatch) => {
    dispatch({ type: TransactionConstants.FETCH_TRANSACTION_REQUEST }) 

    try {
      const res = await axios.get(`/Transaction/list?page=${page}&per_page=${perPage}${type !== 'all' ? '&filter[payment][status]='+type : '' }`)
      if (res.data.status === "success") {
        const transaction_list = res.data.result
        dispatch({
          type: TransactionConstants.FETCH_TRANSACTION_SUCCESS,
          payload: { transaction_list }
        })
      }
    } catch (error) {
      dispatch({
        type: TransactionConstants.FETCH_TRANSACTION_FAILURE,
        payload: { error: "An error occurred, please try again in a moment", status_code :error.response.status }
      })
    }
  }
}


export const CancelTransaction = (id) => async (dispatch) => {
  return new Promise(async (resolve, rejected) => {
      dispatch({ type: TransactionConstants.TRANSACTION_REQUEST });
      const res = await axios.post(`/Transaction/cancel`, {'trx_id' : id });
      if(res.data.status === 'success'){
          resolve(res.data.result.transaction.invoices[0].payments[0].res.links[1].href)
          dispatch({
              type: TransactionConstants.TRANSACTION_SUCCESS,
          });
      }else{
          dispatch({
              type: TransactionConstants.TRANSACTION_FAILURE,
              payload: { error: res.data.error }
          });
      }
      if(res.status === 400){
          resolve(res.data.result)
          dispatch({
              type: TransactionConstants.TRANSACTION_FAILURE,
              payload: { error: res.data.error }
          });
      }
  })
}

export const NewLinkTransaction = (id) => async (dispatch) => {
  return new Promise(async (resolve, rejected) => {
      dispatch({ type: TransactionConstants.TRANSACTION_REQUEST });
      const res = await axios.post(`/Transaction/new_link`, {'trx_id' : id });
      if(res.data.status === 'success'){
          resolve(res.data.result.payment.res.links[1].href)
          dispatch({
              type: TransactionConstants.TRANSACTION_SUCCESS,
          });
      }else{
          dispatch({
              type: TransactionConstants.TRANSACTION_FAILURE,
              payload: { error: res.data.error }
          });
      }
      if(res.status === 400){
          resolve(res.data.result)
          dispatch({
              type: TransactionConstants.TRANSACTION_FAILURE,
              payload: { error: res.data.error }
          });
      }
  })
}
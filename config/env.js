const env = {};

env.baseUrl = process.env.NEXT_PUBLIC_ENV_BASE_URI;
env.Url = process.env.NEXT_PUBLIC_ENV_URI;
env.UrlSocket = process.env.NEXT_PUBLIC_ENV_URI_SOCKET;
env.client_id = process.env.NEXT_PUBLIC_ENV_CLIENT_ID;
env.client_secret = process.env.NEXT_PUBLIC_ENV_CLIENT_SECRET;

export default env;

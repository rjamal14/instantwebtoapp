import authReducer from './auth.reducers';
import { combineReducers } from 'redux';
import profileReducers from './profile.reducers';
import app_listReducers from './AppList.reducers';
import MasterAppReducers from './MasterApp.reducers';
import PlanReducers from './plan.reducers';
import TransactionReducers from './transaction.reducers';
import AppBuild from './AppBuild.reducers';
import AppCredential from './AppCredential.reducers';
import notification from './notification.reducers';

const rootReducer = combineReducers({
    auth: authReducer,
    profile: profileReducers,
    app_list: app_listReducers,
    MasterApp:MasterAppReducers,
    plan: PlanReducers,
    transaction: TransactionReducers,
    app_build: AppBuild,
    AppCredential: AppCredential,
    notification: notification
});

export default rootReducer;
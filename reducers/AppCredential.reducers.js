import { AppCredentialConstants } from "../actions/constants"

const initState = {
  error: null,
  message: '',
  success: null,
  loading: false,
  status_code:200,
  AppCredential: {
  },
}

export default (state = initState, action) => {
  switch (action.type) {
    case AppCredentialConstants.FETCH_APP_CREDENTIAL_REQUEST:
      state = {
        ...state,
        loading: true,
        AppCredential: {}
      }
      break;
    case AppCredentialConstants.FETCH_APP_CREDENTIAL_SUCCESS:
      state = {
        ...state,
        loading: false,
        success:true,
        AppCredential: action.payload.AppCredential,
        status_code: action.payload.status_code,
      }
      break;
    case AppCredentialConstants.FETCH_APP_CREDENTIAL_FAILURE:
      state = {
        ...state,
        loading: false,
        success: false,
        error: action.payload.error,
        status_code: action.payload.status_code,
      }
      break;

    case AppCredentialConstants.FETCH_UPDATE_APP_CREDENTIAL_REQUEST:
      state = {
        ...state,
        loading: true,
        AppCredential: {}
      }
      break;
    case AppCredentialConstants.FETCH_UPDATE_APP_CREDENTIAL_SUCCESS:
      state = {
        ...state,
        loading: false,
        success:true,
        AppCredential: action.payload.AppCredential,
        status_code: action.payload.status_code,
      }
      break;
    case AppCredentialConstants.FETCH_UPDATE_APP_CREDENTIAL_FAILURE:
      state = {
        ...state,
        loading: false,
        success: false,
        error: action.payload.error,
        status_code: action.payload.status_code,
      }
      break;
  }

  return state;
}
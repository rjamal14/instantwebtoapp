/* eslint-disable default-case */
import { PlanContants } from "../actions/constants";

const initState = {
    success:false,
    loading: false,
    error: null,
    result: {
        data: [],
        currentPage: 1,
        nextPage: false,
        totalItems: 4,
        totalPages: 1
    },
    message: ''
};

export default (state = initState, action) => {
    switch (action.type) {
        case PlanContants.PLAN_REQUEST:
            state = {
                ...state,
                loading: true
            }
            break;
        case PlanContants.PLAN_SUCCESS:
            state = {
                ...state,
                success: true,
                result: action.payload.result,
                loading: false
            }
            break;
        case PlanContants.PLAN_FAILURE:
            state = {
                ...state,
                error: action.payload,
                success: false,
                loading: false
            }
            break;
    }


    return state;
}
import { notificationConstants } from "../actions/constants"

const initState = {
  error: null,
  message: '',
  success: null,
  loading: false,
  status_code:200,
  notification_list: {
    data: [],
    currentPage: 1,
    nextPage: 1,
    totalItems: 0,
    totalPages: 1
  },
  notification: {
  }
}

export default (state = initState, action) => {
  switch (action.type) {
    case notificationConstants.FETCH_NOTIFICATION_REQUEST:
      state = {
        ...state,
        loading: true,
        notification_list: {
          data: [],
          currentPage: 1,
          nextPage: 1,
          totalItems: 0,
          totalPages: 1
        },
      }
      break;
    case notificationConstants.FETCH_NOTIFICATION_SUCCESS:
      state = {
        ...state,
        loading: false,
        success:true,
        notification_list: action.payload.notification_list,
        status_code: action.payload.status_code,
      }
      break;
    case notificationConstants.FETCH_NOTIFICATION_FAILURE:
      state = {
        ...state,
        loading: false,
        success: false,
        error: action.payload.error,
        notification_list: {
          data: [],
          currentPage: 1,
          nextPage: 1,
          totalItems: 0,
          totalPages: 1
        },
        status_code: action.payload.status_code,
      }
      break;

    case notificationConstants.FETCH_UPDATE_NOTIFICATION_REQUEST:
      state = {
        ...state,
        loading: true,
        notification: {
        },
      }
      break;
    case notificationConstants.FETCH_UPDATE_NOTIFICATION_SUCCESS:
      state = {
        ...state,
        loading: false,
        success:true,
        notification: action.payload.notification,
        status_code: action.payload.status_code,
      }
      break;
    case notificationConstants.FETCH_UPDATE_NOTIFICATION_FAILURE:
      state = {
        ...state,
        loading: false,
        success: false,
        error: action.payload.error,
        status_code: action.payload.status_code,
      }
      break;
  }

  return state;
}
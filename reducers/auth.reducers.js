/* eslint-disable default-case */
import { authConstants } from "../actions/constants";

const initState = {
    status: null,
    authenticate: false,
    authenticating: false,
    loading: false,
    error: null,
    message: ''
};

export default (state = initState, action) => {
    switch (action.type) {
        case authConstants.LOGIN_REQUEST:
            state = {
                ...state,
                authenticating: true
            }
            break;
        case authConstants.LOGIN_SUCCESS:
            state = {
                ...state,
                status: action.payload.status,
                authenticate: true,
                authenticating: false
            }
            break;
        case authConstants.LOGIN_FAILURE:
            state = {
                ...state,
                error: action.payload,
                authenticate: false,
                authenticating: false
            }
            break;
        case authConstants.LOGOUT_REQUEST:
            state = {
                ...state,
                loading: true
            }
            break;
        case authConstants.LOGOUT_SUCCESS:
            state = {
                ...initState
            }
            break;
        case authConstants.LOGOUT_FAILURE:
            state = {
                ...state,
                error: action.payload.error,
                loading: false
            }
            break;

        case authConstants.LOGIN_CLIENT_REQUEST:
            state = {
                ...state,
                authenticating: true
            }
            break;
        case authConstants.LOGIN_CLIENT_SUCCESS:
            state = {
                ...state,
                status: action.payload.status,
                authenticate: true,
                authenticating: false
            }
            break;
        case authConstants.LOGIN_CLIENT_FAILURE:
            state = {
                ...state,
                error: action.payload,
                authenticate: false,
                authenticating: false
            }
        break;

    }


    return state;
}
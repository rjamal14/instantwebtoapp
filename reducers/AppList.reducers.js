/* eslint-disable default-case */
import { AppListConstants } from "../actions/constants"

const initState = {
  error: null,
  message: '',
  loading: false,
  status_code: 200,
  result: {
    data: [],
    currentPage: 1,
    nextPage: 1,
    totalItems: 0,
    totalPages: 1
  },
  detail : {
    app_builds : []
  }
}

export default (state = initState, action) => {
  switch (action.type) {
    case AppListConstants.FETCH_APP_LIST_REQUEST:
      state = {
        ...state,
        loading: true,
        result: {
          data: [],
          currentPage: 1,
          nextPage: 1,
          totalItems: 0,
          totalPages: 1
        }
      }
      break;
    case AppListConstants.FETCH_APP_LIST_SUCCESS:
      state = {
        ...state,
        loading: false,
        result: action.payload.app_list,
        status_code: action.payload.status_code
      }
      break;
    case AppListConstants.FETCH_APP_LIST_FAILURE:
      state = {
        ...state,
        loading: false,
        error: action.payload.error,
        status_code: action.payload.status_code
      }
      break;

      case AppListConstants.FETCH_APP_LIST_DETAIL_REQUEST:
        state = {
          ...state,
          loading: true,
          detail: {
            app_builds : []
          }
        }
        break;
      case AppListConstants.FETCH_APP_LIST_DETAIL_SUCCESS:
        state = {
          ...state,
          loading: false,
          detail: action.payload.app_list_detail,
          status_code: action.payload.status_code
        }
        break;
      case AppListConstants.FETCH_APP_LIST_DETAIL_FAILURE:
        state = {
          ...state,
          loading: false,
          error: action.payload.error,
          status_code: action.payload.status_code
        }
        break;
  }

  return state;
}
/* eslint-disable default-case */
import { TransactionConstants } from "../actions/constants"

const initState = {
  error: null,
  message: '',
  loading: false,
  status_code: 200,
  transaction_list: {
    data: [],
    currentPage: 1,
    nextPage: 1,
    totalItems: 0,
    totalPages: 1
  },
  transaction_unpaid: {
    data: [],
    currentPage: 1,
    nextPage: 1,
    totalItems: 0,
    totalPages: 1
  },
}

export default (state = initState, action) => {
  switch (action.type) {
    case TransactionConstants.FETCH_TRANSACTION_REQUEST:
      state = {
        ...state,
        loading: true,
        transaction_list: {
          data: [],
          currentPage: 1,
          nextPage: 1,
          totalItems: 0,
          totalPages: 1
        }
      }
      break;
    case TransactionConstants.FETCH_TRANSACTION_SUCCESS:
      state = {
        ...state,
        loading: false,
        transaction_list: action.payload.transaction_list,
        status_code: action.payload.status_code
      }
      break;
    case TransactionConstants.FETCH_TRANSACTION_FAILURE:
      state = {
        ...state,
        loading: false,
        error: action.payload.error,
        status_code: action.payload.status_code
      }
      break;

      case TransactionConstants.FETCH_TRANSACTION_UNPAID_REQUEST:
        state = {
          ...state,
          loading: true,
          transaction_unpaid: {
            data: [],
            currentPage: 1,
            nextPage: 1,
            totalItems: 0,
            totalPages: 1
          }
        }
        break;
      case TransactionConstants.FETCH_TRANSACTION_UNPAID_SUCCESS:
        state = {
          ...state,
          loading: false,
          transaction_unpaid: action.payload.transaction_unpaid,
          status_code: action.payload.status_code
        }
        break;
      case TransactionConstants.FETCH_TRANSACTION_UNPAID_FAILURE:
        state = {
          ...state,
          loading: false,
          error: action.payload.error,
          status_code: action.payload.status_code
        }
        break;
  }

  return state;
}
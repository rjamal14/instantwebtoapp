/* eslint-disable default-case */
import { AppBuildContants } from "../actions/constants"

const initState = {
  error: null,
  message: '',
  loading: false,
  status_code: 200,
  result: {
    data: [],
    currentPage: 1,
    nextPage: 1,
    totalItems: 0,
    totalPages: 1
  }
}

export default (state = initState, action) => {
  switch (action.type) {
    case AppBuildContants.APP_BUILD_REQUEST:
      state = {
        ...state,
        loading: true,
        result: {
          data: [],
          currentPage: 1,
          nextPage: 1,
          totalItems: 0,
          totalPages: 1
        }
      }
      break;
    case AppBuildContants.APP_BUILD_SUCCESS:
      state = {
        ...state,
        loading: false,
        result: action.payload.app_build,
        status_code: action.payload.status_code
      }
      break;
    case AppBuildContants.APP_BUILD_FAILURE:
      state = {
        ...state,
        loading: false,
        error: action.payload.error,
        status_code: action.payload.status_code
      }
      break;

      case AppBuildContants.APP_BUILD_REBUILD_REQUEST:
        state = {
          ...state,
          loading: true,
          detail: {
            app_builds : []
          }
        }
        break;
      case AppBuildContants.APP_BUILD_REBUILD_SUCCESS:
        state = {
          ...state,
          loading: false,
        }
        break;
      case AppBuildContants.APP_BUILD_REBUILD_FAILURE:
        state = {
          ...state,
          loading: false,
          error: action.payload.error,
          status_code: action.payload.status_code
        }
        break;
  }

  return state;
}
/* eslint-disable default-case */
import { MasterAppContants } from "../actions/constants";

const initState = {
    success:false,
    loading: false,
    error: null,
    message: ''
};

export default (state = initState, action) => {
    switch (action.type) {
        case MasterAppContants.MASTER_APP_REQUEST:
            state = {
                ...state,
                loading: true
            }
            break;
        case MasterAppContants.MASTER_APP_SUCCESS:
            state = {
                ...state,
                success: action.payload,
                loading: false
            }
            break;
        case MasterAppContants.MASTER_APP_FAILURE:
            state = {
                ...state,
                error: action.payload,
                success: false,
                loading: false
            }
            break;
    }


    return state;
}
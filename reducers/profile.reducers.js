import { profileConstants } from "../actions/constants"

const initState = {
  error: null,
  message: '',
  success: null,
  loading: false,
  status_code:200,
  profile: {
  }
}

export default (state = initState, action) => {
  switch (action.type) {
    case profileConstants.FETCH_PROFILE_REQUEST:
      state = {
        ...state,
        loading: true,
        profile: {}
      }
      break;
    case profileConstants.FETCH_PROFILE_SUCCESS:
      state = {
        ...state,
        loading: false,
        success:true,
        profile: action.payload.profile,
        status_code: action.payload.status_code,
      }
      break;
    case profileConstants.FETCH_PROFILE_FAILURE:
      state = {
        ...state,
        loading: false,
        success: false,
        error: action.payload.error,
        status_code: action.payload.status_code,
      }
      break;

    case profileConstants.FETCH_UPDATE_PROFILE_REQUEST:
      state = {
        ...state,
        loading: true,
        profile: {}
      }
      break;
    case profileConstants.FETCH_UPDATE_PROFILE_SUCCESS:
      state = {
        ...state,
        loading: false,
        success:true,
        profile: action.payload.profile,
        status_code: action.payload.status_code,
      }
      break;
    case profileConstants.FETCH_UPDATE_PROFILE_FAILURE:
      state = {
        ...state,
        loading: false,
        success: false,
        error: action.payload.error,
        status_code: action.payload.status_code,
      }
      break;
  }

  return state;
}
import { commisionConstants } from "../actions/constants"

const initState = {
  error: null,
  message: '',
  loading: false,
  commisions: {
    data: [],
    meta: {
      current_page: 1,
      per_page: 10,
      total: 0
    }
  },
  meta: {
    total: 0
  }
}

export default (state = initState, action) => {
  switch (action.type) {
    case commisionConstants.FETCH_COMMISIONS_REQUEST:
      state = {
        ...state,
        loading: true
      }
      break;
    case commisionConstants.FETCH_COMMISIONS_SUCCESS:
      state = {
        ...state,
        loading: false,
        commisions: action.payload.data,
        meta: action.payload.meta
      }
      break;
    case commisionConstants.FETCH_COMMISIONS_FAILURE:
      state = {
        ...state,
        loading: false,
        error: action.payload.error
      }
      break;
  }

  return state;
}